%{
#define YY_Parser_STYPE yy_Parser_stype
%}
%name Parser
%define LSP_NEEDED
%define LEX_BODY =0
%define ERROR_BODY =0
%define DEBUG 1

%header{
#include <bits/stdc++.h>
#include "node.hpp"
  using namespace std;
Node * createSCNode(Node * descend, string type, string value = "");
void assignRoot(Node * n);
void printToDot(string filename);
void initializeError();
void incError();
int getErrorCount();
#define YY_DECL int yyFlexLexer::yylex(YY_Parser_STYPE *val)

#ifndef FLEXFIX
#define FLEXFIX YY_Parser_STYPE *val
#define FLEXFIX2 val
#endif

%}


%union{
	Node *node_type;
	char *strval;
}

%type <node_type> Goal
%type <node_type> CompilationUnit

%type <node_type> Literal IntegralType Type PrimitiveType ReferenceType NumericType FloatingPointType ArrayType Name ClassType SimpleName QualifiedName ClassDeclaration ClassBody ClassBodyDeclaration MethodDeclarator ClassBodyDeclarations VariableInitializer FormalParameter ConstructorDeclaration VariableDeclaratorId ClassMemberDeclaration Expression MethodDeclaration ArrayInitializer FormalParameterList Block MethodHeader VariableDeclarators TypeDeclarations MethodBody FieldDeclaration VariableDeclarator BlockStatements ConstructorBody VariableInitializers ConstructorDeclarator PostDecrementExpression ExpressionStatement Statement Assignment PostIncrementExpression BlockStatement SwitchBlockStatementGroups PreIncrementExpression ReturnStatement ConstantExpression SwitchLabel BreakStatement IfThenStatement SwitchBlock EmptyStatement StatementNoShortIf MethodInvocation IfThenElseStatement WhileStatementNoShortIf ClassInstanceCreationExpression ContinueStatement SwitchStatement SwitchBlockStatementGroup SwitchLabels LocalVariableDeclaration WhileStatement LabeledStatement StatementExpression IfThenElseStatementNoShortIf LocalVariableDeclarationStatement PreDecrementExpression LabeledStatementNoShortIf StatementWithoutTrailingSubstatement AndExpression ArgumentList DimExpr PrimaryNoNewArray AssignmentOperator ConditionalAndExpression PostfixExpression EqualityExpression MultiplicativeExpression ArrayCreationExpression AssignmentExpression UnaryExpressionNotPlusMinus ArrayAccess ExclusiveOrExpression Primary RelationalExpression Dims ConditionalOrExpression AdditiveExpression ConditionalExpression UnaryExpression InclusiveOrExpression CastExpression DimExprs LeftHandSide ShiftExpression FieldAccess ForStatement ForStatementNoShortIf ForInit ForUpdate StatementExpressionList DoStatement

%token <node_type> IntegerLiteral FloatingPointLiteral BooleanLiteral CharacterLiteral StringLiteral NullLiteral Identifier KeywordBoolean  KeywordByte KeywordShort KeywordInt KeywordLong KeywordChar KeywordFloat KeywordDouble KeywordClass KeywordVoid KeywordIf KeywordElse KeywordThis KeywordSwitch KeywordCase KeywordDefault KeywordWhile KeywordBreak KeywordContinue KeywordReturn Keyword SeparatorLSqBrac SeparatorRSqBrac SeparatorDot SeparatorSemiCo SeparatorComma OperatorEq OperatorColon SeparatorLCuBrac SeparatorRCuBrac SeparatorLParen SeparatorRParen OperatorMiMi OperatorMiEq OperatorUp OperatorLeLeEq KeywordNew KeywordInstanceof OperatorVert OperatorDiv OperatorGrGrGr OperatorLe OperatorPlPl OperatorPl OperatorGrGrEq OperatorNE OperatorAmp OperatorExcl OperatorMi OperatorAnd OperatorUpEq OperatorAmpEq OperatorQm OperatorVertEq OperatorPlEq OperatorLeEq OperatorMul OperatorGrEq OperatorOr OperatorAsEq OperatorPercEq OperatorGr OperatorSlEq OperatorGrGrGrEq OperatorTild OperatorPerc OperatorGrGr OperatorEqEq OperatorLeLe KeywordFor KeywordDo
%start Goal


%%

Goal		: CompilationUnit 		{ $$ = createSCNode($1, string("Goal"));
						  $$->nodeNo = 0; 
						  assignRoot($$);
							//call DFS on $$
					        	//yyterminate();
						}
		;
Literal 	: IntegerLiteral		{ $$ = createSCNode($1, string("Literal")); }
		| FloatingPointLiteral		{ $$ = createSCNode($1, string("Literal")); } 
		| BooleanLiteral		{ $$ = createSCNode($1, string("Literal")); } 
		| CharacterLiteral		{ $$ = createSCNode($1, string("Literal")); }
		| StringLiteral			{ $$ = createSCNode($1, string("Literal")); }
		| NullLiteral			{ $$ = createSCNode($1, string("Literal")); } 
		;
Type		: PrimitiveType			{ $$ = createSCNode($1, string("Type")); }
		| ReferenceType                 { $$ = createSCNode($1, string("Type")); }
		;
PrimitiveType	: NumericType			{ $$ = createSCNode($1, string("PrimitiveType")); }
		| KeywordBoolean		{ $$ = createSCNode($1, string("PrimitiveType")); }
		;
NumericType	: IntegralType			{ $$ = createSCNode($1, string("NumericType")); }		
		| FloatingPointType		{ $$ = createSCNode($1, string("NumericType")); }
		;
IntegralType	: KeywordByte			{ $$ = createSCNode($1, string("IntegralType")); }
		| KeywordShort			{ $$ = createSCNode($1, string("IntegralType")); }
		| KeywordInt			{ $$ = createSCNode($1, string("IntegralType")); }
		| KeywordLong			{ $$ = createSCNode($1, string("IntegralType")); }
		| KeywordChar	  		{ $$ = createSCNode($1, string("IntegralType")); }  	
		;

FloatingPointType	: KeywordDouble		{ $$ = createSCNode($1, string("FloatingPointType")); }     

			| KeywordFloat		{ $$ = createSCNode($1, string("FloatingPointType")); }     
			;

ReferenceType	: ClassType		{ $$ = createSCNode($1, string("ReferenceType")); }
		| ArrayType			{ $$ = createSCNode($1, string("ReferenceType")); }
		;

ClassType	: Name		{ $$ = createSCNode($1, string("ClassType")); }
		;

ArrayType	: PrimitiveType SeparatorLSqBrac SeparatorRSqBrac
						{
						         $$ = createSCNode($1, string("ArrayType"));
							 $$->addChild($2);
							 $$->addChild($3);
						}
		| Name SeparatorLSqBrac SeparatorRSqBrac
		                 	    	{ 	 
							 $$ = createSCNode($1, string("ArrayType"));
							 $$->addChild($2);
							 $$->addChild($3);
						}
							   
		| ArrayType SeparatorLSqBrac SeparatorRSqBrac	
					    	{ 
							 	 $$ = createSCNode($1, string("ArrayType"));
								 $$->addChild($2);
								 $$->addChild($3);
						}
		;
Name 		: SimpleName			{ $$ = createSCNode($1, string("Name")); }
		| QualifiedName			{ $$ = createSCNode($1, string("Name")); }
		;
SimpleName 	: Identifier			{ $$ = createSCNode($1, string("SimpleName")); }
		;
QualifiedName	: Name SeparatorDot Identifier	{  
                                                         $$ = createSCNode($1, string("QualifiedName"));
							 $$->addChild($2);
							 $$->addChild($3);
						}
		;
CompilationUnit : TypeDeclarations 		{ $$ = createSCNode($1, string("CompilationUnit")); }	
		;
TypeDeclarations: ClassDeclaration		{ $$ = createSCNode($1, string("TypeDeclarations")); }
		| TypeDeclarations ClassDeclaration	{  $$ = createSCNode($1, string("TypeDeclarations"));
							   $$->addChild($2);
							}
		; //TODO check if semicolon needed instead of ClassDeclaration

ClassDeclaration: KeywordClass Identifier ClassBody 	{	
									$$ = createSCNode($1, "ClassDeclaration");
									$$->addChild($2);
									$$->addChild($3);
					      		}
		; 
ClassBody	: SeparatorLCuBrac ClassBodyDeclarations SeparatorRCuBrac	
							{
										$$ = createSCNode($1, "ClassBody");
										$$->addChild($2);
										$$->addChild($3);
							}
		| SeparatorLCuBrac SeparatorRCuBrac	{	$$ = createSCNode($1, "ClassBody");
								$$->addChild($2);
							}
		;
ClassBodyDeclarations	: ClassBodyDeclaration		{ $$ = createSCNode($1, "ClassBodyDeclarations"); }
			| ClassBodyDeclarations ClassBodyDeclaration  	{ $$ = createSCNode($1, "ClassBodyDeclarations");
									  $$->addChild($2);
								      	}
			;
ClassBodyDeclaration	: ClassMemberDeclaration 	{ $$ = createSCNode($1, "ClassBodyDeclaration"); }
			| ConstructorDeclaration	{ $$ = createSCNode($1, "ClassBodyDeclaration"); }
			; 
ClassMemberDeclaration  : FieldDeclaration	{ $$ = createSCNode($1, "ClassMemberDeclaration"); }
			| MethodDeclaration	{ $$ = createSCNode($1, "ClassMemberDeclaration"); }
			;
FieldDeclaration	: Type VariableDeclarators SeparatorSemiCo	{  
									   	$$ = createSCNode($1, "FieldDeclaration");
									   	$$->addChild($2);
									   	$$->addChild($3);
									}
			;
VariableDeclarators	: VariableDeclarator	{ $$ = createSCNode($1, "VariableDeclarators"); }
			| VariableDeclarators SeparatorComma VariableDeclarator	{ 																$$ = createSCNode($1, "VariableDeclarators");		
											$$->addChild($2);
											$$->addChild($3);
										}
			;
VariableDeclarator	: VariableDeclaratorId	{ $$ = createSCNode($1, "VariableDeclarator"); }
			| VariableDeclaratorId OperatorEq VariableInitializer	
										{ 
										  	$$ = createSCNode($1, "VariableDeclarator");
										  	$$->addChild($2);
										  	$$->addChild($3);
										}	
			;
VariableDeclaratorId	: Identifier	{ $$ = createSCNode($1, "VariableDeclaratorId"); }
			| VariableDeclaratorId SeparatorLSqBrac SeparatorRSqBrac	{
								  				$$ = createSCNode($1, "VariableDeclaratorId");
									  			$$->addChild($2);
									  			$$->addChild($3);
											}
			;
VariableInitializer	: Expression	{ $$ = createSCNode($1, "VariableInitializer"); }
			| ArrayInitializer	{ $$ = createSCNode($1, "VariableInitializer"); }
			;
MethodDeclaration	: MethodHeader MethodBody	{ $$ = createSCNode($1, "MethodDeclaration"); 
							  $$->addChild($2);
							}
			;
MethodHeader	: Type MethodDeclarator			{ $$ = createSCNode($1, "MethodHeader"); 
							  $$->addChild($2);
							}
		| KeywordVoid MethodDeclarator	{ 
							  	$$ = createSCNode($1, "MethodHeader"); 
								$$->addChild($2);
						}
		;
MethodDeclarator: Identifier SeparatorLParen FormalParameterList SeparatorRParen { 									  										$$ = createSCNode($1, "MethodDeclarator"); 
											$$->addChild($2);
											$$->addChild($3);
											$$->addChild($4);
										}
		| Identifier SeparatorLParen SeparatorRParen	{ $$ = createSCNode($1, "MethodDeclarator"); 
								  $$->addChild($2);
								  $$->addChild($3);
								}
		| MethodDeclarator SeparatorLSqBrac SeparatorRSqBrac	{ $$ = createSCNode($1, "MethodDeclarator"); 
									  $$->addChild($2);
									  $$->addChild($3);
									}
		;
FormalParameterList	: FormalParameter 	{ $$ = createSCNode($1, "FormalParameterList"); }
			| FormalParameterList SeparatorComma FormalParameter 	{ 	$$ = createSCNode($1, "FormalParameterList"); 
											$$->addChild($2);
											$$->addChild($3);
									    	}
			;
FormalParameter	: Type VariableDeclaratorId	{ $$ = createSCNode($1, "FormalParameter"); 
						  $$->addChild($2);
						}
		;
MethodBody	: Block		{ $$ = createSCNode($1, "MethodBody"); }
		| SeparatorSemiCo
				{ 
				  	$$ = createSCNode($1, "MethodBody"); 
				}
		;
ConstructorDeclaration	: ConstructorDeclarator ConstructorBody	{ $$ = createSCNode($1, "ConstructorDeclaration");
										  $$->addChild($2);
										}
			;
ConstructorDeclarator	: SimpleName SeparatorLParen FormalParameterList SeparatorRParen{ $$ = createSCNode($1, "ConstructorDeclarator"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  $$->addChild($4);
											}
			| SimpleName SeparatorLParen SeparatorRParen	{ $$ = createSCNode($1, "ConstructorDeclarator"); 
									  $$->addChild($2);
									  $$->addChild($3);
									}
			;
ConstructorBody	: SeparatorLCuBrac BlockStatements SeparatorRCuBrac	{ $$ = createSCNode($1, "ConstructorBody");
									  $$->addChild($2);
									  $$->addChild($3);
									}
		| SeparatorLCuBrac SeparatorRCuBrac	{							
							 $$ = createSCNode($1, "ConstructorBody");
							 $$->addChild($2);
							}
		;
ArrayInitializer: SeparatorLCuBrac VariableInitializers SeparatorRCuBrac	{	$$ = createSCNode($1, "ArrayInitializer");
											$$->addChild($2);
											$$->addChild($3);
										}
		| SeparatorLCuBrac SeparatorRCuBrac		{	$$ = createSCNode($1, "ArrayInitializer");
									$$->addChild($2);
								}
		;
VariableInitializers	: VariableInitializer 	{ $$ = createSCNode($1, "VariableInitializers"); }
			| VariableInitializers SeparatorComma VariableInitializer 	{	
												$$ = createSCNode($1, "VariableInitializers");
												$$->addChild($2);
												$$->addChild($3);
 											}
			;
Block 	: SeparatorLCuBrac BlockStatements SeparatorRCuBrac	{  $$ = createSCNode($1, "Block"); 
								   $$->addChild($2);
								   $$->addChild($3);
								}
	| SeparatorLCuBrac SeparatorRCuBrac	{ 
						  $$ = createSCNode($1, "Block"); 
						  $$->addChild($2);
						}
	;
BlockStatements	: BlockStatement 	{ $$ = createSCNode($1, "BlockStatements"); }
		| BlockStatements BlockStatement 	{ $$ = createSCNode($1, "BlockStatements"); 
							  $$->addChild($2);
							}
		;
BlockStatement 	: LocalVariableDeclarationStatement	{ $$ = createSCNode($1, "BlockStatement"); }
		| Statement 				{ $$ = createSCNode($1, "BlockStatement"); }
		;
LocalVariableDeclarationStatement	: LocalVariableDeclaration SeparatorSemiCo{ $$ = createSCNode($1, "LocalVariableDeclarationStatement"); 
										    $$->addChild($2);
										  }
					;
LocalVariableDeclaration 	: Type VariableDeclarators 	{ $$ = createSCNode($1, "LocalVariableDeclaration"); 
								  $$->addChild($2);
								}
				;
Statement 	: StatementWithoutTrailingSubstatement 	{ $$ = createSCNode($1, "Statement"); }
		| LabeledStatement			{ $$ = createSCNode($1, "Statement"); }
		| IfThenStatement 			{ $$ = createSCNode($1, "Statement"); }
		| IfThenElseStatement			{ $$ = createSCNode($1, "Statement"); }
		| WhileStatement			{ $$ = createSCNode($1, "Statement"); }
		| ForStatement				{ $$ = createSCNode($1, "Statement"); }
		;
StatementNoShortIf 	: StatementWithoutTrailingSubstatement	{ $$ = createSCNode($1, "StatementNoShortIf"); }
			| LabeledStatementNoShortIf		{ $$ = createSCNode($1, "StatementNoShortIf"); }
			| IfThenElseStatementNoShortIf		{ $$ = createSCNode($1, "StatementNoShortIf"); }
			| WhileStatementNoShortIf		{ $$ = createSCNode($1, "StatementNoShortIf"); }
			| ForStatementNoShortIf			{ $$ = createSCNode($1, "StatementNoShortIf"); }
			;
StatementWithoutTrailingSubstatement	: Block 		{ $$ = createSCNode($1, "StatementWithoutTrailingSubstatement"); }
				   	| EmptyStatement	{ $$ = createSCNode($1, "StatementWithoutTrailingSubstatement"); }
					| ExpressionStatement	{ $$ = createSCNode($1, "StatementWithoutTrailingSubstatement"); }
					| SwitchStatement	{ $$ = createSCNode($1, "StatementWithoutTrailingSubstatement"); }
					| BreakStatement	{ $$ = createSCNode($1, "StatementWithoutTrailingSubstatement"); }
					| ContinueStatement	{ $$ = createSCNode($1, "StatementWithoutTrailingSubstatement"); }
					| ReturnStatement	{ $$ = createSCNode($1, "StatementWithoutTrailingSubstatement"); }
					| DoStatement 	{ $$ = createSCNode($1, "StatementWithoutTrailingSubstatement"); }
					;
EmptyStatement	: SeparatorSemiCo   { $$ = createSCNode($1, "EmptyStatement"); }
		;
LabeledStatement: Identifier OperatorColon Statement 	{ 	string v1 = $2->getNodeVal();
						  		$$ = createSCNode($1, "LabeledStatement"); 
						  		$$->addChild($2);
							}
		;
LabeledStatementNoShortIf	: Identifier OperatorColon StatementNoShortIf 	{ $$ = createSCNode($1, "LabeledStatementNoShortIf"); 
										  $$->addChild($2);
										  $$->addChild($3);
										}
				;
ExpressionStatement 	: StatementExpression SeparatorSemiCo { $$ = createSCNode($1, "ExpressionStatement"); 
							  	$$->addChild($2);
							      }
			;
StatementExpression	: Assignment 	{ $$ = createSCNode($1, "StatementExpression"); }
			| PreIncrementExpression	{ $$ = createSCNode($1, "StatementExpression"); }
			| PreDecrementExpression	{ $$ = createSCNode($1, "StatementExpression"); }
			| PostIncrementExpression	{ $$ = createSCNode($1, "StatementExpression"); }
			| PostDecrementExpression	{ $$ = createSCNode($1, "StatementExpression"); }
			| MethodInvocation	{ $$ = createSCNode($1, "StatementExpression"); }
			| ClassInstanceCreationExpression 	{ $$ = createSCNode($1, "StatementExpression"); }
			;
IfThenStatement : KeywordIf SeparatorLParen Expression SeparatorRParen Statement 	{ 
										  	$$ = createSCNode($1, "IfThenStatement"); 
										$$->addChild($2);
										$$->addChild($3);
										$$->addChild($4);
										$$->addChild($5);
									}
		;
IfThenElseStatement 	: KeywordIf SeparatorLParen Expression SeparatorRParen StatementNoShortIf KeywordElse Statement 	{ 													  	$$ = createSCNode($1, "IfThenElseStatement"); 
														$$->addChild($2);
														$$->addChild($3);
														$$->addChild($4);
														$$->addChild($5);
														$$->addChild($6);
														$$->addChild($7);
													}
			;
IfThenElseStatementNoShortIf	: KeywordIf SeparatorLParen Expression SeparatorRParen StatementNoShortIf KeywordElse StatementNoShortIf 	{ 															  	$$ = createSCNode($1, "IfThenElseStatementNoShortIf"); 
																$$->addChild($2);
																$$->addChild($3);
																$$->addChild($4);
																$$->addChild($5);
																$$->addChild($6);
																$$->addChild($7);
															}
				;
SwitchStatement : KeywordSwitch SeparatorLParen Expression SeparatorRParen SwitchBlock 	{ 									  	$$ = createSCNode($1, "SwitchStatement"); 
										$$->addChild($2);
										$$->addChild($3);
										$$->addChild($4);
										$$->addChild($5);
									}
		;
SwitchBlock 	:  SeparatorLCuBrac SwitchBlockStatementGroups SeparatorRCuBrac	{											$$ = createSCNode($1, "SwitchBlock");
											$$->addChild($2);
											$$->addChild($3);
									}
		| SeparatorLCuBrac SeparatorRCuBrac 	{								$$ = createSCNode($1, "SwitchBlock");
							$$->addChild($2);
						}
		;
SwitchBlockStatementGroups 	: SwitchBlockStatementGroup 	{ $$ = createSCNode($1, "SwitchBlockStatementGroups"); }
				| SwitchBlockStatementGroups SwitchBlockStatementGroup 	{ $$ = createSCNode($1, "SwitchBlockStatementGroups"); 
											  $$->addChild($2);
											}
				;
SwitchBlockStatementGroup 	: SwitchLabels BlockStatements 	{ $$ = createSCNode($1, "SwitchBlockStatementGroup"); 
								  $$->addChild($2);
								}
				;
SwitchLabels	: SwitchLabel 	{ $$ = createSCNode($1, "SwitchLabels"); }
		| SwitchLabels SwitchLabel 	{ $$ = createSCNode($1, "SwitchLabels"); 
						  $$->addChild($2);
						}
		;
SwitchLabel 	: KeywordCase ConstantExpression OperatorColon   {										$$ = createSCNode($1, "SwitchLabel");
									$$->addChild($2);
									$$->addChild($3);
							}
		| KeywordDefault OperatorColon	{							$$ = createSCNode($1, "SwitchLabel");
							$$->addChild($2);
					}
		;
WhileStatement 	: KeywordWhile SeparatorLParen Expression SeparatorRParen Statement 	{ $$ = createSCNode($1, "WhileStatement"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											}
		;
WhileStatementNoShortIf : KeywordWhile SeparatorLParen Expression SeparatorRParen StatementNoShortIf 	{ 											  	$$ = createSCNode($1, "WhileStatementNoShortIf"); 
												$$->addChild($2);
												$$->addChild($3);
												$$->addChild($4);
												$$->addChild($5);
											}
			;
DoStatement : KeywordDo Statement KeywordWhile SeparatorLParen Expression SeparatorRParen SeparatorSemiCo 	{ 											  	$$ = createSCNode($1, "DoStatement"); 
												$$->addChild($2);
												$$->addChild($3);
												$$->addChild($4);
												$$->addChild($5);
												$$->addChild($6);
												$$->addChild($7);
											}
			;
ForStatement 	: KeywordFor SeparatorLParen ForInit SeparatorSemiCo Expression SeparatorSemiCo ForUpdate SeparatorRParen Statement 	{ $$ = createSCNode($1, "ForStatement"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											  $$->addChild($6);
											  $$->addChild($7);
											  $$->addChild($8);
											  $$->addChild($9);
											}
		| KeywordFor SeparatorLParen ForInit SeparatorSemiCo Expression SeparatorSemiCo SeparatorRParen Statement 	{ $$ = createSCNode($1, "ForStatement"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											  $$->addChild($6);
											  $$->addChild($7);
											  $$->addChild($8);
											}
		| KeywordFor SeparatorLParen ForInit SeparatorSemiCo SeparatorSemiCo ForUpdate SeparatorRParen Statement 	{ $$ = createSCNode($1, "ForStatement"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											  $$->addChild($6);
											  $$->addChild($7);
											  $$->addChild($8);
											}
		| KeywordFor SeparatorLParen ForInit SeparatorSemiCo SeparatorSemiCo SeparatorRParen Statement 	{ $$ = createSCNode($1, "ForStatement"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											  $$->addChild($6);
											  $$->addChild($7);
											}
		| KeywordFor SeparatorLParen SeparatorSemiCo Expression SeparatorSemiCo ForUpdate SeparatorRParen Statement 	{ $$ = createSCNode($1, "ForStatement"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											  $$->addChild($6);
											  $$->addChild($7);
											  $$->addChild($8);
											}
		| KeywordFor SeparatorLParen SeparatorSemiCo Expression SeparatorSemiCo SeparatorRParen Statement 	{ $$ = createSCNode($1, "ForStatement"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											  $$->addChild($6);
											  $$->addChild($7);
											}
		| KeywordFor SeparatorLParen SeparatorSemiCo SeparatorSemiCo ForUpdate SeparatorRParen Statement 	{ $$ = createSCNode($1, "ForStatement"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											  $$->addChild($6);
											  $$->addChild($7);
											}
		| KeywordFor SeparatorLParen SeparatorSemiCo SeparatorSemiCo SeparatorRParen Statement 	{ $$ = createSCNode($1, "ForStatement"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											  $$->addChild($6);
											}
	    | KeywordFor SeparatorLParen ForInit SeparatorSemiCo Expression SeparatorSemiCo error SeparatorRParen Statement SeparatorSemiCo { yyerrok;yyclearin;}
		;
ForStatementNoShortIf	: KeywordFor SeparatorLParen ForInit SeparatorSemiCo Expression SeparatorSemiCo ForUpdate SeparatorRParen StatementNoShortIf 	{ $$ = createSCNode($1, "ForStatementNoShortIf"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											  $$->addChild($6);
											  $$->addChild($7);
											  $$->addChild($8);
											  $$->addChild($9);
											}
			| KeywordFor SeparatorLParen ForInit SeparatorSemiCo Expression SeparatorSemiCo SeparatorRParen StatementNoShortIf 	{ $$ = createSCNode($1, "ForStatementNoShortIf"); 
												  $$->addChild($2);
												  $$->addChild($3);
												  $$->addChild($4);
												  $$->addChild($5);
												  $$->addChild($6);
												  $$->addChild($7);
												  $$->addChild($8);
												}
			| KeywordFor SeparatorLParen ForInit SeparatorSemiCo SeparatorSemiCo ForUpdate SeparatorRParen StatementNoShortIf 	{ $$ = createSCNode($1, "ForStatementNoShortIf"); 
												  $$->addChild($2);
												  $$->addChild($3);
												  $$->addChild($4);
												  $$->addChild($5);
												  $$->addChild($6);
												  $$->addChild($7);
												  $$->addChild($8);
												}
			| KeywordFor SeparatorLParen ForInit SeparatorSemiCo SeparatorSemiCo SeparatorRParen StatementNoShortIf 	{ $$ = createSCNode($1, "ForStatementNoShortIf"); 
												  $$->addChild($2);
												  $$->addChild($3);
												  $$->addChild($4);
												  $$->addChild($5);
												  $$->addChild($6);
												  $$->addChild($7);
												}
			| KeywordFor SeparatorLParen SeparatorSemiCo Expression SeparatorSemiCo ForUpdate SeparatorRParen StatementNoShortIf 	{ $$ = createSCNode($1, "ForStatementNoShortIf"); 
												  $$->addChild($2);
												  $$->addChild($3);
												  $$->addChild($4);
												  $$->addChild($5);
												  $$->addChild($6);
												  $$->addChild($7);
												  $$->addChild($8);
												}
			| KeywordFor SeparatorLParen SeparatorSemiCo Expression SeparatorSemiCo SeparatorRParen StatementNoShortIf 	{ $$ = createSCNode($1, "ForStatementNoShortIf"); 
												  $$->addChild($2);
												  $$->addChild($3);
												  $$->addChild($4);
												  $$->addChild($5);
												  $$->addChild($6);
												  $$->addChild($7);
												}
			| KeywordFor SeparatorLParen SeparatorSemiCo SeparatorSemiCo ForUpdate SeparatorRParen StatementNoShortIf 	{ $$ = createSCNode($1, "ForStatementNoShortIf"); 
												  $$->addChild($2);
												  $$->addChild($3);
												  $$->addChild($4);
												  $$->addChild($5);
												  $$->addChild($6);
												  $$->addChild($7);
												}
			| KeywordFor SeparatorLParen SeparatorSemiCo SeparatorSemiCo SeparatorRParen StatementNoShortIf 	{ $$ = createSCNode($1, "ForStatementNoShortIf"); 
												  $$->addChild($2);
												  $$->addChild($3);
												  $$->addChild($4);
												  $$->addChild($5);
												  $$->addChild($6);
												}
			;
ForInit 	: StatementExpressionList 	{ $$ = createSCNode($1, "ForInit"); }
		| LocalVariableDeclaration 	{ $$ = createSCNode($1, "ForInit"); }
		;
ForUpdate 	: StatementExpressionList 	{ $$ = createSCNode($1, "ForUpdate"); }

StatementExpressionList 	: StatementExpression 	{ $$ = createSCNode($1, "StatementExpressionList"); }
				| StatementExpressionList SeparatorComma StatementExpression 	{ $$ = createSCNode($1, "StatementExpressionList"); }
				;
BreakStatement	: KeywordBreak Identifier SeparatorSemiCo 	{ 						  	$$ = createSCNode($1, "BreakStatement"); 
							$$->addChild($2);
							$$->addChild($3);
						}
		| KeywordBreak SeparatorSemiCo	{					  	$$ = createSCNode($1, "BreakStatement"); 
						$$->addChild($2);
					}
		;
ContinueStatement	: KeywordContinue Identifier SeparatorSemiCo	{ 							  	$$ = createSCNode($1, "ContinueStatement"); 
								$$->addChild($2);
								$$->addChild($3);
							}
			| KeywordContinue SeparatorSemiCo	{ 						  	$$ = createSCNode($1, "ContinueStatement"); 
							$$->addChild($2);
						}
			;
ReturnStatement	: KeywordReturn Expression SeparatorSemiCo { 						  	$$ = createSCNode($1, "ReturnStatement"); 
							$$->addChild($2);
							$$->addChild($3);
						}
		| KeywordReturn SeparatorSemiCo	{ $$ = createSCNode($1, "ReturnStatement"); 
						$$->addChild($2);
					}
		;
Primary	: PrimaryNoNewArray	{ $$ = createSCNode($1, "Primary"); }
	|ArrayCreationExpression	{ $$ = createSCNode($1, "Primary"); }
	;
PrimaryNoNewArray	: Literal 	{ $$ = createSCNode($1, "PrimaryNoNewArray"); }
			| KeywordThis	{ $$ = createSCNode($1, "PrimaryNoNewArray"); }
			| SeparatorLParen Expression SeparatorRParen	{ $$ = createSCNode($1, "PrimaryNoNewArray");
									  $$->addChild($2);
									  $$->addChild($3);
									}
			| ClassInstanceCreationExpression	{ $$ = createSCNode($1, "PrimaryNoNewArray"); }
			| FieldAccess	{ $$ = createSCNode($1, "PrimaryNoNewArray"); }
			| MethodInvocation 	{ $$ = createSCNode($1, "PrimaryNoNewArray"); }
			| ArrayAccess 	{ $$ = createSCNode($1, "PrimaryNoNewArray"); }
			;
ClassInstanceCreationExpression	: KeywordNew ClassType SeparatorLParen ArgumentList SeparatorRParen	{ $$ = createSCNode($1, "ClassInstanceCreationExpression"); 
													  $$->addChild($2);
													  $$->addChild($3);
													  $$->addChild($4);
													  $$->addChild($5);
													}
				| KeywordNew ClassType SeparatorLParen SeparatorRParen	{ $$ = createSCNode($1, "ClassInstanceCreationExpression"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  $$->addChild($4);
											}
				;
ArgumentList 	: Expression 	{ $$ = createSCNode($1, "ArgumentList"); }
		| ArgumentList SeparatorComma Expression 	{ $$ = createSCNode($1, "ArgumentList"); 
								  $$->addChild($2);
								  $$->addChild($3);
								}
		;
ArrayCreationExpression : KeywordNew PrimitiveType DimExprs Dims 	{ $$ = createSCNode($1, "ArrayCreationExpression"); 
									  $$->addChild($2);
									  $$->addChild($3);
									  $$->addChild($4);
									}
			| KeywordNew PrimitiveType DimExprs	{ $$ = createSCNode($1, "ArrayCreationExpression"); 
								  $$->addChild($2);
								  $$->addChild($3);
								}
			| KeywordNew ClassType DimExprs Dims { $$ = createSCNode($1, "ArrayCreationExpression"); 
									  $$->addChild($2);
									  $$->addChild($3);
									  $$->addChild($4);
									}
			| KeywordNew ClassType DimExprs 	{ $$ = createSCNode($1, "ArrayCreationExpression"); 
									  $$->addChild($2);
									  $$->addChild($3);
									}
			;
DimExprs: DimExpr 	{ $$ = createSCNode($1, "DimExprs"); }
	| DimExprs DimExpr 	{ $$ = createSCNode($1, "DimExprs"); 
				  $$->addChild($2);
				}
	;
DimExpr : SeparatorLSqBrac Expression SeparatorRSqBrac 	{ $$ = createSCNode($1, "DimExpr"); 
							  $$->addChild($2);
							  $$->addChild($3);
							}
	;
Dims 	: SeparatorLSqBrac SeparatorRSqBrac 	{ $$ = createSCNode($1, "Dims"); 
						  $$->addChild($2);
						}
	| Dims SeparatorLSqBrac SeparatorRSqBrac 	{ $$ = createSCNode($1, "Dims"); 
							  $$->addChild($2);
							  $$->addChild($3);
							}
	;
FieldAccess	: Primary SeparatorDot Identifier	{ $$ = createSCNode($1, "FieldAccess"); 
							  $$->addChild($2);
							  $$->addChild($3);
							}
		;
MethodInvocation	: Name SeparatorLParen ArgumentList SeparatorRParen 	{ $$ = createSCNode($1, "MethodInvocation"); 
										  $$->addChild($2);
										  $$->addChild($3);
										  $$->addChild($4);
										}
			| Name SeparatorLParen SeparatorRParen	{ $$ = createSCNode($1, "MethodInvocation"); 
								  $$->addChild($2);
								  $$->addChild($3);
								}
			| Primary SeparatorDot Identifier SeparatorLParen ArgumentList SeparatorRParen 	{ $$ = createSCNode($1, "MethodInvocation"); 
										  $$->addChild($2);
										  $$->addChild($3);
										  $$->addChild($4);
										  $$->addChild($5);
										  $$->addChild($6);
										}
			| Primary SeparatorDot Identifier SeparatorLParen SeparatorRParen 	{ $$ = createSCNode($1, "MethodInvocation"); 
												  $$->addChild($2);
												  $$->addChild($3);
												  $$->addChild($4);
												  $$->addChild($5);
												}
			;
ArrayAccess	: Name SeparatorLSqBrac Expression SeparatorRSqBrac	{ $$ = createSCNode($1, "ArrayAccess"); 
									  $$->addChild($2);
									  $$->addChild($3);
									  $$->addChild($4);
									}
		| PrimaryNoNewArray SeparatorLSqBrac Expression SeparatorRSqBrac	{ $$ = createSCNode($1, "ArrayAccess"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  $$->addChild($4);
											}
		;
PostfixExpression	: Primary 	{ $$ = createSCNode($1, "PostfixExpression"); }
			| Name 		{ $$ = createSCNode($1, "PostfixExpression"); }
			| PostIncrementExpression 	{ $$ = createSCNode($1, "PostfixExpression"); }
			| PostDecrementExpression 	{ $$ = createSCNode($1, "PostfixExpression"); }
			;
PostIncrementExpression	: PostfixExpression OperatorPlPl 	{ $$ = createSCNode($1, "PostIncrementExpression"); 
								  $$->addChild($2);
								}
			;
PostDecrementExpression : PostfixExpression OperatorMiMi	{ $$ = createSCNode($1, "PostDecrementExpression"); 
								  $$->addChild($2);
								}
			;
UnaryExpression : PreIncrementExpression 	{ $$ = createSCNode($1, "UnaryExpression"); }
		| PreDecrementExpression	{ $$ = createSCNode($1, "UnaryExpression"); }
		| OperatorPl UnaryExpression 	{ $$ = createSCNode($1, "UnaryExpression"); 
						  $$->addChild($2);
						}
		| OperatorMi UnaryExpression 	{ $$ = createSCNode($1, "UnaryExpression"); 
						  $$->addChild($2);
						}
		| UnaryExpressionNotPlusMinus 	{ $$ = createSCNode($1, "UnaryExpression"); }
		;
PreIncrementExpression 	: OperatorPlPl UnaryExpression 	{ $$ = createSCNode($1, "PreIncrementExpression"); 
							  $$->addChild($2);
							}
			;
PreDecrementExpression	: OperatorMiMi UnaryExpression 	{ $$ = createSCNode($1, "PreDecrementExpression"); 
							  $$->addChild($2);
							}
			;
UnaryExpressionNotPlusMinus	: PostfixExpression 	{ $$ = createSCNode($1, "UnaryExpressionNotPlusMinus"); }
				| OperatorTild UnaryExpression 	{ $$ = createSCNode($1, "UnaryExpressionNotPlusMinus"); 
								  $$->addChild($2);
								}
				| OperatorExcl UnaryExpression 	{ $$ = createSCNode($1, "UnaryExpressionNotPlusMinus"); 
								  $$->addChild($2);
								}
				| CastExpression 	{ $$ = createSCNode($1, "UnaryExpressionNotPlusMinus"); }
				;
CastExpression 	: SeparatorLParen PrimitiveType Dims SeparatorRParen UnaryExpression 	{ $$ = createSCNode($1, "CastExpression"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											}
		| SeparatorLParen PrimitiveType SeparatorRParen UnaryExpression { $$ = createSCNode($1, "CastExpression"); 
										  $$->addChild($2);
										  $$->addChild($3);
										  $$->addChild($4);
										}
		| SeparatorLParen Expression SeparatorRParen UnaryExpressionNotPlusMinus 	{ $$ = createSCNode($1, "CastExpression"); 
												  $$->addChild($2);
												  $$->addChild($3);
												  $$->addChild($4);
												}
		| SeparatorLParen Name Dims SeparatorRParen UnaryExpressionNotPlusMinus	{ $$ = createSCNode($1, "CastExpression"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											}
		;
MultiplicativeExpression: UnaryExpression 	{ $$ = createSCNode($1, "MultiplicativeExpression"); }
			| MultiplicativeExpression OperatorMul UnaryExpression 	{ $$ = createSCNode($1, "MultiplicativeExpression"); 
										  $$->addChild($2);
										  $$->addChild($3);
										}
			| MultiplicativeExpression OperatorDiv UnaryExpression 	{ $$ = createSCNode($1, "MultiplicativeExpression"); 
										  $$->addChild($2);
										  $$->addChild($3);
										}
			| MultiplicativeExpression OperatorPerc UnaryExpression { $$ = createSCNode($1, "MultiplicativeExpression"); 
										  $$->addChild($2);
										  $$->addChild($3);
										}
			| MultiplicativeExpression OperatorMul error UnaryExpression { yyerrok;yyclearin; }
			| MultiplicativeExpression OperatorDiv error UnaryExpression { yyerrok;yyclearin; }
			| MultiplicativeExpression OperatorPerc error UnaryExpression { yyerrok;yyclearin; }
			;
AdditiveExpression	: MultiplicativeExpression	{ $$ = createSCNode($1, "AdditiveExpression"); }
			| AdditiveExpression OperatorPl MultiplicativeExpression	{ $$ = createSCNode($1, "AdditiveExpression"); 
											  $$->addChild($2);
											  $$->addChild($3);
											}
			| AdditiveExpression OperatorMi MultiplicativeExpression	{ $$ = createSCNode($1, "AdditiveExpression"); 
											  $$->addChild($2);
											  $$->addChild($3);
											}
			| AdditiveExpression OperatorPl error MultiplicativeExpression { yyerrok;yyclearin;}
			| AdditiveExpression OperatorMi error MultiplicativeExpression { yyerrok;yyclearin;}
			;
ShiftExpression	: AdditiveExpression	{ $$ = createSCNode($1, "ShiftExpression"); }
		| ShiftExpression OperatorLeLe AdditiveExpression	{ $$ = createSCNode($1, "ShiftExpression"); 
									  $$->addChild($2);
									  $$->addChild($3);
									}
		| ShiftExpression OperatorGrGr AdditiveExpression	{ $$ = createSCNode($1, "ShiftExpression"); 
									  $$->addChild($2);
									  $$->addChild($3);
									}
		| ShiftExpression OperatorGrGrGr AdditiveExpression	{ $$ = createSCNode($1, "ShiftExpression"); 
									  $$->addChild($2);
									  $$->addChild($3);
									}
		;
RelationalExpression	: ShiftExpression	{ $$ = createSCNode($1, "RelationalExpression"); }
			| RelationalExpression OperatorLe ShiftExpression	{ $$ = createSCNode($1, "RelationalExpression"); 
										  $$->addChild($2);
										  $$->addChild($3);
										}
			| RelationalExpression OperatorGr ShiftExpression	{ $$ = createSCNode($1, "RelationalExpression"); 
										  $$->addChild($2);
										  $$->addChild($3);
										}
			| RelationalExpression OperatorLeEq ShiftExpression	{ $$ = createSCNode($1, "RelationalExpression"); 
										  $$->addChild($2);
										  $$->addChild($3);
										}
			| RelationalExpression OperatorGrEq ShiftExpression	{ $$ = createSCNode($1, "RelationalExpression"); 
										  $$->addChild($2);
										  $$->addChild($3);
										}
			| RelationalExpression KeywordInstanceof ReferenceType	{ $$ = createSCNode($1, "RelationalExpression"); 
									  $$->addChild($2);
									  $$->addChild($3);
									}
			;
EqualityExpression	: RelationalExpression	{ $$ = createSCNode($1, "EqualityExpression"); }
			| EqualityExpression OperatorEqEq RelationalExpression	{ $$ = createSCNode($1, "EqualityExpression"); 
										  $$->addChild($2);
										  $$->addChild($3);
										}
			| EqualityExpression OperatorNE RelationalExpression	{ $$ = createSCNode($1, "EqualityExpression"); 
										  $$->addChild($2);
										  $$->addChild($3);
										}
			;
AndExpression 	: EqualityExpression	{ $$ = createSCNode($1, "AndExpression"); }
		| AndExpression OperatorAmp EqualityExpression	{ $$ = createSCNode($1, "AndExpression"); 
								  $$->addChild($2);
								  $$->addChild($3);
								}
		;
ExclusiveOrExpression	: AndExpression 	{ $$ = createSCNode($1, "ExclusiveOrExpression"); }
			| ExclusiveOrExpression OperatorUp AndExpression 	{ $$ = createSCNode($1, "ExclusiveOrExpression"); 
										  $$->addChild($2);
										  $$->addChild($3);
										}
			;
InclusiveOrExpression	: ExclusiveOrExpression	{ $$ = createSCNode($1, "InclusiveOrExpression"); }
		  	| InclusiveOrExpression OperatorVert ExclusiveOrExpression	{ $$ = createSCNode($1, "InclusiveOrExpression"); 
											  $$->addChild($2);
											  $$->addChild($3);
											}
			;
ConditionalAndExpression: InclusiveOrExpression	{ $$ = createSCNode($1, "ConditionalAndExpression"); }
			| ConditionalAndExpression OperatorAnd InclusiveOrExpression	{ $$ = createSCNode($1, "ConditionalAndExpression"); 
											  $$->addChild($2);
											  $$->addChild($3);
											}
			;
ConditionalOrExpression	: ConditionalAndExpression	{ $$ = createSCNode($1, "ConditionalOrExpression"); }
			| ConditionalOrExpression OperatorOr ConditionalAndExpression	{ $$ = createSCNode($1, "ConditionalOrExpression"); 
											  $$->addChild($2);
											  $$->addChild($3);
											}
			;
ConditionalExpression 	: ConditionalOrExpression	{ $$ = createSCNode($1, "ConditionalExpression"); }
			| ConditionalOrExpression OperatorQm Expression OperatorColon ConditionalExpression 	{ $$ = createSCNode($1, "ConditionalExpression"); 
														  $$->addChild($2);
														  $$->addChild($3);
														  $$->addChild($4);
														  $$->addChild($5);
														}
			;
AssignmentExpression	: ConditionalExpression 	{ $$ = createSCNode($1, "AssignmentExpression"); }
			| Assignment 	{ $$ = createSCNode($1, "AssignmentExpression"); }
			;
Assignment 	: LeftHandSide AssignmentOperator AssignmentExpression	{ $$ = createSCNode($1, "Assignment"); 
									  $$->addChild($2);
									  $$->addChild($3);
									}
		;
LeftHandSide	: Name 	{ $$ = createSCNode($1, "LeftHandSide"); }
		| FieldAccess	{ $$ = createSCNode($1, "LeftHandSide"); }
		| ArrayAccess	{ $$ = createSCNode($1, "LeftHandSide"); }
		;
AssignmentOperator	: OperatorEq 	{ $$ = createSCNode($1, "AssignmentOperator"); }
			| OperatorAsEq 	{ $$ = createSCNode($1, "AssignmentOperator"); }
			| OperatorSlEq	{ $$ = createSCNode($1, "AssignmentOperator"); }
			| OperatorPercEq	{ $$ = createSCNode($1, "AssignmentOperator"); }
			| OperatorPlEq	{ $$ = createSCNode($1, "AssignmentOperator"); }
			| OperatorMiEq	{ $$ = createSCNode($1, "AssignmentOperator"); }
			| OperatorLeLeEq	{ $$ = createSCNode($1, "AssignmentOperator"); }
			| OperatorGrGrEq 	{ $$ = createSCNode($1, "AssignmentOperator"); }
			| OperatorGrGrGrEq	{ $$ = createSCNode($1, "AssignmentOperator"); }
			| OperatorAmpEq	{ $$ = createSCNode($1, "AssignmentOperator"); }
			| OperatorUpEq	{ $$ = createSCNode($1, "AssignmentOperator"); }
			| OperatorVertEq	{ $$ = createSCNode($1, "AssignmentOperator"); }
			;
Expression 	: AssignmentExpression	{ $$ = createSCNode($1, "Expression"); }
		;
ConstantExpression	: Expression 	{ $$ = createSCNode($1, "ConstantExpression"); }
			;
%%

int errorCount;
void initializeError()
{
	errorCount=0;
}
void incError()
{
	errorCount++;
}
int getErrorCount()
{
	return errorCount;
}
Node * createSCNode(Node * descend, string type, string value)		//Create a node with single child
{
	Node * n = new Node(type, value);
	n->addChild(descend);
	return n;
}

Node * root;

void assignRoot(Node * n)
{
	root = n;
}
void printToDot(string filename)
{
	ofstream outfile;
	outfile.open(filename.c_str());

	// Write initial part to outfile 
	outfile<<"digraph G{ "<<endl;

	queue<Node *> q;
	Node * t;
	Node * next;
	
	int nextNo = 1;
	q.push(root);
	
	map<int, string> labels;
	map<int, string> vals;
	while(!q.empty())
	{
		t = q.front(); q.pop();
		labels[t->nodeNo] = t->getNodeType();
		for(int i = 0; i< t->child.size(); ++i)
		{
			next = t->child[i];
			next->nodeNo = nextNo;
			outfile<<t->nodeNo<<"->"<<nextNo<<";"<<endl;		
			q.push(next);
			nextNo++;
		}

		if(t->child.size() == 0)	//leaf node
		{
			vals[t->nodeNo] = t->getNodeVal();
		}
	}

	for(map<int, string>::iterator it = labels.begin(); it!=labels.end(); it++)
	{
		if(vals.find(it->first) == vals.end())	//not a leaf
		{
			outfile<<it->first<<" "<<"[label=\""<<it->second<<"\"];"<<endl;
		}
		else
		{
			outfile<<it->first<<" "<<"[label=\""<<it->second<<"\\n\\n"<<vals[it->first]<<" \"];"<<endl;
		}
	}
	outfile<<"}"<<endl;
	outfile.close();
}

