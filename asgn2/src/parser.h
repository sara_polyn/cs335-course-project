#ifndef YY_Parser_h_included
#define YY_Parser_h_included
#define YY_USE_CLASS

#line 1 "/usr/share/bison++/bison.h"
/* before anything */
#ifdef c_plusplus
 #ifndef __cplusplus
  #define __cplusplus
 #endif
#endif


 #line 8 "/usr/share/bison++/bison.h"
#define YY_Parser_LSP_NEEDED 
#define YY_Parser_LEX_BODY  =0
#define YY_Parser_ERROR_BODY  =0
#define YY_Parser_DEBUG  1
#line 10 "src/parser.y"

#include <bits/stdc++.h>
#include "node.hpp"
  using namespace std;
Node * createSCNode(Node * descend, string type, string value = "");
void assignRoot(Node * n);
void printToDot(string filename);
void initializeError();
void incError();
int getErrorCount();
#define YY_DECL int yyFlexLexer::yylex(YY_Parser_STYPE *val)

#ifndef FLEXFIX
#define FLEXFIX YY_Parser_STYPE *val
#define FLEXFIX2 val
#endif


#line 30 "src/parser.y"
typedef union{
	Node *node_type;
	char *strval;
} yy_Parser_stype;
#define YY_Parser_STYPE yy_Parser_stype

#line 21 "/usr/share/bison++/bison.h"
 /* %{ and %header{ and %union, during decl */
#ifndef YY_Parser_COMPATIBILITY
 #ifndef YY_USE_CLASS
  #define  YY_Parser_COMPATIBILITY 1
 #else
  #define  YY_Parser_COMPATIBILITY 0
 #endif
#endif

#if YY_Parser_COMPATIBILITY != 0
/* backward compatibility */
 #ifdef YYLTYPE
  #ifndef YY_Parser_LTYPE
   #define YY_Parser_LTYPE YYLTYPE
/* WARNING obsolete !!! user defined YYLTYPE not reported into generated header */
/* use %define LTYPE */
  #endif
 #endif
/*#ifdef YYSTYPE*/
  #ifndef YY_Parser_STYPE
   #define YY_Parser_STYPE YYSTYPE
  /* WARNING obsolete !!! user defined YYSTYPE not reported into generated header */
   /* use %define STYPE */
  #endif
/*#endif*/
 #ifdef YYDEBUG
  #ifndef YY_Parser_DEBUG
   #define  YY_Parser_DEBUG YYDEBUG
   /* WARNING obsolete !!! user defined YYDEBUG not reported into generated header */
   /* use %define DEBUG */
  #endif
 #endif 
 /* use goto to be compatible */
 #ifndef YY_Parser_USE_GOTO
  #define YY_Parser_USE_GOTO 1
 #endif
#endif

/* use no goto to be clean in C++ */
#ifndef YY_Parser_USE_GOTO
 #define YY_Parser_USE_GOTO 0
#endif

#ifndef YY_Parser_PURE

 #line 65 "/usr/share/bison++/bison.h"

#line 65 "/usr/share/bison++/bison.h"
/* YY_Parser_PURE */
#endif


 #line 68 "/usr/share/bison++/bison.h"

#line 68 "/usr/share/bison++/bison.h"
/* prefix */

#ifndef YY_Parser_DEBUG

 #line 71 "/usr/share/bison++/bison.h"
#define YY_Parser_DEBUG 1

#line 71 "/usr/share/bison++/bison.h"
/* YY_Parser_DEBUG */
#endif

#ifndef YY_Parser_LSP_NEEDED

 #line 75 "/usr/share/bison++/bison.h"

#line 75 "/usr/share/bison++/bison.h"
 /* YY_Parser_LSP_NEEDED*/
#endif

/* DEFAULT LTYPE*/
#ifdef YY_Parser_LSP_NEEDED
 #ifndef YY_Parser_LTYPE
  #ifndef BISON_YYLTYPE_ISDECLARED
   #define BISON_YYLTYPE_ISDECLARED
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;
  #endif

  #define YY_Parser_LTYPE yyltype
 #endif
#endif

/* DEFAULT STYPE*/
#ifndef YY_Parser_STYPE
 #define YY_Parser_STYPE int
#endif

/* DEFAULT MISCELANEOUS */
#ifndef YY_Parser_PARSE
 #define YY_Parser_PARSE yyparse
#endif

#ifndef YY_Parser_LEX
 #define YY_Parser_LEX yylex
#endif

#ifndef YY_Parser_LVAL
 #define YY_Parser_LVAL yylval
#endif

#ifndef YY_Parser_LLOC
 #define YY_Parser_LLOC yylloc
#endif

#ifndef YY_Parser_CHAR
 #define YY_Parser_CHAR yychar
#endif

#ifndef YY_Parser_NERRS
 #define YY_Parser_NERRS yynerrs
#endif

#ifndef YY_Parser_DEBUG_FLAG
 #define YY_Parser_DEBUG_FLAG yydebug
#endif

#ifndef YY_Parser_ERROR
 #define YY_Parser_ERROR yyerror
#endif

#ifndef YY_Parser_PARSE_PARAM
 #ifndef __STDC__
  #ifndef __cplusplus
   #ifndef YY_USE_CLASS
    #define YY_Parser_PARSE_PARAM
    #ifndef YY_Parser_PARSE_PARAM_DEF
     #define YY_Parser_PARSE_PARAM_DEF
    #endif
   #endif
  #endif
 #endif
 #ifndef YY_Parser_PARSE_PARAM
  #define YY_Parser_PARSE_PARAM void
 #endif
#endif

/* TOKEN C */
#ifndef YY_USE_CLASS

 #ifndef YY_Parser_PURE
  #ifndef yylval
   extern YY_Parser_STYPE YY_Parser_LVAL;
  #else
   #if yylval != YY_Parser_LVAL
    extern YY_Parser_STYPE YY_Parser_LVAL;
   #else
    #warning "Namespace conflict, disabling some functionality (bison++ only)"
   #endif
  #endif
 #endif


 #line 169 "/usr/share/bison++/bison.h"
#define	IntegerLiteral	258
#define	FloatingPointLiteral	259
#define	BooleanLiteral	260
#define	CharacterLiteral	261
#define	StringLiteral	262
#define	NullLiteral	263
#define	Identifier	264
#define	KeywordBoolean	265
#define	KeywordByte	266
#define	KeywordShort	267
#define	KeywordInt	268
#define	KeywordLong	269
#define	KeywordChar	270
#define	KeywordFloat	271
#define	KeywordDouble	272
#define	KeywordClass	273
#define	KeywordVoid	274
#define	KeywordIf	275
#define	KeywordElse	276
#define	KeywordThis	277
#define	KeywordSwitch	278
#define	KeywordCase	279
#define	KeywordDefault	280
#define	KeywordWhile	281
#define	KeywordBreak	282
#define	KeywordContinue	283
#define	KeywordReturn	284
#define	Keyword	285
#define	SeparatorLSqBrac	286
#define	SeparatorRSqBrac	287
#define	SeparatorDot	288
#define	SeparatorSemiCo	289
#define	SeparatorComma	290
#define	OperatorEq	291
#define	OperatorColon	292
#define	SeparatorLCuBrac	293
#define	SeparatorRCuBrac	294
#define	SeparatorLParen	295
#define	SeparatorRParen	296
#define	OperatorMiMi	297
#define	OperatorMiEq	298
#define	OperatorUp	299
#define	OperatorLeLeEq	300
#define	KeywordNew	301
#define	KeywordInstanceof	302
#define	OperatorVert	303
#define	OperatorDiv	304
#define	OperatorGrGrGr	305
#define	OperatorLe	306
#define	OperatorPlPl	307
#define	OperatorPl	308
#define	OperatorGrGrEq	309
#define	OperatorNE	310
#define	OperatorAmp	311
#define	OperatorExcl	312
#define	OperatorMi	313
#define	OperatorAnd	314
#define	OperatorUpEq	315
#define	OperatorAmpEq	316
#define	OperatorQm	317
#define	OperatorVertEq	318
#define	OperatorPlEq	319
#define	OperatorLeEq	320
#define	OperatorMul	321
#define	OperatorGrEq	322
#define	OperatorOr	323
#define	OperatorAsEq	324
#define	OperatorPercEq	325
#define	OperatorGr	326
#define	OperatorSlEq	327
#define	OperatorGrGrGrEq	328
#define	OperatorTild	329
#define	OperatorPerc	330
#define	OperatorGrGr	331
#define	OperatorEqEq	332
#define	OperatorLeLe	333
#define	KeywordFor	334
#define	KeywordDo	335


#line 169 "/usr/share/bison++/bison.h"
 /* #defines token */
/* after #define tokens, before const tokens S5*/
#else
 #ifndef YY_Parser_CLASS
  #define YY_Parser_CLASS Parser
 #endif

 #ifndef YY_Parser_INHERIT
  #define YY_Parser_INHERIT
 #endif

 #ifndef YY_Parser_MEMBERS
  #define YY_Parser_MEMBERS 
 #endif

 #ifndef YY_Parser_LEX_BODY
  #define YY_Parser_LEX_BODY  
 #endif

 #ifndef YY_Parser_ERROR_BODY
  #define YY_Parser_ERROR_BODY  
 #endif

 #ifndef YY_Parser_CONSTRUCTOR_PARAM
  #define YY_Parser_CONSTRUCTOR_PARAM
 #endif
 /* choose between enum and const */
 #ifndef YY_Parser_USE_CONST_TOKEN
  #define YY_Parser_USE_CONST_TOKEN 0
  /* yes enum is more compatible with flex,  */
  /* so by default we use it */ 
 #endif
 #if YY_Parser_USE_CONST_TOKEN != 0
  #ifndef YY_Parser_ENUM_TOKEN
   #define YY_Parser_ENUM_TOKEN yy_Parser_enum_token
  #endif
 #endif

class YY_Parser_CLASS YY_Parser_INHERIT
{
public: 
 #if YY_Parser_USE_CONST_TOKEN != 0
  /* static const int token ... */
  
 #line 212 "/usr/share/bison++/bison.h"
static const int IntegerLiteral;
static const int FloatingPointLiteral;
static const int BooleanLiteral;
static const int CharacterLiteral;
static const int StringLiteral;
static const int NullLiteral;
static const int Identifier;
static const int KeywordBoolean;
static const int KeywordByte;
static const int KeywordShort;
static const int KeywordInt;
static const int KeywordLong;
static const int KeywordChar;
static const int KeywordFloat;
static const int KeywordDouble;
static const int KeywordClass;
static const int KeywordVoid;
static const int KeywordIf;
static const int KeywordElse;
static const int KeywordThis;
static const int KeywordSwitch;
static const int KeywordCase;
static const int KeywordDefault;
static const int KeywordWhile;
static const int KeywordBreak;
static const int KeywordContinue;
static const int KeywordReturn;
static const int Keyword;
static const int SeparatorLSqBrac;
static const int SeparatorRSqBrac;
static const int SeparatorDot;
static const int SeparatorSemiCo;
static const int SeparatorComma;
static const int OperatorEq;
static const int OperatorColon;
static const int SeparatorLCuBrac;
static const int SeparatorRCuBrac;
static const int SeparatorLParen;
static const int SeparatorRParen;
static const int OperatorMiMi;
static const int OperatorMiEq;
static const int OperatorUp;
static const int OperatorLeLeEq;
static const int KeywordNew;
static const int KeywordInstanceof;
static const int OperatorVert;
static const int OperatorDiv;
static const int OperatorGrGrGr;
static const int OperatorLe;
static const int OperatorPlPl;
static const int OperatorPl;
static const int OperatorGrGrEq;
static const int OperatorNE;
static const int OperatorAmp;
static const int OperatorExcl;
static const int OperatorMi;
static const int OperatorAnd;
static const int OperatorUpEq;
static const int OperatorAmpEq;
static const int OperatorQm;
static const int OperatorVertEq;
static const int OperatorPlEq;
static const int OperatorLeEq;
static const int OperatorMul;
static const int OperatorGrEq;
static const int OperatorOr;
static const int OperatorAsEq;
static const int OperatorPercEq;
static const int OperatorGr;
static const int OperatorSlEq;
static const int OperatorGrGrGrEq;
static const int OperatorTild;
static const int OperatorPerc;
static const int OperatorGrGr;
static const int OperatorEqEq;
static const int OperatorLeLe;
static const int KeywordFor;
static const int KeywordDo;


#line 212 "/usr/share/bison++/bison.h"
 /* decl const */
 #else
  enum YY_Parser_ENUM_TOKEN { YY_Parser_NULL_TOKEN=0
  
 #line 215 "/usr/share/bison++/bison.h"
	,IntegerLiteral=258
	,FloatingPointLiteral=259
	,BooleanLiteral=260
	,CharacterLiteral=261
	,StringLiteral=262
	,NullLiteral=263
	,Identifier=264
	,KeywordBoolean=265
	,KeywordByte=266
	,KeywordShort=267
	,KeywordInt=268
	,KeywordLong=269
	,KeywordChar=270
	,KeywordFloat=271
	,KeywordDouble=272
	,KeywordClass=273
	,KeywordVoid=274
	,KeywordIf=275
	,KeywordElse=276
	,KeywordThis=277
	,KeywordSwitch=278
	,KeywordCase=279
	,KeywordDefault=280
	,KeywordWhile=281
	,KeywordBreak=282
	,KeywordContinue=283
	,KeywordReturn=284
	,Keyword=285
	,SeparatorLSqBrac=286
	,SeparatorRSqBrac=287
	,SeparatorDot=288
	,SeparatorSemiCo=289
	,SeparatorComma=290
	,OperatorEq=291
	,OperatorColon=292
	,SeparatorLCuBrac=293
	,SeparatorRCuBrac=294
	,SeparatorLParen=295
	,SeparatorRParen=296
	,OperatorMiMi=297
	,OperatorMiEq=298
	,OperatorUp=299
	,OperatorLeLeEq=300
	,KeywordNew=301
	,KeywordInstanceof=302
	,OperatorVert=303
	,OperatorDiv=304
	,OperatorGrGrGr=305
	,OperatorLe=306
	,OperatorPlPl=307
	,OperatorPl=308
	,OperatorGrGrEq=309
	,OperatorNE=310
	,OperatorAmp=311
	,OperatorExcl=312
	,OperatorMi=313
	,OperatorAnd=314
	,OperatorUpEq=315
	,OperatorAmpEq=316
	,OperatorQm=317
	,OperatorVertEq=318
	,OperatorPlEq=319
	,OperatorLeEq=320
	,OperatorMul=321
	,OperatorGrEq=322
	,OperatorOr=323
	,OperatorAsEq=324
	,OperatorPercEq=325
	,OperatorGr=326
	,OperatorSlEq=327
	,OperatorGrGrGrEq=328
	,OperatorTild=329
	,OperatorPerc=330
	,OperatorGrGr=331
	,OperatorEqEq=332
	,OperatorLeLe=333
	,KeywordFor=334
	,KeywordDo=335


#line 215 "/usr/share/bison++/bison.h"
 /* enum token */
     }; /* end of enum declaration */
 #endif
public:
 int YY_Parser_PARSE(YY_Parser_PARSE_PARAM);
 virtual void YY_Parser_ERROR(char *msg) YY_Parser_ERROR_BODY;
 #ifdef YY_Parser_PURE
  #ifdef YY_Parser_LSP_NEEDED
   virtual int  YY_Parser_LEX(YY_Parser_STYPE *YY_Parser_LVAL,YY_Parser_LTYPE *YY_Parser_LLOC) YY_Parser_LEX_BODY;
  #else
   virtual int  YY_Parser_LEX(YY_Parser_STYPE *YY_Parser_LVAL) YY_Parser_LEX_BODY;
  #endif
 #else
  virtual int YY_Parser_LEX() YY_Parser_LEX_BODY;
  YY_Parser_STYPE YY_Parser_LVAL;
  #ifdef YY_Parser_LSP_NEEDED
   YY_Parser_LTYPE YY_Parser_LLOC;
  #endif
  int YY_Parser_NERRS;
  int YY_Parser_CHAR;
 #endif
 #if YY_Parser_DEBUG != 0
  public:
   int YY_Parser_DEBUG_FLAG;	/*  nonzero means print parse trace	*/
 #endif
public:
 YY_Parser_CLASS(YY_Parser_CONSTRUCTOR_PARAM);
public:
 YY_Parser_MEMBERS 
};
/* other declare folow */
#endif


#if YY_Parser_COMPATIBILITY != 0
 /* backward compatibility */
 /* Removed due to bison problems
 /#ifndef YYSTYPE
 / #define YYSTYPE YY_Parser_STYPE
 /#endif*/

 #ifndef YYLTYPE
  #define YYLTYPE YY_Parser_LTYPE
 #endif
 #ifndef YYDEBUG
  #ifdef YY_Parser_DEBUG 
   #define YYDEBUG YY_Parser_DEBUG
  #endif
 #endif

#endif
/* END */

 #line 267 "/usr/share/bison++/bison.h"
#endif
