
HW2 - CS335
===========
+-----------------------------------------------------------------------------+
| 1. Installation Instructions:                                               |
+-----------------------------------------------------------------------------+

If dotty isn't showing lables, try installing xfonts-100dpi:
sudo apt-get install xfonts-100dpi

Installations:
sudo apt-get install flex-old bison++

To get back the original configuration:
sudo apt-get purge bison++ flex-old
sudo apt-get install flex bison

+-----------------------------------------------------------------------------+
| 2. Running the parser:                                                      |
+-----------------------------------------------------------------------------+

$ cd asgn2
$ make
$ bin/parser test/test1.java
$ dotty test/test1.dot

For compressed parsetree
$ cd asgn2
$ make compressed
$ bin/compressedparser test/test1.java
$ dotty test/test1.dot

To clean all binary/compiled files
$ cd asgn2
$ make clean

+-----------------------------------------------------------------------------+
| 3. Features:                                                                |
+-----------------------------------------------------------------------------+

* Supported features : 
	- all primitive data types are supported
	- all literals
	- class body, method declaration
	- labels with break and continue 
	- do while loop, for loop, while loop
	- if-then-else, if-then, switch constructs
	- ternary operator
	- almost all expressions supported
	- Object Oriented features:
		+ multiple classes can be declared in a file
		+ class instances can be created
		+ qualified name for methods and class invocation rules
 
* Have removed the static initialiser and all modifiers
* Don't support "super"	

...........................
: Generating the dot file :
...........................

We have created a tree data structure (node.hpp) to store the parse tree. The 
nodes of the tree are added to it whenever we reduce using a rule. This is 
specified in the action block of the corresponding rule. We also store the 
information about the parent node of each node when we are adding it to the 
tree. We then perform a Breadth First Search on this tree to get the dot file 
in the required format. This entire process is done in-memmory instead of using 
the debug mode in bison++.

Due to the structure of the grammar, the parse tree often contains long chains 
of nodes, even without which the syntactx is almost valid. So, we have added 
another parsing file which removes unnecessary non-terminals from the parse 
tree rendering it aesthetically pleasing.

+-----------------------------------------------------------------------------+
| 5. Error Handling:                                                          |
+-----------------------------------------------------------------------------+

We report both lexing and parsing errors which we are identifying at the 
command prompt. We also give a count of both types of errors.

Parser detects the following syntactic errors:
1. errors in for loop header
2. ill-formed expressions (addition, subtraction etc.)
3. missing semicolons

ERROR_VERBOSE works best only if error rules are written. Also, yyerrok is a 
macro defined automatically by Bison on using error rules. So, no need to 
write it. Its meaning is that error recovery is complete. Even if the rule is 
reduced, yyerror function is still called upon to print its message.

We have added support for some new lexical errors along with the ones which 
we were reporting earlier. For example, we have removed some of the keywords, 
separators, and operators from the previous implemetation and now instead 
report their usage as errors.
