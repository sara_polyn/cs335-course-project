class FinalTest{
	void main(String args[])
	{
		//if-then-else, identifier, assignment op
		int i;
		if(1 > 2)
		{
			i=2;
		}
		else
		{
			i=2;
		}
		//while
		while(true)
		{
			i = i+1;
			if(i>3)break;
		}
		//dowhile
		do{
			i = i-1;
		}
		while(i>0);
		//array
		int arr[] = new int[100];
		
		//func call
		i = func(i);
		//constant
		double INTEREST = 10.0d;
		alpha = 1.0e4;
		charbeta = 0x25FF;
		char cc = 'c';
		str = "H\"ello \n ok";
		// comment
		/*
		   This is a multiline comment
		   yay!
		   more lines
		   */
		boolean val;
		//switch
		switch(i)
		{
			case 1: {
					val = true;
					break;
				}
			default:{
					val = false;
					break;
			}

		}
		//operators:
		//unary:
		i = -1;
		//binary:
		i = 1*2;
	}
	func(int inp)
	{
		inp = 10;
		return inp;
	}
}
