/*
Class for Symbol Table
Symbol Table Entry class
Maintains internal hash table / map
Unordered map seems useful
All functions shuld be here
*/
#ifndef _SymbTab_
#define _SymbTab_
#include<string>
#include<map>
#include<assert.h>
#include<vector>

using namespace std;

class ST;	//for STE to know it exists

class STE{
	public:
		string name;
		string type;
		string value;
		int offset;
		int size;	//TODO compute
		bool initialised;
		bool isArray;
		bool isFunc; //could be done using enum
		vector<string> * arrayDims;
		vector<string> * funcRets;
		vector<string> *paramslist;
		ST * tab;
		STE(const string &n, const string& t, const string &v, int s){
			name = n;
			type = t;
			value = v;
			offset = s;
			initialised = true;
			tab = NULL;
		}
		STE(const string &n, const string &t, bool v, int s){
			name = n;
			type = t;
			initialised = v;
			offset = s;
			tab = NULL;
		}
		STE(){tab = NULL;};
};

class ST{
	private:
		map<const string,STE> binSymTab;
		ST * parent;
		//TODO do we need own name
		int width;
	public:
		string name;
		string type;
		ST(ST * par);
		void enter(const string &name, const string &type, const string &value, int offset); // creates a new entry
		void enter(const string &name, const string &type, bool value, int offset);
		void enter(const string &name,const string& type, ST * tab);
		void addWidth(int width);	// records cumulative width of all entries
		int getWidth(){ return width; }
		void enterProc(const string &name, ST* newtable);  // creates a new entry for procedure name. newtable points to the symbol table of the new procedure
		bool lookup(const string &name);   //Checks ifentry with this name exsts in the table
		bool lookupLoc(const string &name);
		//IMPORTANT: Prereq before every getType, getVal etc
		STE getSTE(const string &name);
		void print(int sp);//debugging purpose
};
#endif
