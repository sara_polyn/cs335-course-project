#include <string>
#include <vector>

using namespace std;

class Node{
	private:
	string val;
	string type;
	bool array;
	int arrayDim;
	int quad;

	public:
	int nodeNo;
	string attributeType; //type of this node
	string attributeName; //name (for vars etc)
	string attributeVal; //value for this node
	string attributePlace; //place for this node
	bool hasVal;
	vector<Node *> child;
	vector<string> * arrayDimVals;
	vector<int> trueList;
	vector<int> falseList;
	vector<int> nextList;
	vector<int> breakList;
	vector<string> *paramslist;
	Node(string tokenType, string tokenVal)
	{
		type = tokenType; 
		val = tokenVal;
		hasVal = false;
		array = false;
		arrayDim = 0;
		trueList.clear();
		falseList.clear();
		nextList.clear();
		breakList.clear();
	}
	~Node()
	{
	}

	string getNodeType()
	{
		return type;
	}

	string getNodeVal()
	{
		return val;
	}
	
	void addChild(Node * c)
	{
		child.push_back(c);
	}

	void setType(const string & s)
	{
		attributeType = s;
	}
	string getType()
	{
		return attributeType;
	}
	void setName(const string & s)
	{
		attributeName = s;
	}
	string getName()
	{
		return attributeName;
	}

	void setVal(const string & s)
	{
		attributeVal = s;
		hasVal = true;
	}
	string getVal()
	{
		return attributeVal;
	}
	void setArray()
	{
		array = true;
		arrayDimVals = new vector<string>(0);
	}
	bool isArray()
	{
		return array;
	}
	void incDim(int d)
	{
		arrayDim = arrayDim+d;
	}
	int getDim()
	{
		return arrayDim;
	}
	void addDimVal(string v)
	{
		arrayDimVals->push_back(v);
	}
	vector<string> * getArrayDimVals()
	{
		return arrayDimVals;
	}
	void setDimVal(vector<string> * v)
	{
		arrayDimVals = v;
	}
	void setQuad(int pos)
	{
		quad = pos;
	} 
	int getQuad()
	{
		return quad;
	} 
	void setNextList(vector<int> v)
	{
		nextList = v;
	}
	vector<int> getNextList()
	{
		return nextList;
	}
	void setBreakList(vector<int> v)
	{
		breakList = v;
	}
	vector<int> getBreakList()
	{
		return breakList;
	}
	void setTrueList(vector<int> v)
	{
		trueList = v;
	}
	vector<int> getTrueList()
	{
		return trueList;
	}
	void setFalseList(vector<int> v)
	{
		falseList = v;
	}
	vector<int> getFalseList()
	{
		return falseList;
	}
	void setPlace(const string &s)
	{
		attributePlace = s;
	}
	string getPlace()
	{
		return attributePlace;
	}
};
