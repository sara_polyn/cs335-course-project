#include "parser.h"
#define YY_DECL int yyFlexLexer::yylex(YY_Parser_STYPE *val)
#include "FlexLexer.h"
#include <stdio.h>

class ParseTree : public Parser
{
private:
  yyFlexLexer theLexer;
 public:
  virtual int yylex();
  virtual void yyerror(char *m);
  ParseTree(){initializeError();}
};

int ParseTree::yylex()
{
 return theLexer.yylex(&yylval);
}

void ParseTree::yyerror(char *m)
{ fprintf(stderr,"%d: %s at token '%s'\n",theLexer.lineno(), m, theLexer.YYText());
}

int main(int argc,char **argv)
{
 if(argc != 2)
 {
	 cout<<"Invalid invocation. Usage bin/parser <filename>"<<endl;
	 return -1;
 }
 else
 {
	 if(freopen(argv[1], "r", stdin)==NULL)
	 {
	 	cout<<"File not found"<<endl;
	 	return -1;
	 }

	 ParseTree aCompiler;
	 init();
	 int result=aCompiler.yyparse();
       	 cout<<endl;
	 if(result == 0 && getErrorCount()==0 && getTypeError()==0 && otherErrorCount()==0)
	 {
		 string filename(argv[1]);
		 int posOfDot = filename.find('.');
		 string outName = filename.substr(0, posOfDot);
		 outName += ".dot";
		 //printToDot(outName); 
		// cout<<"Success, dotfile "<<outName<<" created"<<endl;
		 printSymbTable();
	 	 print3AC();
		 cerr<<"Compiled Successfully"<<endl;
	 }
	 else
	 {
		cout<<"Failed to parse the file [ERROR]: (Lexing) "<<getErrorCount()<<" error(s), (Parsing) "<< aCompiler.yynerrs<<" error(s)"<< endl;
		cout<<"Type Errors: "<<getTypeError()<<endl;
		cout<<"Other Errors:"<<endl;
		printOtherError();
	 }
 }
 return 0;
}

