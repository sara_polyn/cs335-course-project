%{
#ifndef FLEXFIX
#define FLEXFIX YY_Parser_STYPE *val
#define FLEXFIX2 val
#endif
#include "parser.h"
%}

%x IN_COMMENT IN_STRING

LineTerminator	\n|\r|\r\n
EscapeSequence	\\b|\\\\|\\t|\\'|\\n|\\f|\\\"|\\r
Keyword		 "abstract"|"continue"|"for"|"new"|"switch"|"assert"|"default"|"if"|"package"|"synchronized"|"boolean"|"do"|"goto"|"private"|"this"|"break"|"double"|"implements"|"protected"|"throw"|"byte"|"else"|"import"|"public"|"throws"|"case"|"enum"|"instanceof"|"return"|"transient"|"catch"|"extends"|"int"|"short"|"try"|"char"|"final"|"interface"|"static"|"void"|"class"|"finally"|"long"|"strictfp"|"volatile"|"const"|"float"|"native"|"super"|"while"
KeywordBoolean 	"boolean"
KeywordInt 	"int"
KeywordChar 	"char"
KeywordFloat 	"float"
KeywordDouble 	"double"
KeywordClass 	"class"
KeywordVoid	"void"
KeywordIf	"if"
KeywordElse	"else"
KeywordSwitch	"switch"
KeywordCase	"case"
KeywordDefault	"default"
KeywordWhile	"while"
KeywordBreak	"break"
KeywordContinue	"continue"
KeywordReturn	"return"
KeywordThis	"this"
KeywordNew	"new"
KeywordFor	"for"
KeywordDo 	"do"

SeparatorLSqBrac "["
SeparatorRSqBrac "]"
SeparatorDot	 "."
SeparatorSemiCo  ";"
SeparatorLCuBrac "{"
SeparatorRCuBrac "}"
SeparatorComma	 ","
SeparatorLParen	 "("
SeparatorRParen	 ")"
Separator 	"("|")"|"{"|"}"|"["|"]"|";"|","|"."|"..."|"@"|"::"

OperatorEq	"="
OperatorColon	":"
OperatorPl 	"+"
OperatorMi 	"-"
OperatorExcl 	"!"
OperatorTild 	"~"
OperatorMul	"*"
OperatorDiv	"/"
OperatorPerc	"%"
OperatorLeLe 	"<<"
OperatorGrGr	">>"
OperatorGrGrGr 	">>>"
OperatorLe 	"<"
OperatorGr 	">"
OperatorLeEq 	"<="
OperatorGrEq 	">="
OperatorEqEq 	"=="
OperatorNE 	"!="
OperatorAmp	"&"
OperatorUp	"^"
OperatorVert	"|"
OperatorAnd	"&&"
OperatorOr	"||"
OperatorAsEq	"*=" 
OperatorSlEq	"/="
OperatorPercEq	"%="
OperatorPlEq	"+="
OperatorMiEq	"-="
OperatorLeLeEq	"<<="
OperatorGrGrEq 	">>="
OperatorGrGrGrEq	">>>="
OperatorAmpEq	"&="
OperatorUpEq	"^="
OperatorVertEq	"|="
Operator	"="|">"|"<"|"!"|"~"|"?"|":"|"->"|"=="|">="|"<="|"!="|"&&"|"||"|"++"|"--"|"+"|"-"|"*"|"/"|"&"|"|"|"^"|"%"|"<<"|">>"|">>>"|"+="|"-="|"*="|"/="|"&="|"|="|"^="|"%="|"<<="|">>="|">>>="
Identifier 	[a-zA-Z][a-zA-Z0-9_$]*


Digit 					[0-9]
Digits					{Digit}*
HexDigit				[0-9a-fA-F]
HexDigits				{HexDigit}+
OctalDigit				[0-7]
BinaryDigit				[01]
NonZeroDigit				[1-9]
IntegerTypeSuffix 			[lL]
DecimalNumeral				0|{NonZeroDigit}{Digits}
DecimalIntegerLiteral			{DecimalNumeral}{IntegerTypeSuffix}?
HexIntegerLiteral			"0"[xX]{HexDigits}
OctalIntegerLiteral			"0"{OctalDigit}+
BinaryIntegerLiteral			"0"[bB]{BinaryDigit}+

HexIntegerError				"0"[xX]

Sign 					[+-]
SignedInteger 				{Sign}?{Digits}
ExponentIndicator			[eE]
BinaryExponentIndicator			[pP]
FloatTypeSuffix 			[fFdD]
HexNumeral				{HexIntegerLiteral}
HexSignificand				({HexNumeral}(".")?)|("0"[xX]{HexDigits}?"."{HexDigits})
BinaryExponent				{BinaryExponentIndicator}{SignedInteger}
HexadecimalFloatingPointLiteral 	{HexSignificand}{BinaryExponent}{FloatTypeSuffix}?	
ExponentPart				{ExponentIndicator}{SignedInteger}
DFPLType1				{Digits}"."{Digits}?{ExponentPart}?{FloatTypeSuffix}?
DFPLType2				"."{Digits}{ExponentPart}?{FloatTypeSuffix}?
DFPLType3				{Digits}{ExponentPart}{FloatTypeSuffix}?
DFPLType4				{Digits}{ExponentPart}?{FloatTypeSuffix}
DecimalFloatingPointLiteral		{DFPLType1}|{DFPLType2}|{DFPLType3}|{DFPLType4}
FloatingPointLiteral 			{DecimalFloatingPointLiteral}|{HexadecimalFloatingPointLiteral}

BooleanLiteral 		"true"|"false"
NullLiteral		"null"

SingleCharacter		[^\\\'\n\r(\r\n)]
CharacterLiteral 	\'({SingleCharacter}|{EscapeSequence})\'

%%
<INITIAL>{
	{LineTerminator}			yylineno++; //return Parser::LINE_TERMINATOR;	
	\/\/.*					;//return Parser::COMMENT_SINGLELINE;
	{SeparatorLSqBrac}			{ 
							val->node_type = new Node("SeparatorLSqBrac", string(yytext));
							return Parser::SeparatorLSqBrac;
						}
	{SeparatorRSqBrac}			{ 
							val->node_type = new Node("SeparatorRSqBrac", string(yytext));
							return Parser::SeparatorRSqBrac;
						}
	{SeparatorDot}			{ 
							val->node_type = new Node("SeparatorDot", string(yytext));
							return Parser::SeparatorDot;
						}

	{SeparatorSemiCo}			{ 
							val->node_type = new Node("SeparatorSemiCo", string(yytext));
							return Parser::SeparatorSemiCo;
						}
	{SeparatorComma}			{ 
							val->node_type = new Node("SeparatorComma", string(yytext));
							return Parser::SeparatorComma;
						}
	{SeparatorLCuBrac}			{ 
							val->node_type = new Node("SeparatorLCuBrac", string(yytext));
							return Parser::SeparatorLCuBrac;
						}
	{SeparatorRCuBrac}			{ 
							val->node_type = new Node("SeparatorRCuBrac", string(yytext));
							return Parser::SeparatorRCuBrac;
						}
	{SeparatorLParen}			{ 
							val->node_type = new Node("SeparatorLParen", string(yytext));
							return Parser::SeparatorLParen;
						}
	{SeparatorRParen}			{ 
							val->node_type = new Node("SeparatorRParen", string(yytext));
							return Parser::SeparatorRParen;
						}
	{Separator}				{
								incError();
								fprintf(stderr,"%d: Unsupported separator %s, ignoring and continuing\n",yylineno, yytext);
						}

	{OperatorEq}		{
							val->node_type = new Node("OperatorEq", string(yytext));
							return Parser::OperatorEq;
						}
	{OperatorColon}				{
							val->node_type = new Node("OperatorColon", string(yytext));
							return Parser::OperatorColon;
						}
	{OperatorPl}		{
							val->node_type = new Node("OperatorPl", string(yytext));
							return Parser::OperatorPl;
						}
	{OperatorMi}		{
							val->node_type = new Node("OperatorMi", string(yytext));
							return Parser::OperatorMi;
						}
    {OperatorTild}		{
							val->node_type = new Node("OperatorTild", string(yytext));
							return Parser::OperatorTild;
						}
	{OperatorExcl}		{
							val->node_type = new Node("OperatorExcl", string(yytext));
							return Parser::OperatorExcl;
						}
	{OperatorMul}		{
							val->node_type = new Node("OperatorMul", string(yytext));
							return Parser::OperatorMul;
						}
	{OperatorDiv}		{
							val->node_type = new Node("OperatorDiv", string(yytext));
							return Parser::OperatorDiv;
						}
	{OperatorPerc}		{
							val->node_type = new Node("OperatorPerc", string(yytext));
							return Parser::OperatorPerc;
						}
	{OperatorLeLe}		{
							val->node_type = new Node("OperatorLeLe", string(yytext));
							return Parser::OperatorLeLe;
						}
	{OperatorGrGr}		{
							val->node_type = new Node("OperatorGrGr", string(yytext));
							return Parser::OperatorGrGr;
						}
	{OperatorGrGrGr}		{
							val->node_type = new Node("OperatorGrGrGr", string(yytext));
							return Parser::OperatorGrGrGr;
						}
	{OperatorLe}		{
							val->node_type = new Node("OperatorLe", string(yytext));
							return Parser::OperatorLe;
						}
	{OperatorGr}		{
							val->node_type = new Node("OperatorGr", string(yytext));
							return Parser::OperatorGr;
						}
	{OperatorLeEq}		{
							val->node_type = new Node("OperatorLeEq", string(yytext));
							return Parser::OperatorLeEq;
						}
	{OperatorGrEq}		{
							val->node_type = new Node("OperatorGrEq", string(yytext));
							return Parser::OperatorGrEq;
						}
	{OperatorEqEq}		{
							val->node_type = new Node("OperatorEqEq", string(yytext));
							return Parser::OperatorEqEq;
						}
	{OperatorNE}		{
							val->node_type = new Node("OperatorNE", string(yytext));
							return Parser::OperatorNE;
						}
	{OperatorAmp}		{
							val->node_type = new Node("OperatorAmp", string(yytext));
							return Parser::OperatorAmp;
						}

	{OperatorUp}		{
							val->node_type = new Node("OperatorUp", string(yytext));
							return Parser::OperatorUp;
						}
	{OperatorVert}		{
							val->node_type = new Node("OperatorVert", string(yytext));
							return Parser::OperatorVert;
						}
	{OperatorAnd}		{
							val->node_type = new Node("OperatorAnd", string(yytext));
							return Parser::OperatorAnd;
						}
	{OperatorOr}		{
							val->node_type = new Node("OperatorOr", string(yytext));
							return Parser::OperatorOr;
						}
	{OperatorAsEq}		{
							val->node_type = new Node("OperatorAsEq", string(yytext));
							return Parser::OperatorAsEq;
						}
	{OperatorSlEq}		{
							val->node_type = new Node("OperatorSlEq", string(yytext));
							return Parser::OperatorSlEq;
						}
	{OperatorPercEq}		{
							val->node_type = new Node("OperatorPercEq", string(yytext));
							return Parser::OperatorPercEq;
						}
	{OperatorPlEq}		{
							val->node_type = new Node("OperatorPlEq", string(yytext));
							return Parser::OperatorPlEq;
						}
	{OperatorMiEq}		{
							val->node_type = new Node("OperatorMiEq", string(yytext));
							return Parser::OperatorMiEq;
						}
	{OperatorLeLeEq}		{
							val->node_type = new Node("OperatorLeLeEq", string(yytext));
							return Parser::OperatorLeLeEq;
						}
	{OperatorGrGrEq}		{
							val->node_type = new Node("OperatorGrGrEq", string(yytext));
							return Parser::OperatorGrGrEq;
						}
	{OperatorGrGrGrEq}		{
							val->node_type = new Node("OperatorGrGrGrEq", string(yytext));
							return Parser::OperatorGrGrGrEq;
						}
	{OperatorAmpEq}		{
							val->node_type = new Node("OperatorAmpEq", string(yytext));
							return Parser::OperatorAmpEq;
						}
	{OperatorUpEq}		{
							val->node_type = new Node("OperatorUpEq", string(yytext));
							return Parser::OperatorUpEq;
						}
	{OperatorVertEq}		{
							val->node_type = new Node("OperatorVertEq", string(yytext));
							return Parser::OperatorVertEq;
						}
	{Operator}			{
								incError();
								fprintf(stderr,"%d: Unsupported operator %s, ignoring and continuing\n",yylineno, yytext);
						}

	[ \t\f]					;
	\"					BEGIN(IN_STRING); yymore();
	{KeywordBoolean}			{
							val->node_type = new Node("KeywordBoolean", string(yytext));
							return Parser::KeywordBoolean;
						}

	{KeywordInt}		{
							val->node_type = new Node("KeywordInt", string(yytext));
							return Parser::KeywordInt;
						}
	{KeywordChar}		{
							val->node_type = new Node("KeywordChar", string(yytext));
							return Parser::KeywordChar;
						}

	{KeywordFloat}				{
							val->node_type = new Node("KeywordFloat", string(yytext));
							return Parser::KeywordFloat;
						}

	{KeywordDouble}				{
							val->node_type = new Node("KeywordDouble", string(yytext));
							return Parser::KeywordDouble;
						}
	{KeywordClass}				{
							val->node_type = new Node("KeywordClass", string(yytext));
							return Parser::KeywordClass;
						}
	{KeywordVoid}				{
							val->node_type = new Node("KeywordVoid", string(yytext));
							return Parser::KeywordVoid;
						}

	{KeywordIf}				{
							val->node_type = new Node("KeywordIf", string(yytext));
							return Parser::KeywordIf;
						}
	{KeywordElse}				{
							val->node_type = new Node("KeywordElse", string(yytext));
							return Parser::KeywordElse;
						}
	{KeywordSwitch}				{
							val->node_type = new Node("KeywordSwitch", string(yytext));
							return Parser::KeywordSwitch;
						}
	{KeywordCase}				{
							val->node_type = new Node("KeywordCase", string(yytext));
							return Parser::KeywordCase;
						}
	{KeywordDefault}				{
							val->node_type = new Node("KeywordDefault", string(yytext));
							return Parser::KeywordDefault;
						}
	{KeywordWhile}				{
							val->node_type = new Node("KeywordWhile", string(yytext));
							return Parser::KeywordWhile;
						}
	{KeywordBreak}				{
							val->node_type = new Node("KeywordBreak", string(yytext));
							return Parser::KeywordBreak;
						}
	{KeywordContinue}				{
							val->node_type = new Node("KeywordContinue", string(yytext));
							return Parser::KeywordContinue;
						}
	{KeywordReturn}				{
							val->node_type = new Node("KeywordReturn", string(yytext));
							return Parser::KeywordReturn;
						}
	{KeywordThis}				{
							val->node_type = new Node("KeywordThis", string(yytext));
							return Parser::KeywordThis;
						}
	{KeywordNew}				{
							val->node_type = new Node("KeywordNew", string(yytext));
							return Parser::KeywordNew;
						}
	{KeywordFor}		{
							val->node_type = new Node("KeywordFor", string(yytext));
							return Parser::KeywordFor;
						}
	{KeywordDo}			{
							val->node_type = new Node("KeywordDo", string(yytext));
							return Parser::KeywordDo;
						}

	{Keyword}				{
								incError();
								fprintf(stderr,"%d: Unsupported keyword %s, ignoring and continuing\n",yylineno, yytext);
						}

	{BooleanLiteral}			{
							val->node_type = new Node("BooleanLiteral", string(yytext));
							return Parser::BooleanLiteral;
						}

	{NullLiteral}				{
							val->node_type = new Node("NullLiteral", string(yytext));
							return Parser::NullLiteral;
						}
	{Identifier}				{
							val->node_type = new Node("Identifier", string(yytext));
							return Parser::Identifier;
						}

	{CharacterLiteral} 			{
							val->node_type = new Node("CharacterLiteral", string(yytext));
							return Parser::CharacterLiteral;
						}
	\'\'\'|\'          	{
							incError();
							fprintf(stderr,"%d: Unexpected singlequote %s, ignoring and continuing\n",yylineno, yytext);
						} 
	\'[^\'\n\r(\r\n)]*\' {
							incError();
							fprintf(stderr,"%d: incorrect format for character %s, ignoring and continuing\n",yylineno, yytext);
						}
	"/*"            	        	BEGIN(IN_COMMENT); yymore();



	{DecimalIntegerLiteral}			{
							val->node_type = new Node("DecimalIntegerLiteral", string(yytext));
							return Parser::IntegerLiteral;
	                                        }

	{HexIntegerLiteral}		        {
							val->node_type = new Node("HexIntegerIntegerLiteral", string(yytext));
							return Parser::IntegerLiteral;
	                                        }

	{OctalIntegerLiteral}			{
							val->node_type = new Node("OctalIntegerLiteral", string(yytext));
							return Parser::IntegerLiteral;
	                                        }

	{BinaryIntegerLiteral}			{
							val->node_type = new Node("BinaryIntegerLiteral", string(yytext));
							return Parser::IntegerLiteral;
	                                        }
	{FloatingPointLiteral}			{
							val->node_type = new Node("FloatingPointLiteral", string(yytext));
							return Parser::FloatingPointLiteral;
						}
	
	{HexIntegerError}	{
							incError();
							fprintf(stderr,"%d: hex must have atleast one digit following x : %s, ignoring and continuing\n",yylineno, yytext);
						}

	.					 {
							incError();
							fprintf(stderr,"%d: Unexpected Character %s, ignoring and continuing\n",yylineno, yytext);
						}
}
<IN_COMMENT>{
	"*/"    				{
							BEGIN(INITIAL);	
						}
	<<EOF>>				{
							incError();
							fprintf(stderr,"%d: encountered unexpected EOF while parsing comment, ignoring and continuing\n", yylineno);
						}
	{LineTerminator}			yylineno++; yymore();
	.   					yymore();
}
<IN_STRING>{
	\"					{

							BEGIN(INITIAL);
							val->node_type = new Node("StringLiteral", string(yytext));	
							return Parser::StringLiteral;
						}
	{EscapeSequence}				yymore();
	\\					{
							incError();
							fprintf(stderr,"%d: unexpected \\, ignoring and continuing\n", yylineno);
							BEGIN(INITIAL); 
						}
	<<EOF>>				{
							incError();
							fprintf(stderr,"%d: encountered unexpected EOF while parsing comment, ignoring and continuing\n", yylineno);
						}
	{LineTerminator}			{
							incError();
							fprintf(stderr,"%d: unterminated string, ignoring and continuing\n", yylineno);
							yylineno++; 
							yytext[yyleng-1] = '\0'; 
							BEGIN(INITIAL); 
						}
	.         				yymore();
}

%%

int yywrap(void)
{
	return  1;
}
