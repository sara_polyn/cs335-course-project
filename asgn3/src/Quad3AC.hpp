#ifndef __QUADS__
#define __QUADS__

/**
Class for storing quads of 3AC
**/

#include<string>
#include<vector>
#include<sstream>
using namespace std;

enum TACType{
	JUMP,
	JUMPLABEL,
	ARITH,
	IFCOMPGOTO,
	IFGOTO,
	ASSIGN,
	CONVERT,
	BOOLEXP,
	PRINTSTR,
	INITARRAY,
	METHODLBL,
	LOCAL_FUNCCALL,
	LOCAL_FUNCCALL_ASSIGN,
	RETURN,
	PUSHPARAM,
	PRINT
};

enum OPType{
	NOP,
	GOTO, 
	GOTOLBL,
	FLOATADD,
	INTADD,
	CHARADD,
	FLOATSUB,
	INTSUB,
	CHARSUB,
	FLOATMULT,
	INTMULT,
	FLOATDIV,
	INTDIV,
	FLOATMOD,
	INTMOD,
	SHIFTL,
	SHIFTR,
	BITAND,
	BITOR,
	BITXOR,
	LESSTHAN,
	GRTHAN,
	LESSEQ,
	GREQ,
	EQ,
	EQASSIGN,
	MINUSASSIGN,
	INTTOFLOAT,
	FLOATTOINT,
	INTTOCHAR,
	CHARTOINT,
	UNARYNOT,
	UNARYTILDE,
	BOOLAND,
	BOOLOR
};
class Quad3AC{
	private:
	TACType type; 
	int instNo;
	string result;

	string op2;
	OPType opt;
	public:
			string op1;
	Quad3AC(int n, TACType t, OPType o, const string& s1, const string& s2, const string& res):instNo(n), type(t), opt(o), op1(s1), op2(s2), result(res)
	{}
	Quad3AC()
	{};
	void updateA1(const string & st)
	{
		op1 = st;
	}
	void updateA2(const string & st)
	{
		op2 = st;
	}
	void print();
};

class QuadsList{
	private:
	vector<Quad3AC> * qdl;
	int nextInstr; //could use a static int for these
	int lastTemp;
	public:
	QuadsList()
	{
		nextInstr = 0;
		lastTemp = 0;
		qdl = new vector<Quad3AC>(0);	
	}
	int getNextInstr()
	{
		return nextInstr;
	}
	string NoToStr(int n) //Isnt there a beter way? :(
	{
		ostringstream ss;
     		ss << n;
     		return ss.str();
	}
	void emit(const string& op1, TACType t, OPType op,const string &op2="" ,const string& res="");
	void backpatch(const vector<int>& list, int to);
	void backpatch(int stmt, int to);
	string getNewTemp();
	void printQuads();
	void updateMethodLine(int line, int val);
};

#endif
