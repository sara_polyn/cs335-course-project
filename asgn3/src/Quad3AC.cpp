#include"Quad3AC.hpp"

void QuadsList::emit(const string& op1, TACType t, OPType op,const string &op2,const string& res)
{
	Quad3AC q(nextInstr, t, op, op1, op2, res);
	qdl->push_back(q);
	nextInstr++;
}

#include<iostream>
void QuadsList::backpatch(const vector<int> & l, int to)
{
	string toStr = NoToStr(to);
	int size = l.size();
	for(int i = 0; i<size;++i)
	{
		qdl->at(l[i]).updateA1(toStr);
	}
}

void QuadsList::backpatch(int stmt, int to)
{
	string toStr = NoToStr(to);
	qdl->at(stmt).updateA1(toStr);
}

string QuadsList::getNewTemp()
{
	string prefix = "_t";	//cannot conflict with java vars?
	string var = prefix + NoToStr(lastTemp);
	lastTemp++;
	return var;
}

#include<iostream>
void QuadsList::printQuads()
{
	cout<<"No of quads:"<<qdl->size()<<endl;
	for(int i=0;i<qdl->size();++i)
	{
		qdl->at(i).print();
	}
}

void Quad3AC::print()	//TODO
{
	string temp;
	switch(type)
	{
		case JUMP:
			cout<<instNo<<":GOTO "<<op1<<endl;
			break;
		case JUMPLABEL:
			cout<<instNo<<":LABEL "<<op1<<endl;
			break;
		case ARITH:
			switch(opt)
			{
				case FLOATADD:
				case INTADD:
				case CHARADD:
				case FLOATSUB:		temp = " + "; break;
				case INTSUB:
				case CHARSUB:		temp = " - "; break;
				case FLOATMULT:
				case INTMULT:		temp = " * "; break;
				case FLOATDIV:
				case INTDIV:		temp = " / "; break;
				case FLOATMOD:
				case INTMOD:		temp = " % "; break;
				case SHIFTL:		temp = " << "; break;
				case SHIFTR:		temp = " >> "; break;
				case BITAND:		temp = " & "; break;
				case BITOR:			temp = " | "; break;
				case BITXOR:		temp = " ^ "; break;
				default:			temp = ""; break;
			}
			cout<<instNo<<":"<<result<<" = "<<op1<<temp<<op2<<endl;
			break;
		case IFCOMPGOTO:
			switch(opt)
			{
				case LESSTHAN:		temp = " < "; break;
				case GRTHAN:		temp = " > "; break;
				case LESSEQ:		temp = " >= "; break;
				case GREQ:			temp = " >= "; break;
				case EQ:			temp = " == "; break;
				default:			temp = ""; break;
			}
			cout<<instNo<<":IF "<<op1<<temp<<op2<<" GOTO "<<result<<endl;
			break;
		case IFGOTO:
			cout<<instNo<<":IF "<<op1<<" GOTO "<<op2<<endl;
			break;
		case ASSIGN:
			switch(opt)
			{
				case EQASSIGN:		temp = " = "; break;
				case MINUSASSIGN:	temp = " = -"; break;
				case UNARYNOT:		temp = " = !"; break;
				case UNARYTILDE:	temp = " = ~"; break;
				case FLOATTOINT:	temp = " = (floattoint)"; break;
				case INTTOFLOAT:	temp = " = (inttofloat)"; break;
				case CHARTOINT:		temp = " = (chartoint)"; break;
				case INTTOCHAR:		temp = " = (inttochar)"; break;
				default:			temp = ""; break;
			}
			cout<<instNo<<":"<<op1<<temp<<op2<<endl;
			break;
		//case CONVERT:
		case BOOLEXP:
			switch(opt)
			{
				case BOOLAND:		temp = " AND "; break;
				case BOOLOR:		temp = " OR "; break;
				default:			temp = ""; break;
			}
			cout<<instNo<<":"<<result<<" = "<<op1<<temp<<op2<<endl;
			break;
		case PRINTSTR:
			cout<<instNo<<":"<<"PRINTSTR"<<op1<<endl;
			break;
		case INITARRAY:
			cout<<instNo<<":"<<op1<<" = "<<op2<<"["<<result<<"]"<<endl;
			break;
		case METHODLBL:
			cout<<instNo<<":METHODLBl "<<op1<<endl;
			break;
		case LOCAL_FUNCCALL:
			if(result == "")
				temp = "";
			else
				temp = " = ";
			cout<<instNo<<":"<<result<<temp<<"LCall _"<<op1<<endl;
			break;
		case LOCAL_FUNCCALL_ASSIGN:
			cout<<instNo<<":"<<result<<" = LCall _"<<op1<<endl;
			break;
		case RETURN:
			cout<<instNo<<":RETURN "<<op1<<endl;
			break;
		case PUSHPARAM:
			cout<<instNo<<":PUSHPARAM "<<op1<<endl;
			break;
		case PRINT:
			cout<<instNo<<":PRINT "<<op1<<endl;
		default:
			break;
	}
}

void QuadsList::updateMethodLine(int line, int val)
{
	qdl->at(line).updateA2(NoToStr(val));
}
