
HW3 - CS335
===========
+-----------------------------------------------------------------------------+
| 1. Installation Instructions:                                               |
+-----------------------------------------------------------------------------+

If dotty isn't showing lables, try installing xfonts-100dpi:
sudo apt-get install xfonts-100dpi

Installations:
sudo apt-get install flex-old bison++

To get back the original configuration:
sudo apt-get purge bison++ flex-old
sudo apt-get install flex bison

+-----------------------------------------------------------------------------+
| 2. Running the compiler:                                                    |
+-----------------------------------------------------------------------------+

$ cd asgn3
$ make
$ bin/irgen test/test1.java

To clean all binary/compiled files
$ cd asgn2
$ make clean

+-----------------------------------------------------------------------------+
| 2. Features:                                                                |
+-----------------------------------------------------------------------------+

1. Expressions: For, while, do-while, if, switch 
2. Function calls
3. Class declarations
4. Type Casting, coercion
5. Type Checking
6. Nested Symbol Tables
7. Scoping in blocks
8. stl::map (Balanced Binary tree) as symbol table
9. Backpatching
10. 1D arrays
11. Object Initialisation (incomplete)
12. Access through qualified names(incomplete)
13. Data types: Primitive - int, char, float, boolean
				Reference - primitive arrays, Objects
* Parser modified to support associativity instead of nested rules so grammar 
slightly different from previous assignment.
+-----------------------------------------------------------------------------+
| 3. Unsupported Features:                                                    |
+-----------------------------------------------------------------------------+

1. Increment/Decrement Operators
2. Ternary operators
3. Multidimensional arrays
4. Datatypes: double, long, byte, short

+-----------------------------------------------------------------------------+
| 3. Error Handling:                                                          |
+-----------------------------------------------------------------------------+

Typechecking being done. 

+-----------------------------------------------------------------------------+
| 4. Implementation:                                                          |
+-----------------------------------------------------------------------------+

Three Address Codes are implemented in class Quad3AC and QuadList (which is an
abstraction over vector of Quad3AC).

SymbolTable is implemented in class ST as an abstration over 
stl::map<string, SymbTableEntry>

SymbolTable stores name, place, type and also other info like parameter types. 
Nesting is also is supported. 

