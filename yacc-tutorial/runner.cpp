#include <vector>
#include <iostream>
#include <string>
#include <cstring>
#include <cstdio>
#include <iomanip>

#include "calc.tab.h"

using namespace std;

extern int yyparse();

int main(int argc, char *argv[])
{
	if(argc==2)
	{
		if(freopen(argv[1], "r", stdin)==NULL)
		{
			cout<<"File not found"<<endl;
			return 0;
		}
	}
	yyparse();
	return 0;
}