#include "parser.h"
#define YY_DECL int yyFlexLexer::yylex(YY_Parser_STYPE *val)
#include "FlexLexer.h"
#include <stdio.h>
#include "CodeGen.hpp"

class ParseTree : public Parser
{
private:
  yyFlexLexer theLexer;
 public:
  virtual int yylex();
  virtual void yyerror(char *m);
  ParseTree(){initializeError();}
};

int ParseTree::yylex()
{
 return theLexer.yylex(&yylval);
}

void ParseTree::yyerror(char *m)
{ fprintf(stderr,"%d: %s at token '%s'\n",theLexer.lineno(), m, theLexer.YYText());
}

int main(int argc,char **argv)
{
 if(argc != 2)
 {
	 cerr<<"Invalid invocation. Usage bin/parser <filename>"<<endl;
	 return -1;
 }
 else
 {
	 if(freopen(argv[1], "r", stdin)==NULL)
	 {
	 	cerr<<"File not found"<<endl;
	 	return -1;
	 }

	 ParseTree aCompiler;
	 init();
	 int result=aCompiler.yyparse();
   	 cout<<endl;
   	 //printSymbTable();
   	 string filename(argv[1]);
	 int posOfDot = filename.find('.');
	 string outName = filename.substr(0, posOfDot);
	 outName += ".dot";
	 printToDot(outName); 
	 //cout<<"Success, dotfile "<<outName<<" created"<<endl;
	 print3AC();
	 if(result == 0 && getErrorCount()==0 && getTypeError()==0 && otherErrorCount()==0)
	 {
		 
		// printSymbTable();
	 	if(!getMainOcc())
	 	{
	 		cerr<<"No main found, code may not perform as expected";
	 	}
	 	// print3AC();
	 	GenMIPS MIPSCode(getQuadsList(),getRootST());
	 	MIPSCode.emitCode();
	 	MIPSCode.printGeneratedCode();
		cerr<<"Compiled Successfully"<<endl;
	 }
	 else
	 {
		cerr<<"Failed to parse the file [ERROR]: (Lexing) "<<getErrorCount()<<" error(s), (Parsing) "<< aCompiler.yynerrs<<" error(s)"<< endl;
		cerr<<"Type Errors: "<<getTypeError()<<endl;
		cerr<<"Other Errors:"<<endl;
		printOtherError();
	 }
 }
 return 0;
}

