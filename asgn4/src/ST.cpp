#include"ST.hpp"
ST::ST(ST * par)
{
	width = 0;
	parent = par;
	baseOffset = 0;
}

void
ST::enter(const string &name, const string &type, bool value, int w)
{
	STE t(name, type, value, w);
	t.offset = width;  			
	binSymTab[name] = t;	
	width += w;	
}

void 
ST::enter(const string& name,const string& type, ST * ta)
{
	//TODO SIZE
	STE t;
	t.offset = 0;
	t.tab = ta;
	t.type = type;
	binSymTab[name] = t;
}

bool
ST::lookup(const string &name)
{
	//Not looking in parent, need data of whether to look in parent
	if(binSymTab.find(name) != binSymTab.end())
	{
		return true;
	}
	else if(parent != NULL && parent->parent != NULL)
	{
		return parent->lookup(name);
	}
	return false ; 
}

bool
ST::lookupLoc(const string &name)
{
	if(binSymTab.find(name) != binSymTab.end())
	{
		return true;
	}
	return false ; 
}

STE 
ST::getSTE(const string &name)
{
//	assert(lookup(name));
	if(binSymTab.find(name) != binSymTab.end())
	{
		return binSymTab[name];
	}
	else if(parent != NULL && parent->parent != NULL)
	{
		return parent->getSTE(name);
	}
	STE * s = new STE();
	s->type = "type_error";		//Doesn't exist in the symbol table
	return *s;
}

STE * ST::getSTEP(const string &name)
{
	if(binSymTab.find(name) != binSymTab.end())
	{
		return &binSymTab[name];
	}
	else if(parent != NULL && parent->parent != NULL)
	{
		return parent->getSTEP(name);
	}
	STE * s = new STE();
	s->type = "type_error";		//Doesn't exist in the symbol table
	return s;	
}


#include <iostream>
#include <iomanip>
void
ST::print(int sp)
{
	for(map<const string,STE>::iterator it = binSymTab.begin(); it!=binSymTab.end();++it)
	{	
		if(it->second.tab != NULL)
		{
			cout<<setw(sp)<<"------------"<<endl;
			cout<<setw(sp)<<"Subtable:"<<it->first<<":"<<it->second.type<<endl;
			it->second.tab->print(sp+4);
			cout<<setw(sp)<<"-----------"<<endl;
		}
		else
		{
			cout<<setw(sp)<<it->first<<":"<<it->second.type<<endl;
		}
	}
}
void 
ST::addWidth(int w)
{
	width += w;
}

