#include "ST.hpp"
#include "Quad3AC.hpp"
#include <iostream>
class GenMIPS{
private:
	string genCode;
	QuadsList * quads;
	ST * rootTable;
	int curInst;
	string globalHead;
	string className;
public:
	GenMIPS(QuadsList * q, ST * r)
	{
		quads = q;
		rootTable = r;
		genCode="";
		curInst = 0;
		className = rootTable->getFirstName();
		globalHead ="#MIPS code\n.globl main\n.text\n" ;//"\n.data\n_true_msg: .asciiz \"true\"\n _false_msg: .asciiz \"false\"\n";
	}
	void printGeneratedCode(){
		string staticData = ".data:\n";
		ST * classTable = rootTable->getFirstSTE().tab;
		int width = classTable->width;
		staticData+=classTable->name+": .word "+quads->NoToStr(width)+"\t#static allocate\n";
		
		string maintext="\nmain:\n";
		maintext+="jal _main_func\t#call main\n";
		maintext+="li $v0, 10\t#halt\n";
		maintext+="syscall\n";
		
		cout<<globalHead<<endl<<maintext<<endl<<genCode<<endl<<staticData;
	}
	string getReg(const string& x, int selectors);
	void storeReg(const string& x, string& reg);
	void add(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void sub(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void mult(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void div(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void assignConstant(const string& oprnd1,const string& con);
	void le(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void gr(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void leEq(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void grEq(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void eqEq(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void nEq(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void perc(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void leLe(const string& oprnd1,const string& oprnd2,const string& oprnd3);	
	void grGr(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void grGrGr(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void OpAnd(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void OpOr(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void OpXor(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void assignExpr(const string& oprnd1,const string& oprnd2);
	void OpNot(const string& oprnd1,const string& oprnd2);
	void neg(const string& oprnd1,const string& oprnd2);
	void ifEqEq(const string& oprnd1,const string& oprnd2,const string& label);
	void ifLe(const string& oprnd1,const string& oprnd2,const string& label);
	void ifLeEq(const string& oprnd1,const string& oprnd2,const string& label);
	void ifGr(const string& oprnd1,const string& oprnd2,const string& label);
	void ifGrEq(const string& oprnd1,const string& oprnd2,const string& label);
	void ifNEq(const string& oprnd1,const string& oprnd2,const string& label);
	void ifEqEqTrue(const string& oprnd1,const string& label);
	void OpGoto(const string& label);
	void label(const string& label);
	void emitCode();
	void beginFunction(const string& funcName);
	void returnFunction(const string& value);
	void endFunction();
	void pushParam(string& param);
	void assignFuncCall(string& to,string& func);
	void funcCall(string& func);
	int getTypeSize(const string & type);
	void floatToInt(const string & to, const string & from);
	void intToFloat(const string & to, const string & from);
	void subf(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void addf(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void multf(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void divf(const string& oprnd1,const string& oprnd2,const string& oprnd3);
	void printStr(const string& name);
	void printBool(const string& name);
	void printInt(const string& name);
	void printFloat(const string& name);
	void printChar(const string& name);
	void assignStr(const string& oprnd1,const string& con);
	void printLn();
	void arrayAssignLHS(const string& name,const string& size,const string& RHS);
	void arrayAssignRHS(const string& LHS,const string& name,const string& size);
	int getAddr(const string& x);
	int getSize(const string& x);
	bool isFloat(const string& x);
};
 