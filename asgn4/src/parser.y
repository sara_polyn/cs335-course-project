%{
#define YY_Parser_STYPE yy_Parser_stype
%}
%name Parser
%define LSP_NEEDED
%define LEX_BODY =0
%define ERROR_BODY =0
%define DEBUG 1

%header{
#include <bits/stdc++.h>
#include <stdlib.h>
#include <assert.h>
#include "node.hpp"
#include "ST.hpp"
#include "Quad3AC.hpp"
using namespace std;



QuadsList * getQuadsList();
ST * getRootST();

Node * createSCNode(Node * descend, string type, string value = "");
void assignRoot(Node * n);
void printToDot(string filename);
void initializeError();
void incError();
int getErrorCount();
bool checkSwitchType(const string & s);
//Highly inefficient, v TODO
vector<int> merge(const vector<int>& v1,const vector<int>& v2);
void backpatch(const vector<int> &v1, int to);
void backpatchNext(const vector<int> &v1);
void conditionalAction(Node * res, Node * operand1, const string& comp, Node * operand2, Node * comparator);
void arithmeticAction(Node *res, Node * op1, Node * op2, Node * op3);
int getTypeSize(const string& type);


void initReturnList();
void pushReturnList(int lNo);
vector<int> getReturnList();
/***
Wrappers
**/
bool getMainOcc();
void setMainOcc();
void initRootTable(); 
void init();
void initStack();
bool lookupCurTable(const string &name);
//TODO replace above with the one below
void enterCurTable(const string &name, const string &type, bool value, int offset, int arrLength); 
void enterCurTable(const string &name, const string &type, bool value, int offset);
STE getCurTableSTE(const string &name);
bool lookupLocCurTable(const string &name);
bool lookupClassInRoot(const string &name);
bool lookupRoot(const string &objname, const string &memName);
STE getRootSTE(const string& className, const string& varName);

ST * getCurrTable();
void setCurrTable(ST * tab);

void initQuads();
void emitGoto(const string &label);
void emitGoto(int lno);
int getNextQuad();
void emitLabel(const string &label);
void emit(const string& emitted);
void emit(const string& emitted,int quad);
void emit(const string& place, const string& operand1, const string& opt, const string& operand2); /* +-/ */ 
void emitBoolOp(const string& place, const string& operand1, const string& opt, const string& operand2); /* And Or */
string newTemp(); // Generates a new var (say corresponding to instr no.)

void emitIf(const string& operand1, const string& comp,const string& operand2,int quad); 
void emitIf(const string& expr,int quad); 
void emitAssign(const string& tmp, const string& val);
void emitAssignConst(const string& tmp, const string& val);
void emitAssignString(const string& tmp, const string& val);
int getBigType(const string& type1,const string& type2);
string numToType(int type);  
void emitUnaryMinus(const string& operand1,const string& operand2); 
void emitUnaryNot(const string& operand1,const string& operand2); 
void emitUnaryTilde(const string& operand1,const string& operand2);  
void emitConv(const string& tmp, const string& operand,const string& fromType,const string& toType);
void emitArray(const string& name, const string& type, int indices);
void emitArrayLHS(const string& name,const string& size,const string& RHS);
void emitArrayRHS(const string& LHS,const string& name,const string& size);
string numToString(int num);
void emitPrint(string s);
//TODO these functions
void emitPrint(Node * n);
void emitPrintln();

void setSizeLine(int quad);
int getSizeLine();
void emitMLabel(const string& label);
void setCurrTableName(const string& name);
void updateLineSize(int destLine, int mSize);
void emitLCall(const string& func);
void emitLCallAssign(const string& tmp, const string& func);
void emitReturn(const string& func);
bool compareParams(STE t, Node* arg);
void emitPushParam(Node* param);
void initTypeError();
void incTError();
int getTypeError();


void initOtherError();
void OtherError(const string& error);
void printOtherError();
int otherErrorCount();
//debugging
void print3AC();
void printSymbTable();

//stacks
void pushTabToStack(ST * t);
ST * popTabFromStack();
ST * topTabFromStack();
string getNextBlkNo();
void setScope(const string& name, const string& type);
void clrScope();
		
#define YY_DECL int yyFlexLexer::yylex(YY_Parser_STYPE *val)

#ifndef FLEXFIX
#define FLEXFIX YY_Parser_STYPE *val
#define FLEXFIX2 val
#endif

%}


%union{
	Node *node_type;
	char *strval;
}

%type <node_type> Goal
%type <node_type> CompilationUnit

%type <node_type> Literal IntegralType Type PrimitiveType ReferenceType NumericType FloatingPointType Name ClassType SimpleName QualifiedName ClassDeclaration ClassBody ClassBodyDeclaration MethodDeclarator ClassBodyDeclarations VariableInitializer FormalParameter VariableDeclaratorId ClassMemberDeclaration Expression MethodDeclaration FormalParameterList Block MethodHeader VariableDeclarators  TypeDeclarations MethodBody FieldDeclaration VariableDeclarator BlockStatements ExpressionStatement Statement Assignment  BlockStatement SwitchBlockStatementGroups  ReturnStatement ConstantExpression SwitchLabel BreakStatement IfThenStatement SwitchBlock EmptyStatement StatementNoShortIf MethodInvocation IfThenElseStatement WhileStatementNoShortIf ClassInstanceCreationExpression ContinueStatement SwitchStatement SwitchBlockStatementGroup SwitchLabels LocalVariableDeclarationStatement LocalVariableDeclaration WhileStatement  StatementExpression IfThenElseStatementNoShortIf   StatementWithoutTrailingSubstatement  ArgumentList DimExpr  AssignmentOperator PostfixExpression   ArrayCreationExpression  UnaryExpressionNotPlusMinus ArrayAccess  Primary   ConditionalExpression UnaryExpression  CastExpression DimExprs LeftHandSide FieldAccess ForStatement ForStatementNoShortIf ForInit ForUpdate StatementExpressionList DoStatement ArithmeticExpression VoidMethodBody 

%type <node_type> MultTypeMarker PatchMarker GotoMarker MethodMarker

%token <node_type> IntegerLiteral FloatingPointLiteral BooleanLiteral CharacterLiteral StringLiteral NullLiteral Identifier KeywordBoolean KeywordInt KeywordChar KeywordFloat KeywordDouble KeywordClass KeywordVoid KeywordIf KeywordElse KeywordThis KeywordSwitch KeywordCase KeywordDefault KeywordWhile KeywordBreak KeywordContinue KeywordReturn SeparatorLSqBrac SeparatorRSqBrac SeparatorDot SeparatorSemiCo SeparatorComma OperatorEq OperatorColon SeparatorLCuBrac SeparatorRCuBrac SeparatorLParen SeparatorRParen OperatorMiEq OperatorUp OperatorLeLeEq KeywordNew OperatorVert OperatorDiv OperatorGrGrGr OperatorLe OperatorPl OperatorGrGrEq OperatorNE OperatorAmp OperatorExcl OperatorMi OperatorAnd OperatorUpEq OperatorAmpEq OperatorVertEq OperatorPlEq OperatorLeEq OperatorMul OperatorGrEq OperatorOr OperatorAsEq OperatorPercEq OperatorGr OperatorSlEq OperatorGrGrGrEq OperatorTild OperatorPerc OperatorGrGr OperatorEqEq OperatorLeLe KeywordFor KeywordDo SyscallPrint SyscallPrintln

%left OperatorAsEq OperatorMiEq OperatorGrGrEq OperatorUpEq OperatorAmpEq OperatorPlEq OperatorPercEq OperatorSlEq OperatorVertEq OperatorGrGrGrEq OperatorLeLeEq
%left OperatorOr 
%left OperatorAnd
%left OperatorVert
%left OperatorUp
%left OperatorAmp
%left OperatorEqEq OperatorNE
%left OperatorLe OperatorLeEq OperatorGrEq OperatorGr
%left OperatorLeLe OperatorGrGr OperatorGrGrGr
%left OperatorPl OperatorMi
%left OperatorMul OperatorDiv OperatorPerc
%left OperatorExcl OperatorTild


%start Goal


%%

Goal		: CompilationUnit 		{ 
										$$ = createSCNode($1, string("Goal"));
						  				$$->nodeNo = 0; 
						  				assignRoot($$);
						  				$$->setType($1->getType());
						  				if($1->getType() != "void"){
						  					//Do something??
						  				}
										//call DFS on $$
					        			//yyterminate();
									}
			;
Literal 	: IntegerLiteral		{ 
									  $$ = createSCNode($1, string("Literal")); 
									  $$->setType("int");
									  $$->setVal($1->getNodeVal());
									  string tmp=newTemp();
									  emitAssignConst(tmp,$1->getNodeVal());
									  $$->setPlace(tmp);
									  enterCurTable($$->getPlace(),$$->getType(),true,4);	
									  
									}
			| FloatingPointLiteral		{ 
										  $$ = createSCNode($1, string("Literal")); 
										  $$->setType("float");
										  $$->setVal($1->getNodeVal());
										  string tmp=newTemp();
										  emitAssignConst(tmp,$1->getNodeVal());
										  $$->setPlace(tmp);
										  enterCurTable($$->getPlace(),$$->getType(),true,4);	
										} 
			| BooleanLiteral			{ 
										  $$ = createSCNode($1, string("Literal")); 
										  $$->setType("boolean");
										  string tmp=newTemp();
										  string val;
										  if($1->getNodeVal()=="true"){
										  	val = "1";
										  }
										  else{
										  	val = "0";
										  }
										  $$->setVal(val);
								    	  int n=getNextQuad();

								    	  vector<int> v1;
										  v1.push_back(n+5);
										  $$->setTrueList(v1);
										  vector<int> v2;
										  v2.push_back(n+2);
										  $$->setFalseList(v2);
									      emitIf(val,n+4);
									      emitAssignConst(tmp,"0");
									      emit("goto");
									      emitGoto(n+6);
									      emitAssignConst(tmp,"1");
									      emit("goto");
								
								          $$->setPlace(tmp);
								          enterCurTable($$->getPlace(),$$->getType(),true,4);
										} 
			| CharacterLiteral			{ 
										  $$ = createSCNode($1, string("Literal")); 
										  $$->setType("char");
										  $$->setVal($1->getNodeVal());;
										  string tmp=newTemp();
										  emitAssignConst(tmp,$1->getNodeVal());
										  $$->setPlace(tmp);
										  enterCurTable($$->getPlace(),$$->getType(),true,4);
										}
			| StringLiteral				{ 
										  $$ = createSCNode($1, string("Literal")); 
										  $$->setType("String");
										  $$->setVal($1->getNodeVal());
										  string tmp=newTemp();
										  emitAssignString(tmp,$1->getNodeVal());
										  $$->setPlace(tmp);
										  enterCurTable($$->getPlace(),$$->getType(),true,4);	//TODO variable offset
										}
			| NullLiteral				{ 
										  $$ = createSCNode($1, string("Literal")); 
										  $$->setType("NULL");
										  $$->setVal($1->getNodeVal());
										  string tmp=newTemp();
										  emitAssignConst(tmp,$1->getNodeVal());
										  $$->setPlace(tmp);
										  enterCurTable($$->getPlace(),$$->getType(),true,4); //todo	//TODO offset !=4 necessarily
										} 
			;
Type		: PrimitiveType			{ 
									  $$ = createSCNode($1, string("Type")); 
									  $$->setType($1->getType());
									  $$->setPlace($1->getPlace());
									}
			| ReferenceType                 { 
											  $$ = createSCNode($1, string("Type")); 
											  $$->setType($1->getType());
											  $$->setPlace($1->getPlace());
											}
			;
PrimitiveType	: NumericType			{ 
										  $$ = createSCNode($1, string("PrimitiveType")); 
										  $$->setType($1->getType());
										  $$->setPlace($1->getPlace());
										}
				| KeywordBoolean		{ 
										  $$ = createSCNode($1, string("PrimitiveType")); 
										  $$->setType($1->getNodeVal());
										  $$->setPlace($1->getNodeVal());
										}
				;
NumericType	: IntegralType			{ 
									  $$ = createSCNode($1, string("NumericType")); 
									  $$->setType($1->getType());
									  $$->setPlace($1->getPlace());
									}		
			| FloatingPointType		{ 
								  $$ = createSCNode($1, string("NumericType")); 
								  $$->setType($1->getType());
								  $$->setPlace($1->getPlace());
								}
			;

IntegralType	:
				  KeywordInt				{ 
											$$ = createSCNode($1, string("IntegralType")); 
											$$->setType($1->getNodeVal()); 
											$$->setPlace($1->getNodeVal());
										}
				| KeywordChar	  		{ 
											$$ = createSCNode($1, string("IntegralType")); 
											$$->setType($1->getNodeVal()); 
											$$->setPlace($1->getNodeVal());
										}  	
				;

FloatingPointType	: KeywordFloat		{ 
									$$ = createSCNode($1, string("FloatingPointType")); 
									$$->setType($1->getNodeVal()); 
									$$->setPlace($1->getNodeVal());
								}     
					;

ReferenceType	: ClassType		{ $$ = createSCNode($1, string("ReferenceType")); 
								  $$->setType($1->getType());
								  $$->setPlace($1->getPlace());
								}
				;

ClassType	: Identifier		{ $$ = createSCNode($1, string("ClassType")); 
                                  bool doesClassExist = lookupClassInRoot($1->getNodeVal());

						  		  if(doesClassExist)
                                  {
                                    $$->setType($1->getNodeVal());
						  		    $$->setPlace($1->getPlace());
                                  }
                                  else
                                  {
                                    OtherError("Undefined "+$1->getNodeVal());
                                  }    
								}
		;
Name 	: SimpleName		   { 
								  $$ = createSCNode($1, string("Name")); 
								  $$->setType(getCurTableSTE($1->getNodeVal()).type);
								  $$->setPlace($1->getPlace());
								}
		| QualifiedName			{ 
								  $$ = createSCNode($1, string("Name")); 
								  $$->setType($1->getType());
								  $$->setPlace($1->getPlace());
								  
								}
		;
SimpleName 	: Identifier			{ 
										$$ = createSCNode($1, string("SimpleName")); 
						  				bool doesIdEx = lookupCurTable($1->getNodeVal());
						  				if(doesIdEx)
						  				{
						  					$$->setType(getCurTableSTE($1->getNodeVal()).type);	
						  					$$->setPlace($1->getNodeVal());
						  				}
						  				else
						  				{
						  					OtherError("Identifier " + $1->getNodeVal()+" undeclared");
						  				}
									}
		;
QualifiedName	: Identifier SeparatorDot Identifier	{  
                             $$ = createSCNode($1, string("QualifiedName"));
							 $$->addChild($2);
							 $$->addChild($3);
							 //TODO searc from root
							 //cerr<<$1->getType()<<endl;
							 bool classExists = lookupClassInRoot(getCurTableSTE($1->getNodeVal()).type);
							 bool isIdInClass = lookupRoot(getCurTableSTE($1->getNodeVal()).type, $3->getNodeVal());
							 if(isIdInClass)
							 {
                                STE varSTE = getRootSTE(getCurTableSTE($1->getNodeVal()).type, $3->getNodeVal());
                                $$->setType(varSTE.type);
                                //TODO further
                                //$$->setPlace()
							 }
							 else if (classExists)
							 {
							 	OtherError("Variable "+$2->getNodeVal()+ " not a member of class "+ $1->getNodeVal());
							 }
							 else
							 {
							 	OtherError("Error type of " + $1->getNodeVal() + " not found");
							 }
						}
		;
CompilationUnit : TypeDeclarations 		{
											 $$ = createSCNode($1, string("CompilationUnit")); 
										}	
		;

TypeDeclarations: ClassDeclaration		{ 
											$$ = createSCNode($1, string("TypeDeclarations")); 
											$$->setType($1->getType());
										}
		/*| TypeDeclarations ClassDeclaration	{  $$ = createSCNode($1, string("TypeDeclarations"));
							   				   $$->addChild($2);
							   				   if($1->getType()==$2->getType() && $1->getType()=="void"){
							   				   	$$->setType($1->getType());
							   				   }
							   				   else{
							   				   	$$->setType("type_error");
							   				   }
											}
		*/
		; 

ClassDeclaration: KeywordClass Identifier ClassBody 	
				{	
								$$ = createSCNode($1, "ClassDeclaration");
								$$->addChild($2);
								$$->addChild($<node_type>3);
				}
		; 
ClassBody	: SeparatorLCuBrac {
				 setScope($<node_type>0->getNodeVal(), $<node_type>0->getNodeVal());
				 setCurrTableName($<node_type>0->getNodeVal()); 
				 }ClassBodyDeclarations SeparatorRCuBrac	
							{
										$$ = createSCNode($1, "ClassBody");
										$$->addChild($3);
										$$->addChild($4);
										$$->setType($3->getType()); 
										//cerr<<getCurrTable()->width<<endl;
										//TODO class declaration over, emit size?
										clrScope();
							}
		| SeparatorLCuBrac SeparatorRCuBrac	{	
												$$ = createSCNode($1, "ClassBody");
												$$->addChild($2);
												$$->setType("void"); 
												//TODO emit size=0
											}
		;
ClassBodyDeclarations	: ClassBodyDeclaration		{ 
														$$ = createSCNode($1, "ClassBodyDeclarations");
														$$->setType($1->getType()); 
													}
			| ClassBodyDeclarations ClassBodyDeclaration  	{
																 $$ = createSCNode($1, "ClassBodyDeclarations");
									  							 $$->addChild($2);
									  							 if($1->getType()==$2->getType() && $1->getType()=="void"){
									  							 	$$->setType($1->getType()); 
									  							 }
									  							 else{
									  							 	$$->setType("type_error"); 
									  							 }
								      						}
			;
ClassBodyDeclaration	: ClassMemberDeclaration 	{ 
														$$ = createSCNode($1, "ClassBodyDeclaration"); 
														$$->setType($1->getType()); 
													}
			; 
ClassMemberDeclaration  : FieldDeclaration	{ 
												$$ = createSCNode($1, "ClassMemberDeclaration");
												$$->setType($1->getType()); 
											}
			| MethodDeclaration	{ 
									$$ = createSCNode($1, "ClassMemberDeclaration"); 
									$$->setType($1->getType()); 
								}
			;
FieldDeclaration	: Type VariableDeclarators SeparatorSemiCo	{  
										$$ = createSCNode($1, "FieldDeclaration");
									   	$$->addChild($2);
									   	$$->addChild($3);
									   	if($1->getType()==$2->getType()){
									   		$$->setType("void");
									   	}
									   	else{
									   		$$->setType("type_error");
									   	}
									}
			;
VariableDeclarators	: VariableDeclarator	{ 
												$$ = createSCNode($1, "VariableDeclarators");
												$$->setType($1->getType());	
											}
			| VariableDeclarators SeparatorComma MultTypeMarker VariableDeclarator	
										{
											//MultTypeMarker for type
											$$ = createSCNode($1, "VariableDeclarators");		
											$$->addChild($2);
											$$->addChild($4);
											$$->setType($1->getType());
										}
			;
VariableDeclarator	: VariableDeclaratorId	
						{ 
						  $$ = createSCNode($1, "VariableDeclarator"); 
						  bool exists = lookupLocCurTable($1->getPlace()); 
						  bool array = $1->isArray();
						  if(!exists && !array)
						  {
						  	  $$->setType($1->getType());
							  enterCurTable($1->getPlace(),$1->getType(),false,getTypeSize($1->getType()));	//TODO offset function
							  // Here we add false as no value is assigned yet to it.
						  }
						  else
						  {
							  OtherError("Redeclaration for variable " + $1->getPlace());
						  }
						}
			| VariableDeclaratorId OperatorEq VariableInitializer	
										{ 
										  	$$ = createSCNode($1, "VariableDeclarator");
										  	$$->addChild($2);
										  	$$->addChild($3);
											bool exists = lookupCurTable($1->getPlace()); 
											bool typeMatch = $1->getType() == $3->getType();
											//cerr<<"type: "<<$1->getDim()<<" "<<$3->getDim()<<endl;
											if(typeMatch && !exists && $1->getDim() == $3->getDim())
											{
													if($1->isArray()){
														$$->setType($1->getType());
														int arrayLength = atoi($3->getArrayDimVals()->at(0).c_str());
														enterCurTable($1->getPlace(),$1->getType(),true,getTypeSize($1->getType()),arrayLength);	//TODO offset function
														//cerr<<"id "<<$1->getPlace()<<" "<<$1->getType()<<" "<<getTypeSize($1->getType())<<" "<<arrayLength<<endl;
														//TODO emit as well
														emitArray($1->getPlace(),$1->getType(),arrayLength);
													}
													else{
														$$->setType($1->getType());
														enterCurTable($1->getPlace(),$1->getType(),true,getTypeSize($1->getType()));	//TODO offset function
														emitAssign($1->getPlace(),$3->getPlace());
													}
											}
											else if(typeMatch && !$1->isArray())
											{
											  OtherError("Redeclaration for variable " + $1->getPlace());
											}
											else
											{
												$$->setType("type_error");
											}
											//TODO value assigned from return val of initializer, needs to be computed
										}	
			;
VariableDeclaratorId	: Identifier	{ 
											$$ = createSCNode($1, "VariableDeclaratorId"); 
										    $1->setType($<node_type>0->getType());
										    $$->setType($1->getType());
										    $$->setPlace($1->getNodeVal());
										    $$->setVal($1->getNodeVal());
										}
			| Identifier SeparatorLSqBrac SeparatorRSqBrac	{
								  				$$ = createSCNode($1, "VariableDeclaratorId");
									  			$$->addChild($2);
									  			$$->addChild($3);
									  			$1->setType($<node_type>0->getType());
												$$->setType($1->getType()); 
												$$->setPlace($1->getNodeVal());
												$$->setArray();
												$$->incDim(1);
											}
			;
VariableInitializer	: Expression	
								{ 
									$$ = createSCNode($1, "VariableInitializer"); 
									$$->setType($1->getType()); 
                                    //cerr<<$1->getType()<<endl;
									$$->setPlace($1->getPlace());
								}
			| ArrayCreationExpression	
							    { 
							      $$ = createSCNode($1, "VariableInitializer"); 
							      $$->setType($1->getType());
							      $$->setArray();
							      $$->incDim($1->getDim());
							      $$->setDimVal($1->getArrayDimVals())
							    }
			;
			/** methods **/
MethodDeclaration	: MethodHeader MethodBody	{ $$ = createSCNode($1, "MethodDeclaration"); 
							  $$->addChild($2);

							  ST * temp = popTabFromStack();
							  int paramsSize = 0;
  							  for(int i=0;i<$1->paramslist->size();++i)
							  {
							  		paramsSize+= getTypeSize($1->paramslist->at(i));
							  }
							  temp->baseOffset = -paramsSize;
							  ST * tempCl = topTabFromStack();
							  STE * methd = tempCl->getSTEP(temp->name);
							  methd->paramslist = $1->paramslist;
							  //TODO check if this size needs to be added for assignment 4
							  //tempCl->addWidth(temp->getWidth());
							  //entry Already added to this table
							  backpatch(getReturnList(), getNextQuad());
							  emitMLabel("EndFunc");
							  updateLineSize(getSizeLine(), temp->getWidth() - paramsSize);
							  setCurrTable(tempCl);
							  if($2->getType() == "type_error")
							  {
							  	incTError();
							  }
							}
					| KeywordVoid MethodDeclarator VoidMethodBody{ 
							  	 $$ = createSCNode($1, "MethodDeclaration"); 
							 	 $$->addChild($2);
							  	 $$->addChild($3);
								  ST * temp = popTabFromStack();
								  int paramsSize = 0;
	  							  
	  							  for(int i=0;i<$2->paramslist->size();++i)
								  {
								  		paramsSize+= getTypeSize($2->paramslist->at(i));
								  }
								  temp->baseOffset = -paramsSize;
								  //cerr<<"Reached after here";
								  ST * tempCl = topTabFromStack();
								  STE * methd = tempCl->getSTEP(temp->name);
								  methd->paramslist = $2->paramslist;
								  //TODO check if this size needs to be added for assignment 4
								  //tempCl->addWidth(temp->getWidth());
								  //entry Already added to this table
								  backpatch(getReturnList(), getNextQuad());
								  emitMLabel("EndFunc");
								  updateLineSize(getSizeLine(), temp->getWidth() - paramsSize);
								  setCurrTable(tempCl);
								  $$->setType("void");
								  if($2->getType() == "type_error")
								  {
								  	incTError();
								  }
						} 
			;
MethodHeader	: Type MethodDeclarator			{ $$ = createSCNode($1, "MethodHeader"); 
							  $$->addChild($2);
							  $$->paramslist = $2->paramslist;
							}

		;

MethodDeclarator: Identifier MethodMarker SeparatorLParen FormalParameterList SeparatorRParen { $$ = createSCNode($1, "MethodDeclarator"); 
											$$->addChild($3);
											$$->addChild($4);
											$$->addChild($5);
								  			$$->paramslist = $4->paramslist;
								  			ST * temp = popTabFromStack();
											  ST * tempCl = topTabFromStack();
											  STE * methd = tempCl->getSTEP(temp->name);
											  methd->paramslist = $4->paramslist;
											  pushTabToStack(temp);
										}
		| Identifier MethodMarker SeparatorLParen SeparatorRParen	{ $$ = createSCNode($1, "MethodDeclarator"); 
								  $$->addChild($2);
								  $$->addChild($3);
								  $$->addChild($4);
								  $$->paramslist = new vector<string>(0);
								  ST * temp = popTabFromStack();
								  ST * tempCl = topTabFromStack();
								  STE * methd = tempCl->getSTEP(temp->name);
								  methd->paramslist = $$->paramslist;
								  pushTabToStack(temp);

								}
		;
FormalParameterList	: FormalParameter 	{ $$ = createSCNode($1, "FormalParameterList");
					  	  enterCurTable($1->getPlace(),$1->getType(),false,getTypeSize($1->getType()));	//TODO get from PushedParams
						  $$->paramslist = new vector<string>(0);
						  $$->paramslist->push_back($1->getType());
						 }
			| FormalParameter SeparatorComma FormalParameterList { $$ = createSCNode($1, "FormalParameterList"); 
											$$->addChild($2);
											$$->addChild($3);
											$$->paramslist = $3->paramslist;
											$$->paramslist->push_back($1->getType());
					  	  					enterCurTable($1->getPlace(),$1->getType(),false,getTypeSize($1->getType()));	//TODO get from PushedParams
									    	}
			;
FormalParameter	: Type VariableDeclaratorId	{ $$ = createSCNode($1, "FormalParameter"); 
						  $$->addChild($2);
						  $$->setPlace($2->getPlace());
						  $$->setType($1->getType());
						}
		;
MethodBody	: SeparatorLCuBrac BlockStatements ReturnStatement SeparatorRCuBrac	{ $$ = createSCNode($1, "MethodBody"); 
																					$$->addChild($2);
																					$$->addChild($3);
																					$$->addChild($4);
																					if($3->getType() == getCurrTable()->type){
																						$$->setType("void");
																					}
																					else
																					{
																						$$->setType("type_error");
																					}
																					//No need to backpatch return here
																				}
			| SeparatorLCuBrac ReturnStatement SeparatorRCuBrac	{ $$ = createSCNode($1, "MethodBody"); 
																					$$->addChild($2);
																					$$->addChild($3);
																					if($2->getType() == getCurrTable()->type){
																						$$->setType("void");
																					}
																					else
																					{
																						$$->setType("type_error");
																					}
																					//No need to backpatch return here
																				}
			| SeparatorLCuBrac error SeparatorRCuBrac 			{
																	yyclearin;yyerrok;
																	OtherError("Missing return statement");
																}
		;
VoidMethodBody : Block 											{
																	$$ = createSCNode($1, "VoidMethodBody"); 
																					if($1->getType() == getCurrTable()->type){
																						$$->setType("void");
																					}
																					else
																					{
																						$$->setType("type_error");
																					}
																}
//Removed constructor


// Removed VariableInitializers as not supporting static array declaration
Block 	: SeparatorLCuBrac { setScope(getNextBlkNo(), getCurrTable()->type);}
	  BlockStatements SeparatorRCuBrac	
								{  
								   $$ = createSCNode($1, "Block"); 
								   $$->addChild($3);
								   $$->addChild($4);
								   $$->setType($3->getType());
								   $$->setBreakList($3->getBreakList());
								   clrScope();
								}
	| SeparatorLCuBrac SeparatorRCuBrac	{ 
						  $$ = createSCNode($1, "Block"); 
						  $$->addChild($2);
						  $$->setType("void");
						}
	;
BlockStatements	: BlockStatement 	{ $$ = createSCNode($1, "BlockStatements"); 
									  $$->setType($1->getType()); 
									  $$->setBreakList($1->getBreakList());
									}
		| BlockStatements BlockStatement 	{ $$ = createSCNode($1, "BlockStatements"); 
											  $$->addChild($2);
											  if($1->getType()==$2->getType() && $1->getType()=="void"){
											  	$$->setType($1->getType()); 
											  	$$->setBreakList(merge($1->getBreakList(),$2->getBreakList()));
											  }
											  else{
											  	$$->setType("type_error"); 
											  }
											  //TODO emit type error if found
											}
		;
BlockStatement 	: LocalVariableDeclarationStatement	{ $$ = createSCNode($1, "BlockStatement"); 
											  $$->setType($1->getType());
											  $$->setBreakList($1->getBreakList());
											}
		| Statement 				{ $$ = createSCNode($1, "BlockStatement"); 
									  $$->setType($1->getType());
									  $$->setBreakList($1->getBreakList());
									}
		| SyscallPrint SeparatorLParen Expression SeparatorRParen SeparatorSemiCo {
																$$ = createSCNode($1, "BlockStatement");
																if($3->getType()!="String")
																{
																	$$->addChild($2);$$->addChild($3); $$->addChild($4); $$->addChild($5);
																}
																emitPrint($3);
																$$->setType("void");
																//TODO check for string
															}
		| SyscallPrintln SeparatorLParen Expression SeparatorRParen SeparatorSemiCo {
																$$ = createSCNode($1, "BlockStatement");
																if($3->getType()!="String")
																{
																	$$->addChild($2);$$->addChild($3); $$->addChild($4); $$->addChild($5);
																}
																emitPrint($3);
																emitPrintln();
																$$->setType("void");
															}
		| SyscallPrintln SeparatorLParen SeparatorRParen SeparatorSemiCo{
																$$ = createSCNode($1, "BlockStatement");
																$$->addChild($2);$$->addChild($3); $$->addChild($4);
																emitPrintln();
																$$->setType("void");
															}
		;


LocalVariableDeclarationStatement	: LocalVariableDeclaration SeparatorSemiCo{ $$ = createSCNode($1, "LocalVariableDeclarationStatement"); 
										    $$->addChild($2);
										    if($1->getType() == "void")	$$->setType("void");
										    else $$->setType("type_error");
										 }
					;

LocalVariableDeclaration 	: Type VariableDeclarators	{ $$ = createSCNode($1, "LocalVariableDeclaration"); 
								  $$->addChild($2);
								  if($1->getType()==$2->getType()){
								   	$$->setType("void");
								  }
								  else{
								 	$$->setType("type_error");
								  }
								}
				;

Statement 	: StatementWithoutTrailingSubstatement 	{ $$ = createSCNode($1, "Statement"); 
													  $$->setType($1->getType());
													  $$->setNextList($1->getNextList());
									  				  $$->setBreakList($1->getBreakList());
									  				  
									  				  if($$->getType() == "type_error")
									  				  {
									  				  	incTError();
									  				  }
									  				  }
		| IfThenStatement 			{ $$ = createSCNode($1, "Statement");
									  int quad = getNextQuad();
									  backpatch($1->getNextList(),quad); 
									  $$->setType($1->getType());
									  $$->setBreakList($1->getBreakList());
									  
									  if($$->getType() == "type_error")
									  				  {
									  				  	incTError();
									  				  }
									}
		| IfThenElseStatement			{ $$ = createSCNode($1, "Statement"); 
										  int quad = getNextQuad();
									  	  backpatch($1->getNextList(),quad); 
									  	  $$->setType($1->getType());
									  	  $$->setBreakList($1->getBreakList());
									  	  
									  	  if($$->getType() == "type_error")
									  				  {
									  				  	incTError();
									  				  }
										}
		| WhileStatement			{ $$ = createSCNode($1, "Statement"); 
									  int quad = getNextQuad();
									  backpatch($1->getNextList(),quad); 
									  $$->setType($1->getType());
									  
									  if($$->getType() == "type_error")
									  				  {
									  				  	incTError();
									  				  }
									}
		| ForStatement				{ $$ = createSCNode($1, "Statement"); 
									  int quad = getNextQuad();
									  backpatch($1->getNextList(),quad); 
									  $$->setType($1->getType());
									  
									  if($$->getType() == "type_error")
									  				  {
									  				  	incTError();
									  				  }
									}
		;

StatementNoShortIf 	: StatementWithoutTrailingSubstatement	{ $$ = createSCNode($1, "StatementNoShortIf"); 
															  $$->setType($1->getType());
															  $$->setNextList($1->getNextList());
									  						  $$->setBreakList($1->getBreakList());
															}
			
			| IfThenElseStatementNoShortIf		{ $$ = createSCNode($1, "StatementNoShortIf");
									  			  int quad = getNextQuad();
									  			  backpatch($1->getNextList(),quad); 
									  			  $$->setType($1->getType());
									  			  $$->setBreakList($1->getBreakList());
												}
			| WhileStatementNoShortIf		{ $$ = createSCNode($1, "StatementNoShortIf");
									  		  int quad = getNextQuad();
									  		  backpatch($1->getNextList(),quad); 
									  		  $$->setType($1->getType());
											}
			| ForStatementNoShortIf			{ $$ = createSCNode($1, "StatementNoShortIf");
									  		  int quad = getNextQuad();
									  		  backpatch($1->getNextList(),quad); 
									  		  $$->setType($1->getType());
											}
			;

StatementWithoutTrailingSubstatement	
					: Block 		{ $$ = createSCNode($1, "StatementWithoutTrailingSubstatement"); 
									  $$->setType($1->getType());
									  $$->setNextList($1->getNextList());
									  $$->setBreakList($1->getBreakList());
									}
				   	| EmptyStatement	{ $$ = createSCNode($1, "StatementWithoutTrailingSubstatement"); 
										  $$->setType($1->getType());
										}
					| ExpressionStatement	{ $$ = createSCNode($1, "StatementWithoutTrailingSubstatement"); 
											  $$->setType($1->getType());
											}
					| SwitchStatement	{ $$ = createSCNode($1, "StatementWithoutTrailingSubstatement"); 
										  $$->setType($1->getType());
										}
					| BreakStatement	{ $$ = createSCNode($1, "StatementWithoutTrailingSubstatement"); 
										  $$->setType($1->getType());
										  $$->setBreakList($1->getBreakList());
										}
					| ContinueStatement	{ $$ = createSCNode($1, "StatementWithoutTrailingSubstatement"); 
										  $$->setType($1->getType());
										  $$->setNextList($1->getNextList());
										}
					| ReturnStatement	{ $$ = createSCNode($1, "StatementWithoutTrailingSubstatement"); 
										  if($1->getType() == getCurrTable()->type){
												$$->setType("void");
											}
											else
											{
												$$->setType("type_error");
											}
										}
					| DoStatement 	{ $$ = createSCNode($1, "StatementWithoutTrailingSubstatement"); 
									  $$->setType($1->getType());
									  int quad = getNextQuad();
									  backpatch($1->getNextList(),quad);
									}
					;
EmptyStatement	: SeparatorSemiCo   { $$ = createSCNode($1, "EmptyStatement"); 
									  $$->setType("void");
									}
		;

ExpressionStatement 	: StatementExpression SeparatorSemiCo { $$ = createSCNode($1, "ExpressionStatement"); 
							  									$$->addChild($2);
							  									$$->setType($1->getType());
							  									$$->setPlace($1->getPlace());
							      							  }
			;
StatementExpression	: Assignment 	{ $$ = createSCNode($1, "StatementExpression"); 
									  $$->setType($1->getType());
									  $$->setPlace($1->getPlace());
									}
			| MethodInvocation	{ $$ = createSCNode($1, "StatementExpression"); 
								  $$->setType($1->getType());
								  $$->setPlace($1->getPlace());
								}
			| ClassInstanceCreationExpression 	{ $$ = createSCNode($1, "StatementExpression"); 
												  $$->setType($1->getType());
												  $$->setPlace($1->getPlace());
												}
			;
IfThenStatement : KeywordIf SeparatorLParen Expression  SeparatorRParen PatchMarker Statement{ 
										$$ = createSCNode($1, "IfThenStatement"); 
										$$->addChild($2);
										$$->addChild($3);
										$$->addChild($4);
										$$->addChild($6);
										if(getCurTableSTE($3->getPlace()).type=="boolean"){
											$$->setType($6->getType());
										}
										else{
											$$->setType("type_error");
										}
										backpatch($3->getTrueList(),$5->getQuad());
										$$->setNextList(merge($6->getNextList(),$3->getFalseList()));
									}
		;
IfThenElseStatement 	: KeywordIf SeparatorLParen Expression SeparatorRParen PatchMarker StatementNoShortIf GotoMarker KeywordElse PatchMarker Statement 	{ 													  	$$ = createSCNode($1, "IfThenElseStatement"); 
														$$->addChild($2);
														$$->addChild($3);
														$$->addChild($4);
														$$->addChild($6);
														$$->addChild($8);
														$$->addChild($10);
														if(getCurTableSTE($3->getPlace()).type=="boolean"){
															if($6->getType()=="type_error"){
																$$->setType("type_error");
															}
															else{
																$$->setType($10->getType());
															}
														}
														else{
															$$->setType("type_error");
														}
														backpatch($3->getTrueList(),$5->getQuad());
														backpatch($3->getFalseList(),$9->getQuad());
														$$->setNextList(merge(merge($6->getNextList(),$10->getNextList()),$7->getNextList()));
													}
			;
IfThenElseStatementNoShortIf	: KeywordIf SeparatorLParen Expression SeparatorRParen PatchMarker StatementNoShortIf GotoMarker KeywordElse PatchMarker StatementNoShortIf 	{ $$ = createSCNode($1, "IfThenElseStatementNoShortIf"); 
																$$->addChild($2);
																$$->addChild($3);
																$$->addChild($4);
																$$->addChild($6);
																$$->addChild($8);
																$$->addChild($10);
																if(getCurTableSTE($3->getPlace()).type=="boolean"){
																	if($6->getType()=="type_error"){
																		$$->setType("type_error");
																	}
																	else{
																		$$->setType($10->getType());
																	}
																}
																else{
																	$$->setType("type_error");
																}
																backpatch($3->getTrueList(),$5->getQuad());
																backpatch($3->getFalseList(),$9->getQuad());
																$$->setNextList(merge(merge($6->getNextList(),$10->getNextList()),$7->getNextList()));
															}
				;
SwitchStatement : KeywordSwitch SeparatorLParen Expression SeparatorRParen SwitchBlock 	{ 									  	$$ = createSCNode($1, "SwitchStatement"); 
										$$->addChild($2);
										$$->addChild($3);
										$$->addChild($4);
										$$->addChild($5);
										if(checkSwitchType(getCurTableSTE($3->getPlace()).type)){
											$$->setType($5->getType());
											int n = getNextQuad();
											backpatch($5->getBreakList(),n);
										}
										else{
											$$->setType("type_error");
										}
									}
		;
SwitchBlock 	:  SeparatorLCuBrac    {
										   setScope(getNextBlkNo(), getCurrTable()->type);
										   emitAssign("switchExp",$<node_type>-1->getPlace());
										   enterCurTable("switchExp", getCurTableSTE($<node_type>-1->getPlace()).type, true, getTypeSize(getCurTableSTE($<node_type>-1->getPlace()).type))
									   }
		   SwitchBlockStatementGroups SeparatorRCuBrac	{
											$$ = createSCNode($1, "SwitchBlock");
											$$->addChild($3);
											$$->addChild($4);
											$$->setType($3->getType());
											$$->setBreakList($3->getBreakList());
											
											clrScope();
									}
		| SeparatorLCuBrac SeparatorRCuBrac 	{								$$ = createSCNode($1, "SwitchBlock");
							$$->addChild($2);
							$$->setType("void");
						}
		;
SwitchBlockStatementGroups 	: SwitchBlockStatementGroup 	{ $$ = createSCNode($1, "SwitchBlockStatementGroups"); 
															  $$->setType($1->getType());
															  $$->setBreakList($1->getBreakList());
															}
				| SwitchBlockStatementGroups SwitchBlockStatementGroup 	{ $$ = createSCNode($1, "SwitchBlockStatementGroups"); 
											  $$->addChild($2);
											  if($1->getType()=="type_error"){
											  	$$->setType("type_error");
											  }
											  else{
											  	$$->setType($2->getType());
											  	$$->setBreakList(merge($2->getBreakList(),$1->getBreakList()));
											  }
											}
				;
SwitchBlockStatementGroup 	: SwitchLabels BlockStatements  PatchMarker{ 
								  $$ = createSCNode($1, "SwitchBlockStatementGroup"); 
								  $$->addChild($2);
								  if($1->getType()=="type_error"){
								  	$$->setType("type_error");
								  }
								  else{
								  	$$->setType($2->getType());
								  	backpatch($1->getFalseList(),$3->getQuad());
								  	$$->setBreakList($2->getBreakList());
								  }
								}
				;
SwitchLabels	: SwitchLabel 	{ $$ = createSCNode($1, "SwitchLabels"); 
								  $$->setType($1->getType());
								  $$->setFalseList($1->getFalseList());
								}
				| SwitchLabels PatchMarker SwitchLabel 	{ $$ = createSCNode($1, "SwitchLabels"); 
						  $$->addChild($3);
						  if($1->getType()=="type_error"){
						  	$$->setType("type_error");
						  }
						  else{
						  	$$->setType($3->getType());
						  }
						  backpatch($1->getFalseList(),$2->getQuad());
						  $$->setFalseList($3->getFalseList());
						}
				;
SwitchLabel 	: KeywordCase ConstantExpression OperatorColon   {
									$$ = createSCNode($1, "SwitchLabel");
									$$->addChild($2);
									$$->addChild($3);
									if(checkSwitchType($2->getType()))
									{
										if(getCurTableSTE("switchExp").type==getCurTableSTE($2->getPlace()).type){
											$$->setType("void");
											int n = getNextQuad();
											vector<int> v2;
											v2.push_back(n+1);
											$$->setFalseList(v2);
										
											emitIf($2->getPlace(),"==","switchExp",n+2);
									    	emit("goto");
										}
										else{
											$$->setType("type_error");
										}
									}
									else
									{
										$$->setType("type_error");
									}
							}
		| KeywordDefault OperatorColon	{
							$$ = createSCNode($1, "SwitchLabel");
							$$->addChild($2);
							$$->setType("void");					
					}
		;
WhileStatement 	: KeywordWhile SeparatorLParen PatchMarker Expression SeparatorRParen PatchMarker Statement { $$ = createSCNode($1, "WhileStatement"); 
											  $$->addChild($2);
											  $$->addChild($4);
											  $$->addChild($5);
											  $$->addChild($7);
											  if(getCurTableSTE($4->getPlace()).type=="boolean"){
											  	$$->setType($7->getType());
											  	backpatch($7->getNextList(),$3->getQuad());
											    backpatch($4->getTrueList(),$6->getQuad());
											    $$->setNextList(merge($4->getFalseList(),$7->getBreakList()));
											    emit("goto",$3->getQuad());
											  }
											  else{
											  	$$->setType("type_error");
											  }
											}
		;
WhileStatementNoShortIf : KeywordWhile SeparatorLParen PatchMarker Expression SeparatorRParen PatchMarker StatementNoShortIf 	{ 											  	$$ = createSCNode($1, "WhileStatementNoShortIf"); 
												$$->addChild($2);
												$$->addChild($4);
												$$->addChild($5);
												$$->addChild($7);
												if(getCurTableSTE($4->getPlace()).type=="boolean"){
												  $$->setType($7->getType());
												  backpatch($7->getNextList(),$3->getQuad());
											      backpatch($4->getTrueList(),$6->getQuad());
											      $$->setNextList(merge($4->getFalseList(),$7->getBreakList()));
											      emit("goto",$3->getQuad());
												}
												else{
												  $$->setType("type_error");
												}
											}
			;
DoStatement : KeywordDo PatchMarker Statement GotoMarker KeywordWhile SeparatorLParen PatchMarker Expression SeparatorRParen SeparatorSemiCo 						{    $$ = createSCNode($1, "DoStatement"); 
												$$->addChild($3);
												$$->addChild($5);
												$$->addChild($6);
												$$->addChild($8);
												$$->addChild($9);
												$$->addChild($10);
												if(getCurTableSTE($8->getPlace()).type=="boolean"){
												  $$->setType($3->getType());
												  backpatch($8->getTrueList(),$2->getQuad());
											      backpatch($4->getNextList(),$7->getQuad());
											      backpatch($3->getNextList(),$7->getQuad());
											      $$->setNextList(merge($8->getFalseList(),$3->getBreakList()));
												}
												else{
												  $$->setType("type_error");
												}
											}
			;
ForStatement 	: KeywordFor ForMarker SeparatorLParen ForInit SeparatorSemiCo PatchMarker Expression SeparatorSemiCo PatchMarker ForUpdate GotoMarker SeparatorRParen PatchMarker Statement 	{ $$ = createSCNode($1, "ForStatement"); 
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											  $$->addChild($7);
											  $$->addChild($8);
											  $$->addChild($10);
											  $$->addChild($12);
											  $$->addChild($14);
											  if($4->getType()=="void" && getCurTableSTE($7->getPlace()).type=="boolean" && $10->getType()=="void"){
												$$->setType($14->getType());
												backpatch($7->getTrueList(),$13->getQuad());
												backpatch($11->getNextList(),$6->getQuad());
												backpatch($14->getNextList(),$9->getQuad());
												$$->setNextList(merge($7->getFalseList(),$14->getBreakList()));
												emit("goto",$9->getQuad());
										      }
										      else{
												  $$->setType("type_error");
											  }
											  clrScope();
											}
		| KeywordFor ForMarker SeparatorLParen ForInit SeparatorSemiCo PatchMarker Expression SeparatorSemiCo SeparatorRParen PatchMarker Statement 	{ $$ = createSCNode($1, "ForStatement"); 
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											  $$->addChild($7);
											  $$->addChild($8);
											  $$->addChild($9);
											  $$->addChild($11);
											  if($4->getType()=="void" && getCurTableSTE($7->getPlace()).type=="boolean"){
												$$->setType($11->getType());
												backpatch($7->getTrueList(),$10->getQuad());
												backpatch($11->getNextList(),$6->getQuad());
												$$->setNextList(merge($7->getFalseList(),$11->getBreakList()));
												emit("goto",$6->getQuad());
										      }
										      else{
												  $$->setType("type_error");
											  }
											  clrScope();
											}
		| KeywordFor ForMarker SeparatorLParen ForInit SeparatorSemiCo GotoMarker SeparatorSemiCo PatchMarker ForUpdate GotoMarker SeparatorRParen PatchMarker Statement 	{ $$ = createSCNode($1, "ForStatement"); 
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											  $$->addChild($7);
											  $$->addChild($9);
											  $$->addChild($11);
											  $$->addChild($13);
											  if($4->getType()=="void" && $9->getType()=="void"){
												$$->setType($13->getType());
												backpatch($6->getNextList(),$12->getQuad());
												backpatch($10->getNextList(),$12->getQuad());
												backpatch($13->getNextList(),$8->getQuad());
												$$->setNextList($13->getBreakList());
												emit("goto",$8->getQuad());
										      }
										      else{
												  $$->setType("type_error");
											  }
											  clrScope();
											}
		| KeywordFor ForMarker SeparatorLParen ForInit SeparatorSemiCo GotoMarker SeparatorSemiCo SeparatorRParen PatchMarker Statement 	{ $$ = createSCNode($1, "ForStatement"); 
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											  $$->addChild($7);
											  $$->addChild($8);
											  $$->addChild($10);
											  if($4->getType()=="void"){
												$$->setType($10->getType());
												backpatch($6->getNextList(),$9->getQuad());
												backpatch($10->getNextList(),$6->getQuad());
												$$->setNextList($10->getBreakList());
												emit("goto",$6->getQuad());
										      }
										      else{
												  $$->setType("type_error");
											  }
											  clrScope();
											}
		| KeywordFor ForMarker SeparatorLParen SeparatorSemiCo PatchMarker Expression SeparatorSemiCo PatchMarker ForUpdate GotoMarker SeparatorRParen PatchMarker Statement 	{ $$ = createSCNode($1, "ForStatement"); 
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($6);
											  $$->addChild($7);
											  $$->addChild($9);
											  $$->addChild($11);
											  $$->addChild($13);
											  if(getCurTableSTE($6->getPlace()).type=="boolean" && $9->getType()=="void"){
												$$->setType($13->getType());
												backpatch($6->getTrueList(),$12->getQuad());
												backpatch($10->getNextList(),$5->getQuad());
												backpatch($13->getNextList(),$8->getQuad());
												$$->setNextList(merge($6->getFalseList(),$13->getBreakList()));
												emit("goto",$8->getQuad());
										      }
										      else{
												  $$->setType("type_error");
											  }
											  clrScope();
											}
		| KeywordFor ForMarker SeparatorLParen SeparatorSemiCo PatchMarker Expression SeparatorSemiCo SeparatorRParen PatchMarker Statement 	{ $$ = createSCNode($1, "ForStatement"); 
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($6);
											  $$->addChild($7);
											  $$->addChild($8);
											  $$->addChild($10);
											  if(getCurTableSTE($6->getPlace()).type=="boolean"){
												$$->setType($10->getType());
												backpatch($6->getTrueList(),$9->getQuad());
												backpatch($10->getNextList(),$5->getQuad());
												$$->setNextList(merge($6->getFalseList(),$10->getBreakList()));
												emit("goto",$5->getQuad());
										      }
										      else{
												  $$->setType("type_error");
											  }
											  clrScope();
											}
		| KeywordFor ForMarker SeparatorLParen SeparatorSemiCo GotoMarker SeparatorSemiCo PatchMarker ForUpdate GotoMarker SeparatorRParen PatchMarker Statement 	{ $$ = createSCNode($1, "ForStatement"); 
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($6);
											  $$->addChild($8);
											  $$->addChild($10);
											  $$->addChild($12);
											  if($8->getType()=="void"){
												$$->setType($12->getType());
												backpatch($5->getNextList(),$11->getQuad());
												backpatch($12->getNextList(),$7->getQuad());
												backpatch($9->getNextList(),$11->getQuad());
												$$->setNextList(merge($6->getFalseList(),$12->getBreakList()));
												emit("goto",$7->getQuad());
										      }
										      else{
												  $$->setType("type_error");
											  }
											  clrScope();
											}
		| KeywordFor ForMarker SeparatorLParen SeparatorSemiCo GotoMarker SeparatorSemiCo SeparatorRParen PatchMarker Statement 	{ $$ = createSCNode($1, "ForStatement"); 
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($6);
											  $$->addChild($7);
											  $$->addChild($9);
											  $$->setType($9->getType());
											  backpatch($5->getNextList(),$8->getQuad());
											  backpatch($9->getNextList(),$5->getQuad());
											  $$->setNextList($9->getBreakList());
											  emit("goto",$5->getQuad());
											  clrScope();
											}
	    | KeywordFor ForMarker SeparatorLParen ForInit SeparatorSemiCo PatchMarker Expression SeparatorSemiCo error SeparatorRParen Statement SeparatorSemiCo { yyerrok;yyclearin;clrScope();}
		;
ForStatementNoShortIf	: KeywordFor ForMarker SeparatorLParen ForInit SeparatorSemiCo PatchMarker Expression SeparatorSemiCo PatchMarker ForUpdate GotoMarker SeparatorRParen PatchMarker StatementNoShortIf 	{ $$ = createSCNode($1, "ForStatementNoShortIf"); 
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											  $$->addChild($7);
											  $$->addChild($8);
											  $$->addChild($10);
											  $$->addChild($12);
											  $$->addChild($14);
											  if($4->getType()=="void" && getCurTableSTE($7->getPlace()).type=="boolean" && $10->getType()=="void"){
												$$->setType($14->getType());
												backpatch($7->getTrueList(),$13->getQuad());
												backpatch($11->getNextList(),$6->getQuad());
												backpatch($14->getNextList(),$9->getQuad());
												$$->setNextList(merge($7->getFalseList(),$14->getBreakList()));
												emit("goto",$9->getQuad());
										      }
										      else{
												  $$->setType("type_error");
											  }
											  clrScope();
											}
		| KeywordFor ForMarker SeparatorLParen ForInit SeparatorSemiCo PatchMarker Expression SeparatorSemiCo SeparatorRParen PatchMarker StatementNoShortIf 	{ $$ = createSCNode($1, "ForStatementNoShortIf"); 
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											  $$->addChild($7);
											  $$->addChild($8);
											  $$->addChild($9);
											  $$->addChild($11);
											  if($4->getType()=="void" && getCurTableSTE($7->getPlace()).type=="boolean"){
												$$->setType($11->getType());
												backpatch($7->getTrueList(),$10->getQuad());
												backpatch($11->getNextList(),$6->getQuad());
												$$->setNextList(merge($7->getFalseList(),$11->getBreakList()));
												emit("goto",$6->getQuad());
										      }
										      else{
												  $$->setType("type_error");
											  }
											  clrScope();
											}
		| KeywordFor ForMarker SeparatorLParen ForInit SeparatorSemiCo GotoMarker SeparatorSemiCo PatchMarker ForUpdate GotoMarker SeparatorRParen PatchMarker StatementNoShortIf 	{ $$ = createSCNode($1, "ForStatementNoShortIf"); 
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											  $$->addChild($7);
											  $$->addChild($9);
											  $$->addChild($11);
											  $$->addChild($13);
											  if($4->getType()=="void" && $9->getType()=="void"){
												$$->setType($13->getType());
												backpatch($6->getNextList(),$12->getQuad());
												backpatch($10->getNextList(),$12->getQuad());
												backpatch($13->getNextList(),$8->getQuad());
												$$->setNextList($13->getBreakList());
												emit("goto",$8->getQuad());
										      }
										      else{
												  $$->setType("type_error");
											  }
											  clrScope();
											}
		| KeywordFor ForMarker SeparatorLParen ForInit SeparatorSemiCo GotoMarker SeparatorSemiCo SeparatorRParen PatchMarker StatementNoShortIf 	{ $$ = createSCNode($1, "ForStatementNoShortIf"); 
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($5);
											  $$->addChild($7);
											  $$->addChild($8);
											  $$->addChild($10);
											  if($4->getType()=="void"){
												$$->setType($10->getType());
												backpatch($6->getNextList(),$9->getQuad());
												backpatch($10->getNextList(),$6->getQuad());
												$$->setNextList($10->getBreakList());
												emit("goto",$6->getQuad());
										      }
										      else{
												  $$->setType("type_error");
											  }
											  clrScope();
											}
		| KeywordFor ForMarker SeparatorLParen SeparatorSemiCo PatchMarker Expression SeparatorSemiCo PatchMarker ForUpdate GotoMarker SeparatorRParen PatchMarker StatementNoShortIf 	{ $$ = createSCNode($1, "ForStatementNoShortIf"); 
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($6);
											  $$->addChild($7);
											  $$->addChild($9);
											  $$->addChild($11);
											  $$->addChild($13);
											  if(getCurTableSTE($6->getPlace()).type=="boolean" && $9->getType()=="void"){
												$$->setType($13->getType());
												backpatch($6->getTrueList(),$12->getQuad());
												backpatch($10->getNextList(),$5->getQuad());
												backpatch($13->getNextList(),$8->getQuad());
												$$->setNextList(merge($6->getFalseList(),$13->getBreakList()));
												emit("goto",$8->getQuad());
										      }
										      else{
												  $$->setType("type_error");
											  }
											  clrScope();
											}
		| KeywordFor ForMarker SeparatorLParen SeparatorSemiCo PatchMarker Expression SeparatorSemiCo SeparatorRParen PatchMarker StatementNoShortIf 	{ $$ = createSCNode($1, "ForStatementNoShortIf"); 
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($6);
											  $$->addChild($7);
											  $$->addChild($8);
											  $$->addChild($10);
											  if(getCurTableSTE($6->getPlace()).type=="boolean"){
												$$->setType($10->getType());
												backpatch($6->getTrueList(),$9->getQuad());
												backpatch($10->getNextList(),$5->getQuad());
												$$->setNextList(merge($6->getFalseList(),$10->getBreakList()));
												emit("goto",$5->getQuad());
										      }
										      else{
												  $$->setType("type_error");
											  }
											  clrScope();
											}
		| KeywordFor ForMarker SeparatorLParen SeparatorSemiCo GotoMarker SeparatorSemiCo PatchMarker ForUpdate GotoMarker SeparatorRParen PatchMarker StatementNoShortIf 	{ $$ = createSCNode($1, "ForStatementNoShortIf"); 
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($6);
											  $$->addChild($8);
											  $$->addChild($10);
											  $$->addChild($12);
											  if($8->getType()=="void"){
												$$->setType($12->getType());
												backpatch($5->getNextList(),$11->getQuad());
												backpatch($12->getNextList(),$7->getQuad());
												backpatch($9->getNextList(),$11->getQuad());
												$$->setNextList(merge($6->getFalseList(),$12->getBreakList()));
												emit("goto",$7->getQuad());
										      }
										      else{
												  $$->setType("type_error");
											  }
											  clrScope();
											}
		| KeywordFor ForMarker SeparatorLParen SeparatorSemiCo GotoMarker SeparatorSemiCo SeparatorRParen PatchMarker StatementNoShortIf 	{ $$ = createSCNode($1, "ForStatementNoShortIf"); 
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->addChild($6);
											  $$->addChild($7);
											  $$->addChild($9);
											  $$->setType($9->getType());
											  backpatch($5->getNextList(),$8->getQuad());
											  backpatch($9->getNextList(),$5->getQuad());
											  emit("goto",$5->getQuad());		  
											  $$->setNextList($9->getBreakList());
											  clrScope();
											}
			;
ForInit 	: StatementExpressionList 	{ $$ = createSCNode($1, "ForInit"); 
										  $$->setType($1->getType());
										}
		| LocalVariableDeclaration 	{ $$ = createSCNode($1, "ForInit"); 
									  $$->setType($1->getType());
									}
		;
ForUpdate 	: StatementExpressionList 	{ $$ = createSCNode($1, "ForUpdate"); 
										  $$->setType($1->getType());
										}

StatementExpressionList 	: StatementExpression 	{ $$ = createSCNode($1, "StatementExpressionList");  
										  			  $$->setType($1->getType());
										  			}
				| StatementExpressionList SeparatorComma StatementExpression 	{ $$ = createSCNode($1, "StatementExpressionList"); 
																				  if($1->getType()=="void"){
																				  	$$->setType($3->getType());
																				  }
																				  else{
																				  	$$->setType("type_error");
																				  }
																				}
				;
BreakStatement	:  KeywordBreak SeparatorSemiCo	{
						$$ = createSCNode($1, "BreakStatement"); 
						$$->addChild($2);
						$$->setType("void");
						int pos = getNextQuad();
						vector<int> v;
						v.push_back(pos);
						$$->setBreakList(v);
						emit("goto");
					}
		;
ContinueStatement	: KeywordContinue SeparatorSemiCo	{ $$ = createSCNode($1, "ContinueStatement"); 
							$$->addChild($2);
							$$->setType("void");
							int pos = getNextQuad();
							vector<int> v;
							v.push_back(pos);
							$$->setNextList(v);
							emit("goto");
						}
			;
ReturnStatement	: KeywordReturn Expression SeparatorSemiCo { $$ = createSCNode($1, "ReturnStatement"); 
							$$->addChild($2);
							$$->addChild($3);
							if($2->getType()=="type_error"){
								$$->setType("type_error");
							}
							else{
								$$->setType($2->getType());
								emitReturn($2->getPlace());
								pushReturnList(getNextQuad());
								emit("goto");
							}
						}
		| KeywordReturn SeparatorSemiCo	{ $$ = createSCNode($1, "ReturnStatement"); 
						$$->addChild($2);
						$$->setType("void");
						emitReturn("");
						pushReturnList(getNextQuad());
						emit("goto");
					}
		;
Primary	: 	KeywordThis	{ $$ = createSCNode($1, "Primary"); 
							  $$->setType($1->getType());
							  $$->setVal($1->getNodeVal());
							  //TODO remove
							}
			| FieldAccess	{ $$ = createSCNode($1, "Primary"); 
							  $$->setType($1->getType());
							}
			| ArrayAccess 	{ $$ = createSCNode($1, "Primary"); 
							  $$->setType($1->getType());
							  $$->setPlace($1->getPlace());
							}
			| Literal       {
								$$ = createSCNode($1, "Primary"); 
							  $$->setType($1->getType());
							  $$->setVal($1->getNodeVal());
							  $$->setPlace($1->getPlace());
							}
			| MethodInvocation {
									$$ = createSCNode($1, "Primary"); 
							  		$$->setType($1->getType());
							  		$$->setVal($1->getNodeVal());
							        $$->setPlace($1->getPlace());
								}
			;

ClassInstanceCreationExpression	: KeywordNew ClassType SeparatorLParen SeparatorRParen	{ $$ = createSCNode($1, "ClassInstanceCreationExpression"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  $$->addChild($4);
											  $$->setType($2->getType());
											}
ArgumentList    : Expression    { $$ = createSCNode($1, "ArgumentList");
                                                                   $$->setType($1->getType());
                                                                   $$->setPlace($1->getPlace());
                                                                   $$->paramslist = new vector<string>(0);
                                                                   $$->paramslist->push_back($1->getPlace());
                                                                 }
                 | ArgumentList SeparatorComma Expression        { $$ = createSCNode($1, "ArgumentList"); 
                                                                   $$->addChild($2);
                                                                   $$->addChild($3);
                                                                   if($1->getType()=="type_error"){
                                                                         $$->setType("type_error");
                                                                   }
                                                                   else{ 
                                                                         $$->setType($3->getType());
                                                                           $$->paramslist = $1->paramslist;
                                                                           $$->paramslist->push_back($3->getPlace());				;
																		}
																}
								;
ArrayCreationExpression : KeywordNew PrimitiveType DimExprs	{ $$ = createSCNode($1, "ArrayCreationExpression"); 
							  $$->addChild($2);
							  $$->addChild($3);
							  //cerr<<"in ArrayCreationExpression "<<$3->getDim()<<endl;
							  //cerr<<"its type: "<<$3->getType();
							  if($3->getType()=="type_error"){
								$$->setType("type_error");
							  }
							  else{
								$$->setType($2->getType());
								$$->incDim($3->getDim());
								$$->setArray();
								$$->setDimVal($3->getArrayDimVals());
							  }
							}
			;
DimExprs: DimExpr 	{ $$ = createSCNode($1, "DimExprs"); 
					  $$->setArray();
					  $$->incDim($1->getDim());
					  $$->addDimVal($1->getArrayDimVals()->at(0));
					  $$->setType($1->getType());
					}
	;
DimExpr : SeparatorLSqBrac IntegerLiteral SeparatorRSqBrac 	{ $$ = createSCNode($1, "DimExpr"); 
							  $$->addChild($2);
							  $$->addChild($3);
							  $$->setArray();
							  $$->incDim(1);
							  $$->addDimVal($2->getNodeVal());
							  if($2->getType()!="void"){
							  	$$->setType("int");
							  }
							  else{
							  	$$->setType("type_error");
							  }
							}
	;
FieldAccess	: Primary SeparatorDot Identifier	{ $$ = createSCNode($1, "FieldAccess"); 
							  $$->addChild($2);
							  $$->addChild($3);
							  //TODO
							}
		;
MethodInvocation	: Name SeparatorLParen ArgumentList SeparatorRParen 	{ $$ = createSCNode($1, "MethodInvocation"); 
										  $$->addChild($2);
										  $$->addChild($3);
										  $$->addChild($4);
										  bool exists = lookupCurTable($1->getPlace());
										  if(exists)
										  {
										  	STE entry = getCurTableSTE($1->getPlace());
										  	//cerr<<$1->getPlace();
										    bool cmp = compareParams(entry,$3);
										    if(cmp){
											  	emitPushParam($3);
											  	if(entry.type=="void"){
											  		emitLCall($1->getPlace());
											  		$$->setType("void");
											  	}
											  	else{
											  		string tmp = newTemp();
													emitLCallAssign(tmp,$1->getPlace());
											  		$$->setType(entry.type);
											  		$$->setPlace(tmp);
											  		enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize(entry.type));
											  	 }
										  	}
										  	else{
										  		OtherError("Invalid arguments to function " + $1->getPlace());	
										  	}
										  }
										  else{
										  	OtherError("Usage of undefined function " + $1->getPlace());
										  }	
										  
										}
			| Name SeparatorLParen SeparatorRParen	{ $$ = createSCNode($1, "MethodInvocation"); 
								  $$->addChild($2);
								  $$->addChild($3);
								  STE entry = getCurTableSTE($1->getPlace());
								  if(lookupCurTable($1->getPlace()))
								  {
									  if(entry.paramslist->size()==0){
									  	if(entry.type=="void"){
									  		emitLCall($1->getPlace());
									  		$$->setType("void");
									  	}
									  	else{
									  		string tmp = newTemp();
											emitLCallAssign(tmp,$1->getPlace());

									  		$$->setType(entry.type);
									  		$$->setPlace(tmp);
									  		enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize(entry.type));
									  	}
									  }
									  else
									  {
									  	OtherError("Invalid number of parameters to "+$1->getPlace());
									  }
								  }
								  else{
								  	$$->setType("type_error");
								  }
								}
			| Primary SeparatorDot Identifier SeparatorLParen ArgumentList SeparatorRParen 	{ $$ = createSCNode($1, "MethodInvocation"); 
										  $$->addChild($2);
										  $$->addChild($3);
										  $$->addChild($4);
										  $$->addChild($5);
										  $$->addChild($6);
										  //TODO
										}
			| Primary SeparatorDot Identifier SeparatorLParen SeparatorRParen 	{ $$ = createSCNode($1, "MethodInvocation"); 
												  $$->addChild($2);
												  $$->addChild($3);
												  $$->addChild($4);
												  $$->addChild($5);
												  //TODO
												}
			;
ArrayAccess	: Name SeparatorLSqBrac Expression SeparatorRSqBrac	{ $$ = createSCNode($1, "ArrayAccess"); 
									  $$->addChild($2);
									  $$->addChild($3);
									  $$->addChild($4);

									  int indices = getCurTableSTE($1->getPlace()).arrayDims;
									  int size = getCurTableSTE($1->getPlace()).size;
									  if(getCurTableSTE($1->getPlace()).isArray){
									  	string tmp = newTemp();
									  	string tmp2 = newTemp();
									  	string tmp3 = newTemp();

									  	string ind = $3->getPlace();
									  	string si = numToString(size/indices);

									  	emitAssign(tmp, ind);
									  	enterCurTable(tmp,"int",true,4);
									  	emitAssignConst(tmp2, si); 
									  	enterCurTable(tmp2,"int",true,4);  
									  	//int n = getNextQuad();
									    //emitIf($3->getPlace(),"<",tmp,n+2);
									    //emitPrint("\"Array index out of bounds error\"");		// todo Its error
									    //emitGoto(n+5);
									    emit(tmp,tmp,"*",tmp2);
									    //emit(tmp,$1->getPlace(),"+",tmp);
									  	//string pre = "*(";
										//string tmp3 = pre+tmp+")";
										emitArrayRHS(tmp3,$1->getPlace(),tmp);
										
										$$->setPlace(tmp3);
										$$->setType(getCurTableSTE($1->getPlace()).type);
										enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType()));
									  }
									  else{
									  	$$->setType("type_error");
									  }
									}
		| FieldAccess SeparatorLSqBrac Expression SeparatorRSqBrac	{ $$ = createSCNode($1, "ArrayAccess"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  $$->addChild($4);
											  //TODO
											}
		;
PostfixExpression	: Primary 	{ $$ = createSCNode($1, "PostfixExpression"); 
								  $$->setType($1->getType());
								  $$->setPlace($1->getPlace());
								}
			| Name 		{ $$ = createSCNode($1, "PostfixExpression"); 
						  $$->setType($1->getType());
						  $$->setPlace($1->getPlace());
						}
UnaryExpression : OperatorPl UnaryExpressionNotPlusMinus 	{ $$ = createSCNode($1, "UnaryExpression"); 
						  $$->addChild($2);
						  $$->setType($2->getType());
						  $$->setPlace($2->getPlace());
						}
		| OperatorMi UnaryExpressionNotPlusMinus 	{ $$ = createSCNode($1, "UnaryExpression"); 
						  $$->addChild($2);
						  if(lookupCurTable($2->getPlace())){
							  	$$->setType(getCurTableSTE($2->getPlace()).type);
							  	string tmp = newTemp();
							  	emitUnaryMinus(tmp,$2->getPlace());
							  	$$->setPlace(tmp);
							  	enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType())); //TODO is size constant?
							}
							else{
								OtherError("RHS doesn't exist in symbol table : "+$2->getPlace());
							}
						}
		| UnaryExpressionNotPlusMinus 	{ $$ = createSCNode($1, "UnaryExpression"); 
										  $$->setType($1->getType());
					  					  $$->setPlace($1->getPlace());
										}
		;
UnaryExpressionNotPlusMinus	: PostfixExpression 	{ $$ = createSCNode($1, "UnaryExpressionNotPlusMinus"); 
													  $$->setType($1->getType());
					  					  			  $$->setPlace($1->getPlace());
													}
				| OperatorTild PostfixExpression 	{ $$ = createSCNode($1, "UnaryExpressionNotPlusMinus"); 
								  $$->addChild($2);
								  if(getCurTableSTE($2->getPlace()).type=="int"||getCurTableSTE($2->getPlace()).type=="boolean"){
								  		if(lookupCurTable($2->getPlace())){
										  	$$->setType(getCurTableSTE($2->getPlace()).type);
										  	backpatchNext($2->getNextList());
										    backpatchNext($2->getTrueList());
										    backpatchNext($2->getFalseList());
										    string tmp = newTemp();
										  	emitUnaryTilde(tmp,$2->getPlace());
										  	$$->setPlace(tmp);
										  	enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType()));
										}
										else{
											OtherError("RHS doesn't exist in symbol table : "+$2->getPlace());
										}
								  	}
								  	else{
								  		$$->setType("type_error");
								  	}
								}
				| OperatorExcl PostfixExpression 	{ $$ = createSCNode($1, "UnaryExpressionNotPlusMinus"); 
								  $$->addChild($2);
								  if(getCurTableSTE($2->getPlace()).type=="boolean"){
								  	if(lookupCurTable($2->getPlace())){
									  	$$->setType("boolean");
									  	string tmp = newTemp();
									  	emitUnaryNot(tmp,$2->getPlace());
									  	$$->setPlace(tmp);
									  	enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType()));
									}
									else{
										OtherError("RHS doesn't exist in symbol table : "+$2->getPlace());
									}
								  }
								  else{
								  	$$->setType("type_error");
								  }
								}
				| CastExpression 	{ $$ = createSCNode($1, "UnaryExpressionNotPlusMinus"); 
									  $$->setPlace($1->getPlace());
									  $$->setType($1->getType());
									}
				;
// Shortening CastExpression
CastExpression 	: SeparatorLParen PrimitiveType SeparatorRParen UnaryExpression { $$ = createSCNode($1, "CastExpression"); 
										  $$->addChild($2);
										  $$->addChild($3);
										  $$->addChild($4);
										  string tmp = newTemp();
										  emitConv(tmp, $4->getPlace(), getCurTableSTE($4->getPlace()).type, $2->getType());
										  $$->setPlace(tmp);
										  $$->setType($2->getType());
										  enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType()));
										}
		| SeparatorLParen Expression SeparatorRParen UnaryExpressionNotPlusMinus 	{ $$ = createSCNode($1, "CastExpression"); 
												  $$->addChild($2);
												  $$->addChild($3);
												  $$->addChild($4);
												  string tmp = newTemp();
												  emitConv(tmp, $4->getPlace(), getCurTableSTE($4->getPlace()).type, $2->getNodeVal());
												  $$->setPlace(tmp);
												  $$->setType($2->getType());
												  enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType()));
												}
		;

//Add function
ConditionalExpression 	: UnaryExpression 	{ $$ = createSCNode($1, "ConditionalExpression"); 
											  $$->setType(getCurTableSTE($1->getPlace()).type);
											  $$->setPlace($1->getPlace());
											}
						| Expression OperatorLe Expression	{ 
													  $$ = createSCNode($1, "ConditionalExpression"); 
													  $$->addChild($2);
													  $$->addChild($3);
													  if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float" || getCurTableSTE($1->getPlace()).type=="char") && (getCurTableSTE($3->getPlace()).type=="int" || getCurTableSTE($3->getPlace()).type=="float" || getCurTableSTE($3->getPlace()).type=="char")){
														  	
														  	$$->setType("boolean");
														  	int convType = getBigType(getCurTableSTE($1->getPlace()).type, getCurTableSTE($3->getPlace()).type);
														  	string newType = numToType(convType);
														  	if(newType != getCurTableSTE($1->getPlace()).type)
														  	{
														  		string tmp = newTemp();
														  		emitConv(tmp,$1->getPlace(),getCurTableSTE($1->getPlace()).type,newType); //emit convert $1 to newtype
														  		$1->setPlace(tmp);
														  		enterCurTable($1->getPlace(),newType,true,getTypeSize(newType)); 
														  	}
														  	//similar for $2
														  	if(newType != getCurTableSTE($3->getPlace()).type)
														  	{
														  		string tmp = newTemp();
														  		emitConv(tmp,$3->getPlace(),getCurTableSTE($3->getPlace()).type,newType); //emit convert $1 to newtype
														  		$3->setPlace(tmp);
														  		enterCurTable($3->getPlace(),newType,true,getTypeSize(newType)); 
														  	}
													    string tmp = newTemp();
													    int n=getNextQuad();

													    	vector<int> v1;
															v1.push_back(n+5);
															$$->setTrueList(v1);
															vector<int> v2;
															v2.push_back(n+2);
															$$->setFalseList(v2);

														    emitIf($1->getPlace(),"<",$3->getPlace(),n+4);
														    emitAssignConst(tmp,"0");
														    emit("goto");
														    emitGoto(n+6);
														    emitAssignConst(tmp,"1");
														    emit("goto");
													
													    $$->setPlace(tmp);
													    enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType()));
													  }
													  else{
													  	$$->setType("type_error");
													  }
													}
						| Expression OperatorGr Expression	{ 
													  $$ = createSCNode($1, "ConditionalExpression"); 
													  $$->addChild($2);
													  $$->addChild($3);
													  if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float" || getCurTableSTE($1->getPlace()).type=="char") && (getCurTableSTE($3->getPlace()).type=="int" || getCurTableSTE($3->getPlace()).type=="float" || getCurTableSTE($3->getPlace()).type=="char")){
														  	
														  	$$->setType("boolean");
														  	int convType = getBigType(getCurTableSTE($1->getPlace()).type, getCurTableSTE($3->getPlace()).type);
														  	string newType = numToType(convType);
														  	if(newType != getCurTableSTE($1->getPlace()).type)
														  	{
														  		string tmp = newTemp();
														  		emitConv(tmp,$1->getPlace(),getCurTableSTE($1->getPlace()).type,newType); //emit convert $1 to newtype
														  		$1->setPlace(tmp);
														  		enterCurTable($1->getPlace(),newType,true,getTypeSize(newType)); 
														  	}
														  	//similar for $2
														  	if(newType != getCurTableSTE($3->getPlace()).type)
														  	{
														  		string tmp = newTemp();
														  		emitConv(tmp,$3->getPlace(),getCurTableSTE($3->getPlace()).type,newType); //emit convert $1 to newtype
														  		$3->setPlace(tmp);
														  		enterCurTable($3->getPlace(),newType,true,getTypeSize(newType)); 
														  	}
													    string tmp = newTemp();
													    int n=getNextQuad();

													    
													    	vector<int> v1;
															v1.push_back(n+5);
															$$->setTrueList(v1);
															vector<int> v2;
															v2.push_back(n+2);
															$$->setFalseList(v2);

														    emitIf($1->getPlace(),">",$3->getPlace(),n+4);
														    emitAssignConst(tmp,"0");
														    emit("goto");
														    emitGoto(n+6);
														    emitAssignConst(tmp,"1");
														    emit("goto");
													    
													    $$->setPlace(tmp);
													    enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType()));
													  }
													  else{
													  	$$->setType("type_error");
													  }
													}
						| Expression OperatorLeEq Expression	{ 
													  $$ = createSCNode($1, "ConditionalExpression"); 
													  $$->addChild($2);
													  $$->addChild($3);
													  if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float" || getCurTableSTE($1->getPlace()).type=="char") && (getCurTableSTE($3->getPlace()).type=="int" || getCurTableSTE($3->getPlace()).type=="float" || getCurTableSTE($3->getPlace()).type=="char")){
														  	
														  	$$->setType("boolean");
														  	int convType = getBigType(getCurTableSTE($1->getPlace()).type, getCurTableSTE($3->getPlace()).type);
														  	string newType = numToType(convType);
														  	if(newType != getCurTableSTE($1->getPlace()).type)
														  	{
														  		string tmp = newTemp();
														  		emitConv(tmp,$1->getPlace(),getCurTableSTE($1->getPlace()).type,newType); //emit convert $1 to newtype
														  		$1->setPlace(tmp);
														  		enterCurTable($1->getPlace(),newType,true,getTypeSize(newType)); 
														  	}
														  	//similar for $2
														  	if(newType != getCurTableSTE($3->getPlace()).type)
														  	{
														  		string tmp = newTemp();
														  		emitConv(tmp,$3->getPlace(),getCurTableSTE($3->getPlace()).type,newType); //emit convert $1 to newtype
														  		$3->setPlace(tmp);
														  		enterCurTable($3->getPlace(),newType,true,getTypeSize(newType)); 
														  	}
													    string tmp = newTemp();
													    int n=getNextQuad();

													    	vector<int> v1;
															v1.push_back(n+5);
															$$->setTrueList(v1);
															vector<int> v2;
															v2.push_back(n+2);
															$$->setFalseList(v2);

														    emitIf($1->getPlace(),"<=",$3->getPlace(),n+4);
														    emitAssignConst(tmp,"0");
														    emit("goto");
														    emitGoto(n+6);
														    emitAssignConst(tmp,"1");
														    emit("goto");
													
													    $$->setPlace(tmp);
													    enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType()));
													  }
													  else{
													  	$$->setType("type_error");
													  }
													}
						| Expression OperatorGrEq Expression	{ 
													  $$ = createSCNode($1, "ConditionalExpression"); 
													  $$->addChild($2);
													  $$->addChild($3);
													  if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float" || getCurTableSTE($1->getPlace()).type=="char") && (getCurTableSTE($3->getPlace()).type=="int" || getCurTableSTE($3->getPlace()).type=="float" || getCurTableSTE($3->getPlace()).type=="char")){
														  	
														  	$$->setType("boolean");
														  	int convType = getBigType(getCurTableSTE($1->getPlace()).type, getCurTableSTE($3->getPlace()).type);
														  	string newType = numToType(convType);
														  	if(newType != getCurTableSTE($1->getPlace()).type)
														  	{
														  		string tmp = newTemp();
														  		emitConv(tmp,$1->getPlace(),getCurTableSTE($1->getPlace()).type,newType); //emit convert $1 to newtype
														  		$1->setPlace(tmp);
														  		enterCurTable($1->getPlace(),newType,true,getTypeSize(newType)); 
														  	}
														  	//similar for $2
														  	if(newType != getCurTableSTE($3->getPlace()).type)
														  	{
														  		string tmp = newTemp();
														  		emitConv(tmp,$3->getPlace(),getCurTableSTE($3->getPlace()).type,newType); //emit convert $1 to newtype
														  		$3->setPlace(tmp);
														  		enterCurTable($3->getPlace(),newType,true,getTypeSize(newType)); 
														  	}
													    string tmp = newTemp();
													    int n=getNextQuad();

													    	vector<int> v1;
															v1.push_back(n+5);
															$$->setTrueList(v1);
															vector<int> v2;
															v2.push_back(n+2);
															$$->setFalseList(v2);

														    emitIf($1->getPlace(),">=",$3->getPlace(),n+4);
														    emitAssignConst(tmp,"0");
														    emit("goto");
														    emitGoto(n+6);
														    emitAssignConst(tmp,"1");
														    emit("goto");
													
													    $$->setPlace(tmp);
													    enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType()));
													  }
													  else{
													  	$$->setType("type_error");
													  }
													}
						| Expression OperatorEqEq Expression	{ 
													  $$ = createSCNode($1, "ConditionalExpression"); 
													  $$->addChild($2);
													  $$->addChild($3);
													  if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float" || getCurTableSTE($1->getPlace()).type=="char") && (getCurTableSTE($3->getPlace()).type=="int" || getCurTableSTE($3->getPlace()).type=="float" || getCurTableSTE($3->getPlace()).type=="char")){
  	
														  	$$->setType("boolean");
														  	int convType = getBigType(getCurTableSTE($1->getPlace()).type, getCurTableSTE($3->getPlace()).type);
														  	string newType = numToType(convType);
														  	if(newType != getCurTableSTE($1->getPlace()).type)
														  	{
														  		string tmp = newTemp();
														  		emitConv(tmp,$1->getPlace(),getCurTableSTE($1->getPlace()).type,newType); //emit convert $1 to newtype
														  		$1->setPlace(tmp);
														  		enterCurTable($1->getPlace(),newType,true,getTypeSize(newType)); 
														  	}
														  	//similar for $2
														  	if(newType != getCurTableSTE($3->getPlace()).type)
														  	{
														  		string tmp = newTemp();
														  		emitConv(tmp,$3->getPlace(),getCurTableSTE($3->getPlace()).type,newType); //emit convert $1 to newtype
														  		$3->setPlace(tmp);
														  		enterCurTable($3->getPlace(),newType,true,getTypeSize(newType)); 
														  	}
													    string tmp = newTemp();
													    int n=getNextQuad();

													    	vector<int> v1;
															v1.push_back(n+5);
															$$->setTrueList(v1);
															vector<int> v2;
															v2.push_back(n+2);
															$$->setFalseList(v2);

														    emitIf($1->getPlace(),"==",$3->getPlace(),n+4);
														    emitAssignConst(tmp,"0");
														    emit("goto");
														    emitGoto(n+6);
														    emitAssignConst(tmp,"1");
														    emit("goto");
													
													    $$->setPlace(tmp);
													    enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType()));
													  }
													  else{
													  	$$->setType("type_error");
													  }
													}
						| Expression OperatorNE Expression	{ 
													  $$ = createSCNode($1, "ConditionalExpression");
													  $$->addChild($2);
													  $$->addChild($3);
													  if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float" || getCurTableSTE($1->getPlace()).type=="char") && (getCurTableSTE($3->getPlace()).type=="int" || getCurTableSTE($3->getPlace()).type=="float" || getCurTableSTE($3->getPlace()).type=="char")){
														  	
														  	$$->setType("boolean");
														  	int convType = getBigType(getCurTableSTE($1->getPlace()).type, getCurTableSTE($3->getPlace()).type);
														  	string newType = numToType(convType);
														  	if(newType != getCurTableSTE($1->getPlace()).type)
														  	{
														  		string tmp = newTemp();
														  		emitConv(tmp,$1->getPlace(),getCurTableSTE($1->getPlace()).type,newType); //emit convert $1 to newtype
														  		$1->setPlace(tmp);
														  		enterCurTable($1->getPlace(),newType,true,getTypeSize(newType)); 
														  	}
														  	//similar for $2
														  	if(newType != getCurTableSTE($3->getPlace()).type)
														  	{
														  		string tmp = newTemp();
														  		emitConv(tmp,$3->getPlace(),getCurTableSTE($3->getPlace()).type,newType); //emit convert $1 to newtype
														  		$3->setPlace(tmp);
														  		enterCurTable($1->getPlace(),newType,true,getTypeSize(newType)); 
														  	}
													    string tmp = newTemp();
													    int n=getNextQuad();

													    
													    	vector<int> v1;
															v1.push_back(n+2);
															$$->setTrueList(v1);
															vector<int> v2;
															v2.push_back(n+5);
															$$->setFalseList(v2);

														    emitIf($1->getPlace(),"==",$3->getPlace(),n+4);
														    emitAssignConst(tmp,"1");
														    emit("goto");
														    emitGoto(n+6);
														    emitAssignConst(tmp,"0");
														    emit("goto");
													    
													    $$->setPlace(tmp);
													    enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType()));
													  }
													  else{
													  	$$->setType("type_error");
													  }
													}
			| Expression OperatorAnd PatchMarker Expression	{ $$ = createSCNode($1, "ConditionalExpression"); 
											  $$->addChild($2);
											  $$->addChild($4);
											  if(getCurTableSTE($1->getPlace()).type=="boolean" && getCurTableSTE($4->getPlace()).type == "boolean"){
											  	$$->setType("boolean");
											  	backpatch($1->getTrueList(),$3->getQuad());
											  	$$->setTrueList($4->getTrueList());
											  	$$->setFalseList(merge($1->getFalseList(),$4->getFalseList()));
											  	string tmp = newTemp();
											  	emitBoolOp(tmp,$1->getPlace(),"And",$4->getPlace());
											  	$$->setPlace(tmp);
											  	enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType()));
											  }
											  else{
											  	$$->setType("type_error");
											  }
											}
			| Expression OperatorOr PatchMarker Expression	{ $$ = createSCNode($1, "ConditionalExpression"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  $$->addChild($4);
											  if(getCurTableSTE($1->getPlace()).type=="boolean"&&getCurTableSTE($4->getPlace()).type=="boolean"){
											  	$$->setType("boolean");
											  	backpatch($1->getFalseList(),$3->getQuad());
											  	$$->setTrueList(merge($1->getTrueList(),$4->getTrueList()));
											  	$$->setFalseList($4->getFalseList());
											  	string tmp = newTemp();
											  	emitBoolOp(tmp,$1->getPlace(),"Or",$4->getPlace());
											  	$$->setPlace(tmp);
											  	enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType()));
											  }
											  else{
											  	$$->setType("type_error");
											  }
											}
						;
ArithmeticExpression 	: Expression OperatorMul UnaryExpression {	
										  $$ = createSCNode($1, "ArithmeticExpression"); 
										  $$->addChild($2);
										  $$->addChild($3);
										  if((getCurTableSTE($1->getPlace()).type=="int"||getCurTableSTE($1->getPlace()).type=="float") && (getCurTableSTE($3->getPlace()).type=="int"||getCurTableSTE($3->getPlace()).type=="float")){
										  	arithmeticAction($$, $1, $2, $3);
										  }
										  else{
										  	$$->setType("type_error");
										  }
										}
			| Expression OperatorDiv UnaryExpression 	{ $$ = createSCNode($1, "ArithmeticExpression"); 
										  $$->addChild($2);
										  $$->addChild($3);
									  	  if((getCurTableSTE($1->getPlace()).type=="int"||getCurTableSTE($1->getPlace()).type=="float") && (getCurTableSTE($3->getPlace()).type=="int"||getCurTableSTE($3->getPlace()).type=="float")){
										  	arithmeticAction($$, $1, $2, $3);									  	  	
										  }
										  else{
										  	$$->setType("type_error");
										  }
										}
			| Expression OperatorPerc UnaryExpression { $$ = createSCNode($1, "ArithmeticExpression"); 
										  $$->addChild($2);
										  $$->addChild($3);
										  if(getCurTableSTE($1->getPlace()).type=="int" && getCurTableSTE($3->getPlace()).type=="int"){
										  	arithmeticAction($$, $1, $2, $3);
										  }
										  else{
										  	$$->setType("type_error");
										  }
										}
			| Expression OperatorMul error UnaryExpression { yyerrok;yyclearin; }
			| Expression OperatorDiv error UnaryExpression { yyerrok;yyclearin; }
			| Expression OperatorPerc error UnaryExpression { yyerrok;yyclearin; }
			| Expression OperatorPl Expression	{ $$ = createSCNode($1, "ArithmeticExpression"); // Can be for char
											  $$->addChild($2);
											  $$->addChild($3);
											  if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float" || getCurTableSTE($1->getPlace()).type=="char") && (getCurTableSTE($3->getPlace()).type=="int" || getCurTableSTE($3->getPlace()).type=="float" || getCurTableSTE($3->getPlace()).type=="char")){
									  	  		arithmeticAction($$, $1, $2, $3);
											  }
											  else{
											  	$$->setType("type_error");
											  }
											}
			| Expression OperatorMi Expression	{ $$ = createSCNode($1, "ArithmeticExpression"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float" || getCurTableSTE($1->getPlace()).type=="char") && (getCurTableSTE($3->getPlace()).type=="int" || getCurTableSTE($3->getPlace()).type=="float" || getCurTableSTE($3->getPlace()).type=="char")){
									  	  		arithmeticAction($$, $1, $2, $3);
											  }
											  else{
											  	$$->setType("type_error");
											  }
											}
			| Expression OperatorPl error UnaryExpression { yyerrok;yyclearin;}
			| Expression OperatorMi error UnaryExpression { yyerrok;yyclearin;}
		| Expression OperatorLeLe Expression	{ $$ = createSCNode($1, "ArithmeticExpression"); 
									  $$->addChild($2);
									  $$->addChild($3);

									  if(getCurTableSTE($1->getPlace()).type=="int" && getCurTableSTE($3->getPlace()).type=="int"){
									  	  	
								  	  	$$->setType("int");

									    string tmp = newTemp();
									    emit(tmp,$1->getPlace(),"<<",$3->getPlace());
									    $$->setPlace(tmp);
									    enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType()));
									  }
									  else{
									  	$$->setType("type_error");
									  }
									}
		| Expression OperatorGrGr Expression	{ $$ = createSCNode($1, "ArithmeticExpression"); 
									  $$->addChild($2);
									  $$->addChild($3);

									  if(getCurTableSTE($1->getPlace()).type=="int" && getCurTableSTE($3->getPlace()).type=="int"){
									  	  	
								  	  	$$->setType("int");

									    string tmp = newTemp();
									    emit(tmp,$1->getPlace(),">>",$3->getPlace());
									    $$->setPlace(tmp);
									    enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType()));
									  }
									  else{
									  	$$->setType("type_error");
									  }
									}
		| Expression OperatorGrGrGr Expression	{ $$ = createSCNode($1, "ArithmeticExpression"); 
									  $$->addChild($2);
									  $$->addChild($3);

									  if(getCurTableSTE($1->getPlace()).type=="int" && getCurTableSTE($3->getPlace()).type=="int"){
									  	  	
								  	  	$$->setType("int");

									    string tmp = newTemp();
									    emit(tmp,$1->getPlace(),">>>",$3->getPlace());
									    $$->setPlace(tmp);
									    enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType()));
									  }
									  else{
									  	$$->setType("type_error");
									  }

									}
		| Expression OperatorAmp Expression	{ $$ = createSCNode($1, "ArithmeticExpression"); 
								  $$->addChild($2);
								  $$->addChild($3);
							  	  if(getCurTableSTE($1->getPlace()).type==getCurTableSTE($3->getPlace()).type){
								  	  if(getCurTableSTE($1->getPlace()).type=="int"||getCurTableSTE($1->getPlace()).type=="boolean"){
								  	  	$$->setType(getCurTableSTE($1->getPlace()).type);

								  	  	backpatchNext($1->getNextList());
									    backpatchNext($1->getTrueList());
									    backpatchNext($1->getFalseList());
									    backpatchNext($3->getNextList());
									    backpatchNext($3->getTrueList());
									    backpatchNext($3->getFalseList());
									    string tmp = newTemp();
									    emit(tmp,$1->getPlace(),"&",$3->getPlace());
									    $$->setPlace(tmp);
									    enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType()));
								  	  }
								  	  else{
								  	  	$$->setType("type_error");
								  	  }
							      }
							      else{
							  	   	$$->setType("type_error");
							      }
								}
			| Expression OperatorUp Expression 	{ $$ = createSCNode($1, "ArithmeticExpression"); 
										  $$->addChild($2);
										  $$->addChild($3);
										  if(getCurTableSTE($1->getPlace()).type==getCurTableSTE($3->getPlace()).type){
										  	if(getCurTableSTE($1->getPlace()).type=="int"||getCurTableSTE($1->getPlace()).type=="boolean"){
										  		$$->setType(getCurTableSTE($1->getPlace()).type);

											  	backpatchNext($1->getNextList());
											    backpatchNext($1->getTrueList());
											    backpatchNext($1->getFalseList());
											    backpatchNext($3->getNextList());
											    backpatchNext($3->getTrueList());
											    backpatchNext($3->getFalseList());
											    string tmp = newTemp();
											    emit(tmp,$1->getPlace(),"^",$3->getPlace());
											    $$->setPlace(tmp);
											    enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType()));
										  	}
										  	else{
										  		$$->setType("type_error");
										  	}
										  }
										  else{
										  	$$->setType("type_error");
										  }
										}
		  	| Expression OperatorVert Expression	{ $$ = createSCNode($1, "ArithmeticExpression"); 
											  $$->addChild($2);
											  $$->addChild($3);
											  if(getCurTableSTE($1->getPlace()).type==getCurTableSTE($3->getPlace()).type){
											  	  if(getCurTableSTE($1->getPlace()).type=="int"||getCurTableSTE($1->getPlace()).type=="boolean"){
											  	  	$$->setType(getCurTableSTE($1->getPlace()).type);

											  	  	backpatchNext($1->getNextList());
												    backpatchNext($1->getTrueList());
												    backpatchNext($1->getFalseList());
												    backpatchNext($3->getNextList());
												    backpatchNext($3->getTrueList());
												    backpatchNext($3->getFalseList());
												    string tmp = newTemp();
												    emit(tmp,$1->getPlace(),"|",$3->getPlace());
												    $$->setPlace(tmp);
												    enterCurTable($$->getPlace(),$$->getType(),true,getTypeSize($$->getType()));
											  	  }
											  	  else{
											  	  	$$->setType("type_error");
											  	  }
										      }
										      else{
										  	   	$$->setType("type_error");
										      }
											}
		;

Assignment 	: LeftHandSide AssignmentOperator Expression	{ 
									  $$ = createSCNode($1, "Assignment"); 
									  $$->addChild($2);
									  $$->addChild($3);
									  if($2->getVal()=="="){
									  	if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float" || getCurTableSTE($1->getPlace()).type=="char" || getCurTableSTE($1->getPlace()).type=="boolean") && (getCurTableSTE($3->getPlace()).type=="int" || getCurTableSTE($3->getPlace()).type=="float" || getCurTableSTE($3->getPlace()).type=="char" || getCurTableSTE($3->getPlace()).type=="boolean")){
										  	  	$$->setType("void");
										  	  	//similar for $2
										  	  	if(getCurTableSTE($1->getPlace()).type != getCurTableSTE($3->getPlace()).type)
										  	  	{
										  	  		string tmp = newTemp();
										  	  		emitConv(tmp,$3->getPlace(),getCurTableSTE($3->getPlace()).type,getCurTableSTE($1->getPlace()).type);
										  	  		$3->setPlace(tmp);
										  	  		enterCurTable($3->getPlace(),getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type)); 
										  	  	}
											    emitAssign($1->getPlace(),$3->getPlace());
											    $$->setPlace($1->getPlace());
											  }
											  else{
											  	$$->setType("type_error");
											  }
									  }
									  if($2->getVal()=="*="){
									  	if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float") && (getCurTableSTE($3->getPlace()).type=="int" || getCurTableSTE($3->getPlace()).type=="float")){
									  	  	
										  	  	$$->setType("void");
										  	  	if(getCurTableSTE($1->getPlace()).type != getCurTableSTE($3->getPlace()).type)
										  	  	{
										  	  		string tmp = newTemp();
										  	  		emitConv(tmp,$3->getPlace(),getCurTableSTE($3->getPlace()).type,getCurTableSTE($1->getPlace()).type);
										  	  		$3->setPlace(tmp);
										  	  		enterCurTable($3->getPlace(),getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type)); 
										  	  	}
											    emit($1->getPlace(),$1->getPlace(),"*",$3->getPlace());
											    $$->setPlace($1->getPlace());
											  }
											  else{
											  	$$->setType("type_error");
											  }
									  }
									  if($2->getVal()=="/="){
									  	if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float") && (getCurTableSTE($3->getPlace()).type=="int" || getCurTableSTE($3->getPlace()).type=="float")){
									  	  	
										  	  	$$->setType("void");
										  	  	if(getCurTableSTE($1->getPlace()).type != getCurTableSTE($3->getPlace()).type)
										  	  	{
										  	  		string tmp = newTemp();
										  	  		emitConv(tmp,$3->getPlace(),getCurTableSTE($3->getPlace()).type,getCurTableSTE($1->getPlace()).type);
										  	  		$3->setPlace(tmp);
										  	  		enterCurTable($3->getPlace(),getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type)); 
										  	  	}
											    emit($1->getPlace(),$1->getPlace(),"/",$3->getPlace());
											    $$->setPlace($1->getPlace());
											  }
											  else{
											  	$$->setType("type_error");
											  }
									  }
									  if($2->getVal()=="%="){
									  	if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float") && (getCurTableSTE($3->getPlace()).type=="int" || getCurTableSTE($3->getPlace()).type=="float")){
									  	  	
										  	  	$$->setType("void");
										  	  	if(getCurTableSTE($1->getPlace()).type != getCurTableSTE($3->getPlace()).type)
										  	  	{
										  	  		string tmp = newTemp();
										  	  		emitConv(tmp,$3->getPlace(),getCurTableSTE($3->getPlace()).type,getCurTableSTE($1->getPlace()).type);
										  	  		$3->setPlace(tmp);
										  	  		enterCurTable($3->getPlace(),getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type)); 
										  	  	}
											    emit($1->getPlace(),$1->getPlace(),"%",$3->getPlace());
											    $$->setPlace($1->getPlace());
											  }
											  else{
											  	$$->setType("type_error");
											  }
									  }
									  if($2->getVal()=="+="){
									  	if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float" || getCurTableSTE($1->getPlace()).type=="char") && (getCurTableSTE($3->getPlace()).type=="int" || getCurTableSTE($3->getPlace()).type=="float" || getCurTableSTE($3->getPlace()).type=="char")){
									  	  	
										  	  	$$->setType("void");
										  	  	if(getCurTableSTE($1->getPlace()).type != getCurTableSTE($3->getPlace()).type)
										  	  	{
										  	  		string tmp = newTemp();
										  	  		emitConv(tmp,$3->getPlace(),getCurTableSTE($3->getPlace()).type,getCurTableSTE($1->getPlace()).type);
										  	  		$3->setPlace(tmp);
										  	  		enterCurTable($3->getPlace(),getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type)); 
										  	  	}
											    emit($1->getPlace(),$1->getPlace(),"+",$3->getPlace());
											    $$->setPlace($1->getPlace());
											  }
											  else{
											  	$$->setType("type_error");
											  }
									  }
									  if($2->getVal()=="-="){
									  	if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float" || getCurTableSTE($1->getPlace()).type=="char") && (getCurTableSTE($3->getPlace()).type=="int" || getCurTableSTE($3->getPlace()).type=="float" || getCurTableSTE($3->getPlace()).type=="char")){
									  	  	
										  	  	$$->setType("void");
										  	  	if(getCurTableSTE($1->getPlace()).type != getCurTableSTE($3->getPlace()).type)
										  	  	{
										  	  		string tmp = newTemp();
										  	  		emitConv(tmp,$3->getPlace(),getCurTableSTE($3->getPlace()).type,getCurTableSTE($1->getPlace()).type);
										  	  		$3->setPlace(tmp);
										  	  		enterCurTable($3->getPlace(),getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type)); 
										  	  	}
											    emit($1->getPlace(),$1->getPlace(),"-",$3->getPlace());
											    $$->setPlace($1->getPlace());
											  }
											  else{
											  	$$->setType("type_error");
											  }
									  }
									  if($2->getVal()=="<<="){
									  	if(getCurTableSTE($1->getPlace()).type=="int" && getCurTableSTE($3->getPlace()).type=="int"){
									  			$$->setType("void");
											    emit($1->getPlace(),$1->getPlace(),"<<",$3->getPlace());
											    $$->setPlace($1->getPlace());
											  }
											  else{
											  	$$->setType("type_error");
											  }
									  }
									  if($2->getVal()==">>="){
									  	if(getCurTableSTE($1->getPlace()).type=="int" && getCurTableSTE($3->getPlace()).type=="int"){
									  			$$->setType("void");
											    emit($1->getPlace(),$1->getPlace(),">>",$3->getPlace());
											    $$->setPlace($1->getPlace());
											  }
											  else{
											  	$$->setType("type_error");
											  }
									  }
									  if($2->getVal()==">>>="){
									  	if(getCurTableSTE($1->getPlace()).type=="int" && getCurTableSTE($3->getPlace()).type=="int"){
									  			$$->setType("void");
											    emit($1->getPlace(),$1->getPlace(),">>>",$3->getPlace());
											    $$->setPlace($1->getPlace());
											  }
											  else{
											  	$$->setType("type_error");
											  }
									  }
									  if($2->getVal()=="&="){
									  	if(getCurTableSTE($1->getPlace()).type==getCurTableSTE($3->getPlace()).type){
										  	  if(getCurTableSTE($1->getPlace()).type=="int"||getCurTableSTE($1->getPlace()).type=="boolean"){
										  	  	$$->setType("void");

										  	  	backpatchNext($1->getNextList());
											    backpatchNext($1->getTrueList());
											    backpatchNext($1->getFalseList());
											    backpatchNext($3->getNextList());
											    backpatchNext($3->getTrueList());
											    backpatchNext($3->getFalseList());
											    emit($1->getPlace(),$1->getPlace(),"&",$3->getPlace());
											    $$->setPlace($1->getPlace());
										  	  }
										  	  else{
										  	  	$$->setType("type_error");
										  	  }
									      }
									      else{
									  	   	$$->setType("type_error");
									      }
									  }
									  if($2->getVal()=="^="){
									  	if(getCurTableSTE($1->getPlace()).type==getCurTableSTE($3->getPlace()).type){
										  	  if(getCurTableSTE($1->getPlace()).type=="int"||getCurTableSTE($1->getPlace()).type=="boolean"){
										  	  	$$->setType("void");

										  	  	backpatchNext($1->getNextList());
											    backpatchNext($1->getTrueList());
											    backpatchNext($1->getFalseList());
											    backpatchNext($3->getNextList());
											    backpatchNext($3->getTrueList());
											    backpatchNext($3->getFalseList());
											    emit($1->getPlace(),$1->getPlace(),"^",$3->getPlace());
											    $$->setPlace($1->getPlace());
										  	  }
										  	  else{
										  	  	$$->setType("type_error");
										  	  }
									      }
									      else{
									  	   	$$->setType("type_error");
									      }
									  }
									  if($2->getVal()=="|="){
									  	if(getCurTableSTE($1->getPlace()).type==getCurTableSTE($3->getPlace()).type){
										  	  if(getCurTableSTE($1->getPlace()).type=="int"||getCurTableSTE($1->getPlace()).type=="boolean"){
										  	  	$$->setType("void");

										  	  	backpatchNext($1->getNextList());
											    backpatchNext($1->getTrueList());
											    backpatchNext($1->getFalseList());
											    backpatchNext($3->getNextList());
											    backpatchNext($3->getTrueList());
											    backpatchNext($3->getFalseList());
											    emit($1->getPlace(),$1->getPlace(),"|",$3->getPlace());
											    $$->setPlace($1->getPlace());
										  	  }
										  	  else{
										  	  	$$->setType("type_error");
										  	  }
									      }
									      else{
									  	   	$$->setType("type_error");
									      }
									  }

									}
		| Name SeparatorLSqBrac Expression SeparatorRSqBrac AssignmentOperator Expression	{ $$ = createSCNode($1, "Assignment"); 
									  $$->addChild($2);
									  $$->addChild($3);
									  $$->addChild($4);
									  $$->addChild($5);
									  $$->addChild($6);

									  int indices = getCurTableSTE($1->getPlace()).arrayDims;
									  int size = getCurTableSTE($1->getPlace()).size;
									  //cerr<<"getplace: "<<$1->getPlace()<<endl;
									  if(getCurTableSTE($1->getPlace()).isArray){
									  	string tmp5 = newTemp();
									  	string tmp2 = newTemp();
									  	string tmp3 = newTemp();

									  	string ind = $3->getPlace();
									  	string si = numToString(size/indices);
									  	//cerr<<"size is "<<si<<endl;
									  	emitAssign(tmp5, ind);
									  	enterCurTable(tmp5,"int",true,4);
									  	emitAssignConst(tmp2, si); 
									  	enterCurTable(tmp2,"int",true,4); 
									  	//int n = getNextQuad(); 
									    //emitIf($3->getPlace(),"<",tmp5,n+2);
									    //emitPrint("\"Array index out of bounds error\"");		// todo Its error
									    //emitGoto(n+5);
									    emit(tmp5,tmp5,"*",tmp2);



									 if($5->getVal()=="="){
									  	if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float" || getCurTableSTE($1->getPlace()).type=="char" || getCurTableSTE($1->getPlace()).type=="boolean") && (getCurTableSTE($6->getPlace()).type=="int" || getCurTableSTE($6->getPlace()).type=="float" || getCurTableSTE($6->getPlace()).type=="char" || getCurTableSTE($6->getPlace()).type=="boolean")){
										  	  	$$->setType("void");
										  	  	//similar for $2
										  	  	if(getCurTableSTE($1->getPlace()).type != getCurTableSTE($6->getPlace()).type)
										  	  	{
										  	  		string tmp = newTemp();
										  	  		emitConv(tmp,$6->getPlace(),getCurTableSTE($6->getPlace()).type,getCurTableSTE($1->getPlace()).type);
										  	  		$6->setPlace(tmp);
										  	  		enterCurTable($6->getPlace(),getCurTableSTE($6->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type)); 
										  	  	}
												emitArrayLHS($1->getPlace(),tmp5,$6->getPlace());
												$$->setPlace(tmp3);
											  }
											  else{
											  	$$->setType("type_error");
											  }
									  }
									  if($5->getVal()=="*="){
									  	if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float") && (getCurTableSTE($6->getPlace()).type=="int" || getCurTableSTE($6->getPlace()).type=="float")){
									  	  	
										  	  	$$->setType("void");
										  	  	if(getCurTableSTE($1->getPlace()).type != getCurTableSTE($6->getPlace()).type)
										  	  	{
										  	  		string tmp = newTemp();
										  	  		emitConv(tmp,$6->getPlace(),getCurTableSTE($6->getPlace()).type,getCurTableSTE($1->getPlace()).type);
										  	  		$6->setPlace(tmp);
										  	  		enterCurTable($6->getPlace(),getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type)); 
										  	  	}

												emitArrayRHS(tmp3,$1->getPlace(),tmp5);
												enterCurTable(tmp3,getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type));

												emit(tmp3,tmp3,"*",$6->getPlace());												

										  	  	emitArrayLHS($1->getPlace(),tmp5,tmp3);
										  	  	string tmp4 = newTemp();
												$$->setPlace(tmp4);
											  }
											  else{
											  	$$->setType("type_error");
											  }
									  }
									  if($5->getVal()=="/="){
									  	if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float") && (getCurTableSTE($6->getPlace()).type=="int" || getCurTableSTE($6->getPlace()).type=="float")){
									  	  	
										  	  	$$->setType("void");
										  	  	if(getCurTableSTE($1->getPlace()).type != getCurTableSTE($6->getPlace()).type)
										  	  	{
										  	  		string tmp = newTemp();
										  	  		emitConv(tmp,$6->getPlace(),getCurTableSTE($6->getPlace()).type,getCurTableSTE($1->getPlace()).type);
										  	  		$6->setPlace(tmp);
										  	  		enterCurTable($6->getPlace(),getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type)); 
										  	  	}

										  	  	emitArrayRHS(tmp3,$1->getPlace(),tmp5);
												enterCurTable(tmp3,getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type));

												emit(tmp3,tmp3,"/",$6->getPlace());												

										  	  	emitArrayLHS($1->getPlace(),tmp5,tmp3);
										  	  	string tmp4 = newTemp();
												$$->setPlace(tmp4);
											  }
											  else{
											  	$$->setType("type_error");
											  }
									  }
									  if($5->getVal()=="%="){
									  	if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float") && (getCurTableSTE($6->getPlace()).type=="int" || getCurTableSTE($6->getPlace()).type=="float")){
									  	  	
										  	  	$$->setType("void");
										  	  	if(getCurTableSTE($1->getPlace()).type != getCurTableSTE($6->getPlace()).type)
										  	  	{
										  	  		string tmp = newTemp();
										  	  		emitConv(tmp,$6->getPlace(),getCurTableSTE($6->getPlace()).type,getCurTableSTE($1->getPlace()).type);
										  	  		$6->setPlace(tmp);
										  	  		enterCurTable($6->getPlace(),getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type)); 
										  	  	}

										  	  	emitArrayRHS(tmp3,$1->getPlace(),tmp5);
												enterCurTable(tmp3,getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type));

												emit(tmp3,tmp3,"%",$6->getPlace());												

										  	  	emitArrayLHS($1->getPlace(),tmp5,tmp3);
										  	  	string tmp4 = newTemp();
												$$->setPlace(tmp4);
											  }
											  else{
											  	$$->setType("type_error");
											  }
									  }
									  if($5->getVal()=="+="){
									  	if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float" || getCurTableSTE($1->getPlace()).type=="char") && (getCurTableSTE($6->getPlace()).type=="int" || getCurTableSTE($6->getPlace()).type=="float" || getCurTableSTE($6->getPlace()).type=="char")){
									  	  	
										  	  	$$->setType("void");
										  	  	if(getCurTableSTE($1->getPlace()).type != getCurTableSTE($6->getPlace()).type)
										  	  	{
										  	  		string tmp = newTemp();
										  	  		emitConv(tmp,$6->getPlace(),getCurTableSTE($6->getPlace()).type,getCurTableSTE($1->getPlace()).type);
										  	  		$6->setPlace(tmp);
										  	  		enterCurTable($6->getPlace(),getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type)); 
										  	  	}
											    emitArrayRHS(tmp3,$1->getPlace(),tmp5);
												enterCurTable(tmp3,getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type));

												emit(tmp3,tmp3,"+",$6->getPlace());												

										  	  	emitArrayLHS($1->getPlace(),tmp5,tmp3);
										  	  	string tmp4 = newTemp();
												$$->setPlace(tmp4);
											  }
											  else{
											  	$$->setType("type_error");
											  }
									  }
									  if($5->getVal()=="-="){
									  	if((getCurTableSTE($1->getPlace()).type=="int" || getCurTableSTE($1->getPlace()).type=="float" || getCurTableSTE($1->getPlace()).type=="char") && (getCurTableSTE($6->getPlace()).type=="int" || getCurTableSTE($6->getPlace()).type=="float" || getCurTableSTE($6->getPlace()).type=="char")){
									  	  	
										  	  	$$->setType("void");
										  	  	if(getCurTableSTE($1->getPlace()).type != getCurTableSTE($6->getPlace()).type)
										  	  	{
										  	  		string tmp = newTemp();
										  	  		emitConv(tmp,$6->getPlace(),getCurTableSTE($6->getPlace()).type,getCurTableSTE($1->getPlace()).type);
										  	  		$6->setPlace(tmp);
										  	  		enterCurTable($6->getPlace(),getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type)); 
										  	  	}
											    emitArrayRHS(tmp3,$1->getPlace(),tmp5);
												enterCurTable(tmp3,getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type));

												emit(tmp3,tmp3,"-",$6->getPlace());												

										  	  	emitArrayLHS($1->getPlace(),tmp5,tmp3);
										  	  	string tmp4 = newTemp();
												$$->setPlace(tmp4);
											  }
											  else{
											  	$$->setType("type_error");
											  }
									  }
									  if($5->getVal()=="<<="){
									  	if(getCurTableSTE($1->getPlace()).type=="int" && getCurTableSTE($6->getPlace()).type=="int"){
									  			$$->setType("void");
											    emitArrayRHS(tmp3,$1->getPlace(),tmp5);
												enterCurTable(tmp3,getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type));

												emit(tmp3,tmp3,"<<",$6->getPlace());												

										  	  	emitArrayLHS($1->getPlace(),tmp5,tmp3);
										  	  	string tmp4 = newTemp();
												$$->setPlace(tmp4);
											  }
											  else{
											  	$$->setType("type_error");
											  }
									  }
									  if($5->getVal()==">>="){
									  	if(getCurTableSTE($1->getPlace()).type=="int" && getCurTableSTE($6->getPlace()).type=="int"){
									  			$$->setType("void");
											    emitArrayRHS(tmp3,$1->getPlace(),tmp5);
												enterCurTable(tmp3,getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type));

												emit(tmp3,tmp3,">>",$6->getPlace());												

										  	  	emitArrayLHS($1->getPlace(),tmp5,tmp3);
										  	  	string tmp4 = newTemp();
												$$->setPlace(tmp4);
											  }
											  else{
											  	$$->setType("type_error");
											  }
									  }
									  if($5->getVal()==">>>="){
									  	if(getCurTableSTE($1->getPlace()).type=="int" && getCurTableSTE($6->getPlace()).type=="int"){
									  			$$->setType("void");
											    emitArrayRHS(tmp3,$1->getPlace(),tmp5);
												enterCurTable(tmp3,getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type));

												emit(tmp3,tmp3,">>>",$6->getPlace());												

										  	  	emitArrayLHS($1->getPlace(),tmp5,tmp3);
										  	  	string tmp4 = newTemp();
												$$->setPlace(tmp4);
											  }
											  else{
											  	$$->setType("type_error");
											  }
									  }
									  if($5->getVal()=="&="){
									  	if(getCurTableSTE($1->getPlace()).type==getCurTableSTE($6->getPlace()).type){
										  	  if(getCurTableSTE($1->getPlace()).type=="int"||getCurTableSTE($1->getPlace()).type=="boolean"){
										  	  	$$->setType("void");

										  	  	backpatchNext($1->getNextList());
											    backpatchNext($1->getTrueList());
											    backpatchNext($1->getFalseList());
											    backpatchNext($6->getNextList());
											    backpatchNext($6->getTrueList());
											    backpatchNext($6->getFalseList());
											    emitArrayRHS(tmp3,$1->getPlace(),tmp5);
												enterCurTable(tmp3,getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type));

												emit(tmp3,tmp3,"&",$6->getPlace());												

										  	  	emitArrayLHS($1->getPlace(),tmp5,tmp3);
										  	  	string tmp4 = newTemp();
												$$->setPlace(tmp4);
										  	  }
										  	  else{
										  	  	$$->setType("type_error");
										  	  }
									      }
									      else{
									  	   	$$->setType("type_error");
									      }
									  }
									  if($5->getVal()=="^="){
									  	if(getCurTableSTE($1->getPlace()).type==getCurTableSTE($6->getPlace()).type){
										  	  if(getCurTableSTE($1->getPlace()).type=="int"||getCurTableSTE($1->getPlace()).type=="boolean"){
										  	  	$$->setType("void");

										  	  	backpatchNext($1->getNextList());
											    backpatchNext($1->getTrueList());
											    backpatchNext($1->getFalseList());
											    backpatchNext($6->getNextList());
											    backpatchNext($6->getTrueList());
											    backpatchNext($6->getFalseList());
											    emitArrayRHS(tmp3,$1->getPlace(),tmp5);
												enterCurTable(tmp3,getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type));

												emit(tmp3,tmp3,"^",$6->getPlace());												

										  	  	emitArrayLHS($1->getPlace(),tmp5,tmp3);
										  	  	string tmp4 = newTemp();
												$$->setPlace(tmp4);
										  	  }
										  	  else{
										  	  	$$->setType("type_error");
										  	  }
									      }
									      else{
									  	   	$$->setType("type_error");
									      }
									  }
									  if($5->getVal()=="|="){
									  	if(getCurTableSTE($1->getPlace()).type==getCurTableSTE($6->getPlace()).type){
										  	  if(getCurTableSTE($1->getPlace()).type=="int"||getCurTableSTE($1->getPlace()).type=="boolean"){
										  	  	$$->setType("void");

										  	  	backpatchNext($1->getNextList());
											    backpatchNext($1->getTrueList());
											    backpatchNext($1->getFalseList());
											    backpatchNext($6->getNextList());
											    backpatchNext($6->getTrueList());
											    backpatchNext($6->getFalseList());
											    emitArrayRHS(tmp3,$1->getPlace(),tmp5);
												enterCurTable(tmp3,getCurTableSTE($1->getPlace()).type,true,getTypeSize(getCurTableSTE($1->getPlace()).type));

												emit(tmp3,tmp3,"|",$6->getPlace());												

										  	  	emitArrayLHS($1->getPlace(),tmp5,tmp3);
										  	  	string tmp4 = newTemp();
												$$->setPlace(tmp4);
										  	  }
										  	  else{
										  	  	$$->setType("type_error");
										  	  }
									      }
									      else{
									  	   	$$->setType("type_error");
									      }
									  }



									    
									  }
									  else{
									  	$$->setType("type_error");
									  }
									}
		;
LeftHandSide	: Name 	{ $$ = createSCNode($1, "LeftHandSide");
					       $$->setPlace($1->getPlace());
					       $$->setType(getCurTableSTE($1->getPlace()).type);
						}
		| FieldAccess	{ $$ = createSCNode($1, "LeftHandSide"); }
		;
AssignmentOperator	: OperatorEq 	{ 	
										$$ = createSCNode($1, "AssignmentOperator"); 
										$$->setVal($1->getNodeVal());
									}
			| OperatorAsEq 	{ 
								$$ = createSCNode($1, "AssignmentOperator"); 
							  	$$->setVal($1->getNodeVal());
							}
			| OperatorSlEq	{ 
								$$ = createSCNode($1, "AssignmentOperator"); 
							  	$$->setVal($1->getNodeVal());
							}
			| OperatorPercEq	{ 
									$$ = createSCNode($1, "AssignmentOperator"); 
								  	$$->setVal($1->getNodeVal());
								}
			| OperatorPlEq	{ 
								$$ = createSCNode($1, "AssignmentOperator"); 
							  	$$->setVal($1->getNodeVal());
							}
			| OperatorMiEq	{ 
								$$ = createSCNode($1, "AssignmentOperator"); 
							  	$$->setVal($1->getNodeVal());
							}
			| OperatorLeLeEq	{ 
									$$ = createSCNode($1, "AssignmentOperator"); 
								  	$$->setVal($1->getNodeVal());
								}
			| OperatorGrGrEq 	{ 
									$$ = createSCNode($1, "AssignmentOperator"); 
								  	$$->setVal($1->getNodeVal());
								}
			| OperatorGrGrGrEq	{ 
									$$ = createSCNode($1, "AssignmentOperator"); 
								  	$$->setVal($1->getNodeVal());
								}
			| OperatorAmpEq	{ 
								$$ = createSCNode($1, "AssignmentOperator"); 
							  	$$->setVal($1->getNodeVal());
							}
			| OperatorUpEq	{ 
								$$ = createSCNode($1, "AssignmentOperator"); 
							  	$$->setVal($1->getNodeVal());
							}
			| OperatorVertEq	{ 
									$$ = createSCNode($1, "AssignmentOperator"); 
								  	$$->setVal($1->getNodeVal());
								}
			;
Expression 	: ConditionalExpression 	{ $$ = createSCNode($1, "Expression");  
										  $$->setType(getCurTableSTE($1->getPlace()).type);
										  $$->setPlace($1->getPlace());
										  $$->setTrueList($1->getTrueList());
										  $$->setFalseList($1->getFalseList());
										}
			| ArithmeticExpression		{ $$ = createSCNode($1, "Expression");  
										  $$->setType(getCurTableSTE($1->getPlace()).type);
										  $$->setPlace($1->getPlace());
										}
            | ClassInstanceCreationExpression   { $$ = createSCNode($1, "Primary"); 
                                                  $$->setType($1->getType());
                                                  $$->setPlace($1->getPlace());
                                                }
		;
ConstantExpression	: Expression 	{ $$ = createSCNode($1, "ConstantExpression"); 
									  if($1->getType()!="void"){
									  	$$->setType(getCurTableSTE($1->getPlace()).type);
									  	$$->setPlace($1->getPlace());
									  }
									  else{
									  	$$->setType($1->getType());
									  }
									}
			;

MultTypeMarker: {
			$$ = new Node("MultTypeMarker", "");
			$$->setType($<node_type>-2->getType());
		}
		;

PatchMarker: {
				int pos = getNextQuad();
				$$ = new Node("PatchMarker", "");
				$$->setQuad(pos);
			};

GotoMarker: {
				int pos = getNextQuad();
				vector<int> v;
				v.push_back(pos);
				$$->setNextList(v);
				emit("goto");
			};
MethodMarker : {
				$$ = new Node("MethodMarker", "");
			  ST * temp = getCurrTable();
  			  string mthdname = $<node_type>0->getNodeVal();
  			  ST * temp2 = new ST(temp);
			  temp2->name = mthdname;
			  temp2->type = $<node_type>-1->getType();	
			  if($<node_type>-1->getNodeVal() == "void") temp2->type = "void";
			  setCurrTable(temp2);
			  if(temp->lookup(mthdname)){
  			  	OtherError("Method redeclared with same name as another method/variable: " + mthdname);
  			  }

			  temp->enter(mthdname, temp2->type, getCurrTable());
			  pushTabToStack(getCurrTable());
			  string mtdHead = "";
			  mtdHead = mtdHead+temp->name+"."+mthdname; //_Classname->methodname
		  	  
		  	  if(mthdname=="main"){
  			  	setMainOcc();
  			  	emitLabel(mtdHead);
  			  	emitLabel("main_func");
  			  	if(temp2->type!="void"){
  			  		OtherError("Main not returning void error");
  			  	}
  			  }
  			  else {
  			  	emitLabel(mtdHead);
  			  }
		  	  setSizeLine(getNextQuad());
		  	  emitMLabel("BeginFunc"); 
		  	  initReturnList();
			};
ForMarker : {
				setScope(getNextBlkNo(), getCurrTable()->type);
			};

%%

int errorCount;
void initializeError()
{
	errorCount=0;
	initTypeError();
	initOtherError();
}
void incError()
{
	errorCount++;
}
int getErrorCount()
{
	return errorCount;
}
Node * createSCNode(Node * descend, string type, string value)		//Create a node with single child
{
	Node * n = new Node(type, value);
	n->addChild(descend);
	return n;
}

Node * root;
ST * rootTable;
ST * currTable;

void assignRoot(Node * n)
{
	root = n;
}
void printToDot(string filename)
{
	ofstream outfile;
	outfile.open(filename.c_str());

	// Write initial part to outfile 
	outfile<<"digraph G{ "<<endl;

	queue<Node *> q;
	Node * t;
	Node * next;
	
	int nextNo = 1;
	q.push(root);
	
	map<int, string> labels;
	map<int, string> vals;
	while(!q.empty())
	{
		t = q.front(); q.pop();
		labels[t->nodeNo] = t->getNodeType();
		for(int i = 0; i< t->child.size(); ++i)
		{
			next = t->child[i];
			next->nodeNo = nextNo;
			outfile<<t->nodeNo<<"->"<<nextNo<<";"<<endl;		
			q.push(next);
			nextNo++;
		}

		if(t->child.size() == 0)	//leaf node
		{
			vals[t->nodeNo] = t->getNodeVal();
		}
	}

	for(map<int, string>::iterator it = labels.begin(); it!=labels.end(); it++)
	{
		if(vals.find(it->first) == vals.end())	//not a leaf
		{
			outfile<<it->first<<" "<<"[label=\""<<it->second<<"\"];"<<endl;
		}
		else
		{
			outfile<<it->first<<" "<<"[label=\""<<it->second<<"\\n\\n"<<vals[it->first]<<" \"];"<<endl;
		}
	}
	outfile<<"}"<<endl;
	outfile.close();
}

void initRootTable()
{
	rootTable = new ST(NULL);
	currTable = rootTable;
}

bool lookupCurTable(const string &name)
{
	return currTable->lookup(name);
}

void enterCurTable(const string &name, const string &type, bool value, int offset)
{
	currTable->enter(name, type, value, offset);
}

void enterCurTable(const string &name, const string &type, bool value, int offset, int v1)
{
	currTable->enter(name, type, value, offset*v1);
	STE* t1 = currTable->getSTEP(name); t1->arrayDims = v1;
	STE* t2 = currTable->getSTEP(name); t2->isArray = true;
}

bool checkSwitchType(const string & s)
{
	if(s == "int" || s == "byte" || s == "short" || s == "char")
	return true;
	return false;
}

STE getCurTableSTE(const string &name)
{
	return currTable->getSTE(name);
}

bool lookupLocCurTable(const string &name)
{
	return currTable->lookupLoc(name);
}

QuadsList * quads;
void initQuads()
{
	quads = new QuadsList();
}

void emitGoto(const string& label)
{
	quads->emit(label, JUMP, GOTOLBL);
    quads->setST(currTable);
}
void emitGoto(int lno)
{
	quads->emit(quads->NoToStr(lno), JUMP, GOTO);
    quads->setST(currTable);
}
void emitLabel(const string& label)
{
	quads->emit(label, JUMPLABEL, NOP);
    quads->setST(currTable);
}

int getNextQuad()
{
	quads->getNextInstr();
}

vector<int> merge(const vector<int>& v1,const vector<int>& v2){
	vector<int> v1v2 = v1;
	v1v2.insert(v1v2.end(), v2.begin(), v2.end());
	return v1v2;
}

void backpatch(const vector<int>& v1, int to)
{
	quads->backpatch(v1, to);
}

void backpatchNext(const vector<int> &v1)
{
	for(int i=0; i< v1.size(); ++i)
	{
		quads->backpatch(v1[i], v1[i]+1);
	}
}

void emit(const string& emitted)
{
	quads->emit("", JUMP, GOTO);
    quads->setST(currTable);
}
void emit(const string& emitted,int quad)
{
	quads->emit(quads->NoToStr(quad), JUMP, GOTO);
    quads->setST(currTable);
}

//all are TACType = ARITH
void emit(const string& place, const string& operand1, const string& opt, const string& operand2)
{
	STE tempSTE1 = (getCurTableSTE(operand1));
	STE tempSTE2 = (getCurTableSTE(operand2));
	OPType op;
	//all are TACType = ARITH
	if(opt == "+")
	{
		//float + float
		if(tempSTE1.type == "float" && tempSTE2.type == "float")
		{
			op = FLOATADD;
		}
		//int+int
		else if(tempSTE1.type == "int" && tempSTE2.type == "int")
		{
			op = INTADD;
		}
		else if(tempSTE1.type == "char" && tempSTE2.type == "char")
		{
			op = CHARADD;
		}
		else
		{
			//ERROR
		}
	}
	else if(opt == "-")
	{
		//float + float
		if(tempSTE1.type == "float" && tempSTE2.type == "float")
		{
			op = FLOATSUB;
		}
		//int+int
		else if(tempSTE1.type == "int" && tempSTE2.type == "int")
		{
			op = INTSUB;
		}
		else if(tempSTE1.type == "char" && tempSTE2.type == "char")
		{
			op = CHARSUB;
		}
		else
		{
			//ERROR
		}
	}
	else if(opt == "*")
	{
		//float * float
		if(tempSTE1.type == "float" && tempSTE2.type == "float")
		{
			op = FLOATMULT;
		}
		//int * int
		else if(tempSTE1.type == "int" && tempSTE2.type == "int")
		{
			op = INTMULT;
		}
		else
		{
			//ERROR
		}
	}
	else if(opt == "/")
	{
		//float / float
		if(tempSTE1.type == "float" && tempSTE2.type == "float")
		{
			op = FLOATDIV;
		}
		//int / int
		else if(tempSTE1.type == "int" && tempSTE2.type == "int")
		{
			op = INTDIV;
		}
		else
		{
			//ERROR
		}
	}
	else if(opt == "%")
	{
		//int / int
		if(tempSTE1.type == "int" && tempSTE2.type == "int")
		{
			op = INTMOD;
		}
		else
		{
			//ERROR
		}
	}
	
	else if(opt == "<<") op = SHIFTL;
	else if(opt == ">>") op = SHIFTR;
	else if(opt == "&") op = BITAND;
	else if(opt == "|") op = BITOR;
	else if(opt == "^") op = BITXOR;
	quads->emit(operand1, ARITH, op, operand2, place);
    quads->setST(currTable);
}

void emitBoolOp(const string& place, const string& operand1, const string& opt, const string& operand2)
{
	OPType op;
	if(opt == "And") op = BOOLAND;
	else if(opt == "Or") op = BOOLOR;
	quads->emit(operand1, BOOLEXP, op, operand2, place);
    quads->setST(currTable);
}


void emitIf(const string& expr,int quad)
{
	quads->emit(expr, IFGOTO, NOP, quads->NoToStr(quad));
    quads->setST(currTable);
}

void emitIf(const string& operand1, const string& comp,const string& operand2,int quad)
{
	OPType op;
	if(comp == "<")
	{
		op = LESSTHAN;
	}
	else if(comp == ">")
	{
		op = GRTHAN;
	}
	else if(comp == "<=")
	{
		op = LESSEQ;
	}
	else if(comp == ">=")
	{
		op = GREQ;
	}
	else if(comp == "==")
	{
		op = EQ;
	}
	quads->emit(operand1, IFCOMPGOTO, op, operand2, quads->NoToStr(quad));
    quads->setST(currTable);
}

void emitAssign(const string& tmp, const string& val)
{
	quads->emit(tmp, ASSIGN, EQASSIGN, val);
    quads->setST(currTable);
}

void emitAssignConst(const string& tmp, const string& val)
{
	quads->emit(tmp, ASSIGN, EQASSIGNCONST, val);
    quads->setST(currTable);
}

void emitAssignString(const string& tmp, const string& val)
{
	quads->emit(tmp, ASSIGN, EQASSIGNSTR, val);
	quads->setST(currTable);
}

void emitUnaryMinus(const string& operand1,const string& operand2)
{
	quads->emit(operand1, ASSIGN, MINUSASSIGN, operand2);
    quads->setST(currTable);
}

void emitUnaryNot(const string& operand1,const string& operand2)
{
	quads->emit(operand1, ASSIGN, UNARYNOT, operand2);
    quads->setST(currTable);
}


void emitUnaryTilde(const string& operand1,const string& operand2)
{
	quads->emit(operand1, ASSIGN, UNARYTILDE, operand2);
    quads->setST(currTable);
}

void emitConv(const string& tmp, const string& operand,const string& fromType,const string& toType)
{
	OPType op;
	if(fromType == "int" && toType == "float")
	{
		op = INTTOFLOAT;
	}
	else if(fromType == "float" && toType == "int")
	{
		op = FLOATTOINT;
	}
	else if(fromType == "char" && toType == "int")
	{
		op = CHARTOINT; 
	}
	else if(fromType == "int" && toType == "char")
	{
		op = INTTOCHAR;
	}
	//else error? TODO
	quads->emit(tmp, ASSIGN, op, operand);
    quads->setST(currTable);
}

string newTemp()
{
	return quads->getNewTemp();
}

int getBigType(const string& type1,const string& type2)
{
	//CHAR<INT<FLOAT
	if(type1 == "float" || type2 == "float")return 20;
	else if(type1 == "int" || type2 == "int")return 10;
	return 0;
}
string numToType(int type)
{
	if(type == 20) return "float";
	if(type == 10) return "int";
	return "char";
}
void print3AC()
{
	cout<<"Printing"<<endl<<"-----------------"<<endl;
	quads->printQuads();
	cout<<endl<<"--------------"<<endl;
}

void printSymbTable()
{
	cout<<"SymbolTable"<<endl;
	rootTable->print(0);
	cout<<"------"<<endl;
}

stack<ST *> *tblptr;
void initStack()
{
	tblptr = new stack<ST *>();
	tblptr->push(currTable);
}

void pushTabToStack(ST * t)
{
	tblptr->push(t);
	currTable = t;
}

ST * popTabFromStack()
{
	ST * s = tblptr->top();
	tblptr->pop();
	currTable = tblptr->top();
	return s;
}
ST * topTabFromStack()
{
	return tblptr->top();
}
void init()
{
	 initRootTable();	//initialise root symbol table
	 initQuads();		//initialise quads
	 initStack();
}
ST * getCurrTable()
{
	return currTable;
}
void setCurrTable(ST * tab)
{
	currTable = tab;
}
static int blkNo = 0;

string getNextBlkNo()
{
	return quads->NoToStr(blkNo++);
}
void setScope(const string& name, const string& type)
{
	ST * temp = getCurrTable();
	ST * temp2 = new ST(topTabFromStack());
	temp2->name = name;
	temp2->type = type;
	temp2->baseOffset = temp->width + temp->baseOffset;
	setCurrTable(temp2);
	pushTabToStack(getCurrTable());
	temp->enter(name, type, getCurrTable());

}
void clrScope()
{
	setCurrTable(popTabFromStack());
	ST * temp = topTabFromStack();
	int w = getCurrTable()->getWidth();
	temp->addWidth(w);
	setCurrTable(temp);
}
void conditionalAction(Node * res, Node * operand1, const string& comp, Node * operand2, Node * comparator)
{
  res = createSCNode(operand1, "ConditionalExpression"); 
  res->addChild(comparator);
  res->addChild(operand2);
  if((getCurTableSTE(operand1->getPlace()).type=="int" || getCurTableSTE(operand1->getPlace()).type=="float" || getCurTableSTE(operand1->getPlace()).type=="char") && (getCurTableSTE(operand2->getPlace()).type=="int" || getCurTableSTE(operand2->getPlace()).type=="float" || getCurTableSTE(operand2->getPlace()).type=="char")){
	  	
	  	res->setType("boolean");
	  	int convType = getBigType(getCurTableSTE(operand1->getPlace()).type, getCurTableSTE(operand2->getPlace()).type);
	  	string newType = numToType(convType);
	  	if(newType != getCurTableSTE(operand1->getPlace()).type)
	  	{
	  		string tmp = newTemp();
	  		emitConv(tmp,operand1->getPlace(),getCurTableSTE(operand1->getPlace()).type,newType); //emit convert operand1 to newtype
	  		operand1->setPlace(tmp);
	  		enterCurTable(operand1->getPlace(),newType,true,getTypeSize(newType)); 
	  	}
	  	//similar for comparator
	  	if(newType != getCurTableSTE(operand2->getPlace()).type)
	  	{
	  		string tmp = newTemp();
	  		emitConv(tmp,operand2->getPlace(),getCurTableSTE(operand2->getPlace()).type,newType); //emit convert operand1 to newtype
	  		operand2->setPlace(tmp);
	  		enterCurTable(operand2->getPlace(),newType,true,getTypeSize(newType)); 
	  	}
    string tmp = newTemp();
    int n=getNextQuad();

    if(comp!="!="){
    	vector<int> v1;
		v1.push_back(n+5);
		res->setTrueList(v1);
		vector<int> v2;
		v2.push_back(n+2);
		res->setFalseList(v2);

	    emitIf(operand1->getPlace(),comp,operand2->getPlace(),n+4);
	    emitAssignConst(tmp,"0");
	    emit("goto");
	    emitGoto(n+6);
	    emitAssignConst(tmp,"1");
	    emit("goto");
    }
    else{
    	vector<int> v1;
		v1.push_back(n+2);
		res->setTrueList(v1);
		vector<int> v2;
		v2.push_back(n+5);
		res->setFalseList(v2);

	    emitIf(operand1->getPlace(),"==",operand2->getPlace(),n+4);
	    emitAssignConst(tmp,"1");
	    emit("goto");
	    emitGoto(n+6);
	    emitAssignConst(tmp,"0");
	    emit("goto");
    }
    res->setPlace(tmp);
    enterCurTable(res->getPlace(),res->getType(),true,getTypeSize(res->getType()));
  }
  else{
  	res->setType("type_error");
  }
}
void arithmeticAction(Node *res, Node * op1, Node * op2, Node * op3)
{
		int convType = getBigType(getCurTableSTE(op1->getPlace()).type, getCurTableSTE(op3->getPlace()).type);
	  	string newType = numToType(convType);
	  	res->setType(newType);
  	  	if(newType != getCurTableSTE(op1->getPlace()).type)
  	  	{
  	  		string tmp = newTemp();
  	  		emitConv(tmp,op1->getPlace(),getCurTableSTE(op1->getPlace()).type,newType); //emit convert op1 to newtype
  	  		op1->setPlace(tmp);
  	  		enterCurTable(op1->getPlace(),newType,true,getTypeSize(op1->getType())); 
  	  	}
  	  	//similar for $2
  	  	if(newType != getCurTableSTE(op3->getPlace()).type)
  	  	{
  	  		string tmp = newTemp();
  	  		emitConv(tmp,op3->getPlace(),getCurTableSTE(op3->getPlace()).type,newType);
  	  		op3->setPlace(tmp);
  	  		enterCurTable(op3->getPlace(),newType,true,getTypeSize(op3->getType())); 
  	  	}

	    string tmp2 = newTemp();
	    emit(tmp2,op1->getPlace(),op2->getNodeVal() ,op3->getPlace());
	    res->setPlace(tmp2);
	    enterCurTable(res->getPlace(),res->getType(),true,getTypeSize(res->getType()));
}

string numToString(int num)
{
	return quads->NoToStr(num);
}
void emitPrint(string s)
{
	quads->emit(s,PRINTSTR,NOP);
    quads->setST(currTable);	
}

void emitArray(const string& name, const string& type, int indices)
{
	quads->emit(name, INITARRAY, NOP, type, quads->NoToStr(indices));	//name = type[indices]
    quads->setST(currTable);
}
void setCurrTableName(const string& name)
{
	currTable->name = name;
}
int sizeLine;
void setSizeLine(int quad)
{
	sizeLine = quad;
}
int getSizeLine()
{
	return sizeLine;
}
void emitMLabel(const string& label)
{
	quads->emit(label, METHODLBL, NOP);
	quads->setST(currTable);
}
void updateLineSize(int destLine, int mSize)
{
	quads->updateMethodLine(destLine, mSize);
}
void emitLCall(const string& func)
{
	quads->emit(func, LOCAL_FUNCCALL, NOP);	//LCall _func
	quads->setST(currTable);
}
void emitLCallAssign(const string& tmp, const string& func)
{
	quads->emit(func, LOCAL_FUNCCALL_ASSIGN, NOP, tmp); //tmp = LCall _func
	quads->setST(currTable);
}
void emitReturn(const string& var)
{
	quads->emit(var, RETURN, NOP); //return var	
	quads->setST(currTable);
}
bool compareParams(STE t, Node* arg)
{
	int paramLen = t.paramslist->size();
	for(int i=0; i<t.paramslist->size(); ++i)
	{
		if(!lookupCurTable(arg->paramslist->at(i))) return false;

		if(t.paramslist->at(paramLen - 1 - i) != getCurTableSTE(arg->paramslist->at(i)).type) return false;
	}
	return true;
}
void emitPushParam(Node* param)
{
	STE tempEnt;
	for(int i=param->paramslist->size()-1;i>=0; --i)
	{
		tempEnt = getCurTableSTE(param->paramslist->at(i));
		quads->emit(tempEnt.name, PUSHPARAM, NOP);
		quads->setST(currTable);
	}
}
int typeError;
void initTypeError()
{
	typeError = 0;
}
void incTError()
{
	typeError++;
}
int getTypeError()
{
	return typeError;
}
//Table 

vector<string> * OE;
void initOtherError()
{
	OE = new vector<string>(0);
}
void OtherError(const string& error)
{
	OE->push_back(error);
}
int otherErrorCount()
{
	return OE->size();
}
void printOtherError()
{
	for(int i =0;i<OE->size();++i)
	{
		cout<<OE->at(i)<<endl;
	}
	//TODO print vector
}

int getTypeSize(const string & type)
{
	if(type == "int" || type == "float") return 4;
	else if(type == "char"||type == "boolean") return 4;
	else if(type == "String"){ return 4; }
	else {

		//get type size from ST for objects of class TODO
	}
}


bool lookupClassInRoot(const string &name)
{
	return rootTable->lookup(name);
}
bool lookupRoot(const string &objname, const string &memName)
{
	STE classTable = rootTable->getSTE(objname);
	return classTable.tab->lookup(memName);
}

STE getRootSTE(const string& className, const string& varName)
{
        STE classTableE = rootTable->getSTE(className);
        ST * classTable = classTableE.tab;
        return classTable->getSTE(varName); 
}

QuadsList * getQuadsList(){
	return quads;
}
ST * getRootST(){
	return rootTable;
}

vector<int> returnList(0);
void initReturnList(){
	returnList.clear();
}

void pushReturnList(int lNo){
	returnList.push_back(lNo);
}

vector<int> getReturnList(){
	return returnList;
}
void emitPrint(Node * n)
{
	string type = currTable->getSTE(n->getPlace()).type;
	if(type == "boolean"){
		quads->emit(n->getPlace(), PRINTBOOL, NOP);
	}
	else if(type == "char"){
		quads->emit(n->getPlace(), PRINTCHAR, NOP);	
	}
	else if(type == "int"){
		quads->emit(n->getPlace(), PRINTINT, NOP);	
	}
	else if(type == "float"){
		quads->emit(n->getPlace(), PRINTFLOAT, NOP);	
	}
	else if(type == "String"){
		//TODO check string
		quads->emit(n->getPlace(), PRINTSTR, NOP);	
	}
	quads->setST(currTable);
}
void emitPrintln(){
	quads->emit("", PRINTLN, NOP);
	quads->setST(currTable);
}
bool mainOcc = false;
bool getMainOcc()
{
	return mainOcc;
}
void setMainOcc()
{
	mainOcc = true;
}

void emitArrayLHS(const string& name,const string& size,const string& RHS)
{
	quads->emit(name, ARRAYASSIGN, LHS, size, RHS);
	quads->setST(currTable);
}
void emitArrayRHS(const string& LHS,const string& name,const string& size)
{
	quads->emit(LHS, ARRAYASSIGN, RHS,name, size);
	quads->setST(currTable);
}
