#ifndef __QUADS__
#define __QUADS__

/**
Class for storing quads of 3AC
**/

#include <string>
#include <vector>
#include <sstream>
#include "ST.hpp"
using namespace std;

enum TACType{
	JUMP,
	JUMPLABEL,
	ARITH,
	IFCOMPGOTO,
	IFGOTO,
	ASSIGN,
	CONVERT,
	BOOLEXP,
	INITARRAY,
	METHODLBL,
	LOCAL_FUNCCALL,
	LOCAL_FUNCCALL_ASSIGN,
	RETURN,
	PUSHPARAM,
	PRINTINT,
	PRINTCHAR,
	PRINTFLOAT,
	PRINTBOOL,
	PRINTLN,
	PRINTSTR,
	ARRAYASSIGN
};

enum OPType{
	NOP,
	GOTO, 
	GOTOLBL,
	FLOATADD,
	INTADD,
	CHARADD,
	FLOATSUB,
	INTSUB,
	CHARSUB,
	FLOATMULT,
	INTMULT,
	FLOATDIV,
	INTDIV,
	INTMOD,
	SHIFTL,
	SHIFTR,
	BITAND,
	BITOR,
	BITXOR,
	LESSTHAN,
	GRTHAN,
	LESSEQ,
	GREQ,
	EQ,
	EQASSIGN,
	EQASSIGNCONST,
	EQASSIGNSTR,
	MINUSASSIGN,
	INTTOFLOAT,
	FLOATTOINT,
	INTTOCHAR,
	CHARTOINT,
	UNARYNOT,
	UNARYTILDE,
	BOOLAND,
	BOOLOR,
	LHS,
	RHS
};
#include<iostream>
class Quad3AC{
	private:
	public:
	int instNo;
	string op1;
	string op2;
	OPType opt;
	string result;
	TACType type; 
	ST * ownTable;	//pointer to own table, for offsets etc

	Quad3AC(int n, TACType t, OPType o, const string& s1, const string& s2, const string& res):instNo(n), type(t), opt(o), op1(s1), op2(s2), result(res)
	{}
	Quad3AC()
	{};
	void updateA1(const string & st)
	{
		op1 = st;
	}
	void updateA2(const string & st)
	{
		op2 = st;
	}
	void print();
};

class QuadsList{
	private:
	int nextInstr; //could use a static int for these
	int lastTemp;
	bool preReq;	//force emit before setST
	public:
	vector<Quad3AC> * qdl;
	QuadsList()
	{
		nextInstr = 0;
		lastTemp = 0;
		qdl = new vector<Quad3AC>(0);
		preReq = false;
	}
	int getNextInstr()
	{
		return nextInstr;
	}
	string NoToStr(int n) //Isnt there a beter way? :(
	{
		ostringstream ss;
     	ss << n;
     	return ss.str();
	}
	void emit(const string& op1, TACType t, OPType op,const string &op2="" ,const string& res="");
	void setST(ST * curTable);
	void backpatch(const vector<int>& list, int to);
	void backpatch(int stmt, int to);
	string getNewTemp();
	void printQuads();
	void updateMethodLine(int line, int val);
};

#endif
