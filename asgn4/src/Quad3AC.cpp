#include "Quad3AC.hpp"
#include <assert.h>

void QuadsList::emit(const string& op1, TACType t, OPType op,const string &op2,const string& res)
{
	assert(!preReq);
	Quad3AC q(nextInstr, t, op, op1, op2, res);
	qdl->push_back(q);
	nextInstr++;
	preReq = true;
}

void QuadsList::setST(ST * curTable)
{
	//IMP DO AFTER emit
	assert(preReq);
	qdl->at(nextInstr-1).ownTable = curTable;
	preReq = false;
}

#include<iostream>
void QuadsList::backpatch(const vector<int> & l, int to)
{
	string toStr = NoToStr(to);
	int size = l.size();
	for(int i = 0; i<size;++i)
	{
		qdl->at(l[i]).updateA1(toStr);
	}
}

void QuadsList::backpatch(int stmt, int to)
{
	string toStr = NoToStr(to);
	qdl->at(stmt).updateA1(toStr);
}

string QuadsList::getNewTemp()
{
	string prefix = "_t";	//cannot conflict with java vars?
	string var = prefix + NoToStr(lastTemp);
	lastTemp++;
	return var;
}

void QuadsList::printQuads()
{
	cout<<"No of quads:"<<qdl->size()<<endl;
	for(int i=0;i<qdl->size();++i)
	{
		qdl->at(i).print();
	}
}

void Quad3AC::print()	//TODO
{
	string temp;
	switch(type)
	{
		case JUMP:
			cout<<instNo<<":GOTO "<<op1<<endl;
			break;
		case JUMPLABEL:
			cout<<instNo<<":LABEL "<<op1<<endl;
			break;
		case ARITH:
			switch(opt)
			{
				case FLOATADD:
				case INTADD:
				case CHARADD:{
					temp = " + "; break;
				}	
				case FLOATSUB:	
				case INTSUB:
				case CHARSUB:{
					temp = " - "; break;
				}
				case FLOATMULT:
				case INTMULT:{
					temp = " * "; break;
				}
				case FLOATDIV:
				case INTDIV:{
					temp = " / "; break;
				}
				case INTMOD:{
					temp = " % "; break;
				}
				case SHIFTL:{
					temp = " << "; break;
				}		
				case SHIFTR:{
					temp = " >> "; break;
				}
				case BITAND:{
					temp = " & "; break;
				}
				case BITOR:{
					temp = " | "; break;
				}
				case BITXOR:{
					temp = " ^ "; break;
				}
				default:			temp = ""; break;
			}
			cout<<instNo<<":"<<result<<" = "<<op1<<temp<<op2<<endl;
			break;
		case IFCOMPGOTO:
			switch(opt)
			{
				case LESSTHAN:{
					temp = " < "; break;
				}
				case GRTHAN:{
					temp = " > "; break;
				}
				case LESSEQ:{
					temp = " <= "; break;
				}
				case GREQ:{
					temp = " >= "; break;
				}
				case EQ:{
					temp = " == "; break;
				}
				default:			temp = ""; break;
			}
			cout<<instNo<<":IF "<<op1<<temp<<op2<<" GOTO "<<result<<endl;
			break;
		case IFGOTO:    
			cout<<instNo<<":IF "<<op1<<" GOTO "<<op2<<endl;
			break;
		case ARRAYASSIGN:
		{
			switch(opt){
				case LHS:{
					cout<<instNo<<":"<<op1<<"["<<op2<<"]="<<result<<endl;
					break;
				}
				case RHS:{
					cout<<instNo<<":"<<op1<<"="<<op2<<"["<<result<<"]"<<endl;
					break;
				}
			}
			break;
		}
		case ASSIGN:
			switch(opt)
			{
				case EQASSIGN:{
					temp = " = "; break;
				}
				case EQASSIGNCONST:{
					temp = " = "; break;
				}
				case MINUSASSIGN:{
					temp = " = -"; break;
				}
				case UNARYNOT:{
					temp = " = !"; break;
				}
				case UNARYTILDE:{
					temp = " = ~"; break;
				}
				case FLOATTOINT:{
					temp = " = (floattoint)"; break;
				}
				case INTTOFLOAT:{
					temp = " = (inttofloat)"; break;
				}
				case CHARTOINT:{
					temp = " = (chartoint)"; break;
				}
				case INTTOCHAR:{
					temp = " = (inttochar)"; break;
				}
				default:			temp = ""; break;
			}
			cout<<instNo<<":"<<op1<<temp<<op2<<endl;
			break;
		//case CONVERT:
		case BOOLEXP:
			switch(opt)
			{
				case BOOLAND:{
					temp = " AND "; break;
				}
				case BOOLOR:{
					temp = " OR "; break;
				}
				default:			temp = ""; break;
			}
			cout<<instNo<<":"<<result<<" = "<<op1<<temp<<op2<<endl;
			break;
		case INITARRAY:{
			cout<<instNo<<":"<<op1<<" = "<<op2<<"["<<result<<"]"<<endl;
			break;
		}
		case METHODLBL:
			if(op1=="BeginFunc"){
				cout<<instNo<<":"<<op1<<" "<<op2<<endl;  
			}
			else if(op1=="EndFunc"){
				cout<<instNo<<":"<<op1<<endl;
			}
			break;
		case LOCAL_FUNCCALL:
			cout<<instNo<<":"<<"LCall _"<<op1<<endl;
			break;
		case LOCAL_FUNCCALL_ASSIGN:
			cout<<instNo<<":"<<op2<<" = LCall _"<<op1<<endl;
			break;
		case RETURN:
			cout<<instNo<<":RETURN "<<op1<<endl; //TODO when return without operand?
			break;
		case PUSHPARAM:
			cout<<instNo<<":PUSHPARAM "<<op1<<endl;
			break;
		case PRINTINT:{
			cout<<instNo<<":"<<"PRINTINT"<<op1<<endl;
			break;
		}
		case PRINTCHAR:{
			cout<<instNo<<":"<<"PRINTCHAR"<<op1<<endl;
			break;
		}
		case PRINTFLOAT:{
			cout<<instNo<<":"<<"PRINTFLOAT"<<op1<<endl;
			break;
		}
		case PRINTBOOL:{
			cout<<instNo<<":"<<"PRINTBOOL"<<op1<<endl;
			break;
		}
		case PRINTSTR:{
			cout<<instNo<<":"<<"PRINTSTR"<<op1<<endl;
			break;
		}
		case PRINTLN:
			cout<<instNo<<":PRINTLN"<<endl;
			break;
		default:
			break;
	}
}

void QuadsList::updateMethodLine(int line, int val)
{
	qdl->at(line).updateA2(NoToStr(val));
}
