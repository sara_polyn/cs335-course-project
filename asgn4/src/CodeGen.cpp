#include"CodeGen.hpp"
#include"Quad3AC.hpp"

//TODO use Floating point registers??
int GenMIPS::getAddr(const string& x)
{
	string regname; //concat("r", selectors); //TODO
	//string xAddr = "0";//<address of x>;	//TODO
	ST * curTable = quads->qdl->at(curInst).ownTable;
	ST * parent = curTable->parent;
	int baseOffset = curTable->baseOffset;
	while(!curTable->lookupLoc(x)){
		curTable = parent;
		parent = curTable->parent;
		baseOffset = curTable->baseOffset;
	}
	STE curSTE = curTable->getSTE(x);
	int size = curSTE.size;
	string type = curSTE.type;
	int xAddr = curSTE.offset + baseOffset;
	if(xAddr>=0){
		xAddr = xAddr +8;
	}
	return xAddr;
}

int GenMIPS::getSize(const string& x)
{
	string regname; //concat("r", selectors); //TODO
	//string xAddr = "0";//<address of x>;	//TODO
	ST * curTable = quads->qdl->at(curInst).ownTable;
	ST * parent = curTable->parent;
	int baseOffset = curTable->baseOffset;
	while(!curTable->lookupLoc(x)){
		curTable = parent;
		parent = curTable->parent;
		baseOffset = curTable->baseOffset;
	}
	STE curSTE = curTable->getSTE(x);
	int size = curSTE.size;
	return size;
}

bool GenMIPS::isFloat(const string& x)
{
	string regname; //concat("r", selectors); //TODO
	//string xAddr = "0";//<address of x>;	//TODO
	ST * curTable = quads->qdl->at(curInst).ownTable;
	ST * parent = curTable->parent;
	int baseOffset = curTable->baseOffset;
	while(!curTable->lookupLoc(x)){
		curTable = parent;
		parent = curTable->parent;
		baseOffset = curTable->baseOffset;
	}
	STE curSTE = curTable->getSTE(x);
	string type = curSTE.type;
	if(type=="float"){
		return true;
	}
	else return false;
}

string GenMIPS::getReg(const string& x, int selectors)
{
	string regname; //concat("r", selectors); //TODO
	//string xAddr = "0";//<address of x>;	//TODO
	ST * curTable = quads->qdl->at(curInst).ownTable;
	ST * parent = curTable->parent;
	int baseOffset = curTable->baseOffset;
	while(!curTable->lookupLoc(x)){
		curTable = parent;
		parent = curTable->parent;
		baseOffset = curTable->baseOffset;
	}
	STE curSTE = curTable->getSTE(x);
	int size = curSTE.size;
	string type = curSTE.type;
	int xAddr = curSTE.offset + baseOffset;
	if(xAddr>=0){
		xAddr = xAddr +8;
	}
	string inst;
	if(size == 1)
	{
		inst="lb";
	}
	else if(size == 4)
	{
		inst="lw";
		regname = "$t"+quads->NoToStr(selectors);
		if(type == "float"){
			inst="lwc1";
			regname = "$f"+quads->NoToStr(selectors);
		}
	}
	genCode+= inst+" "+regname+", "+quads->NoToStr(-xAddr)+"($fp)"+"\t#Loading\n";  
	return regname;
}

void GenMIPS::storeReg(const string& x, string& reg)
{
	//string xAddr = "0";//<address of x>;	//TODO
	ST * curTable = quads->qdl->at(curInst).ownTable;
	ST * parent = curTable->parent;
	int baseOffset = curTable->baseOffset;
	while(!curTable->lookupLoc(x)){
		curTable = parent;
		parent = curTable->parent;
		baseOffset = curTable->baseOffset;
	}
	STE curSTE = curTable->getSTE(x);
	int size = curSTE.size;
	string type = curSTE.type;
	int xAddr = curSTE.offset + baseOffset;
	if(xAddr>=0){
		xAddr = xAddr +8;
	}
	string inst;
	if(size == 1)
	{
		inst="sb";
	}
	else if(size == 4)
	{
		inst="sw";
		if(type == "float")
			inst="swc1";
	}
	else{
		inst=quads->NoToStr(size);
	}
	genCode+= inst+" "+reg+", "+quads->NoToStr(-xAddr)+"($fp)"+"\t#Storing\n";  
}

void GenMIPS::add(const string& oprnd1,const string& oprnd2,const string& oprnd3)
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "add";
	genCode+=inst+" "+"$t1"+", "+reg2+", "+reg3+"\t#add instr\n"; 
	string s = "$t1";
	storeReg(oprnd1,s);	
}

void GenMIPS::addf(const string& oprnd1,const string& oprnd2,const string& oprnd3)
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "add.s";
	genCode+=inst+" "+"$f1"+", "+reg2+", "+reg3+"\t#add instr\n"; 
	string s = "$f1";
	storeReg(oprnd1,s);	
}

void GenMIPS::sub(const string& oprnd1,const string& oprnd2,const string& oprnd3)
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "sub";
	genCode+=inst+" "+"$t1"+", "+reg2+", "+reg3+"\t#sub instr\n"; 	
	string s = "$t1";
	storeReg(oprnd1,s);	
}

void GenMIPS::subf(const string& oprnd1,const string& oprnd2,const string& oprnd3)
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "sub.s";
	genCode+=inst+" "+"$f1"+", "+reg2+", "+reg3+"\t#sub instr\n"; 	
	string s = "$f1";
	storeReg(oprnd1,s);	
}

void GenMIPS::mult(const string& oprnd1,const string& oprnd2,const string& oprnd3)
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "mult";
	genCode+=inst+" "+reg2+", "+reg3+"\t#mul instr\n";
	inst = "mflo"; 	
	genCode+=inst+" "+"$t1"+"\t#LO->reg instr\n";
	string s = "$t1";
	storeReg(oprnd1,s);	
}

void GenMIPS::multf(const string& oprnd1,const string& oprnd2,const string& oprnd3)
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "mul.s";
	genCode+=inst+" "+"$f1"+", "+reg2+", "+reg3+"\t#mul instr\n";
	string s = "$f1";
	storeReg(oprnd1,s);	
}

void GenMIPS::div(const string& oprnd1,const string& oprnd2,const string& oprnd3)
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "div";
	genCode+=inst+" "+reg2+", "+reg3+"\t#div instr\n";
	inst = "mflo"; 	
	genCode+=inst+" "+"$t1"+"\t#LO->reg instr\n";
	string s = "$t1";
	storeReg(oprnd1,s);	
}

void GenMIPS::divf(const string& oprnd1,const string& oprnd2,const string& oprnd3)
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "div.s";
	genCode+=inst+" "+"$f1"+", "+reg2+", "+reg3+"\t#div instr\n";
	string s = "$f1";
	storeReg(oprnd1,s);	
}

void GenMIPS::assignConstant(const string& oprnd1,const string& con)
{
	string inst = "li";
	genCode+=inst+" "+"$t1"+", "+con+"\t#constant to reg\n";	
	string s = "$t1";
	storeReg(oprnd1,s);	
}

void GenMIPS::assignStr(const string& oprnd1,const string& con)
{
	genCode+=".data \t# create string constant marked with label\n";
	genCode+= oprnd1 + ": .asciiz  "+con+"\n";
	genCode+=".text\n";	
}

void GenMIPS::le(const string& oprnd1,const string& oprnd2,const string& oprnd3)
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "slt";
	genCode+=inst+" "+"$t1"+", "+reg2+", "+reg3+"\t#Less than instr\n"; 
	string s = "$t1";
	storeReg(oprnd1,s);	
}

void GenMIPS::gr(const string& oprnd1,const string& oprnd2,const string& oprnd3)
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	// string inst = "sub";
	// genCode+=inst+" "+reg2+", "+"$0"+", "+reg2+"\t#negative reg\n"; 
	// string inst = "sub";
	// genCode+=inst+" "+reg3+", "+"$0"+", "+reg3+"\t#negative reg\n"; 
	// string inst = "slt";
	// genCode+=inst+" "+"$t1"+", "+reg2+", "+reg3+"\t#Less than instr\n"; 
	string inst = "sgt";
	genCode+=inst+" "+"$t1"+", "+reg2+", "+reg3+"\t#Grtr than instr\n"; 
	string s = "$t1";
	storeReg(oprnd1,s);	
}

void GenMIPS::leEq(const string& oprnd1,const string& oprnd2,const string& oprnd3)
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "sle";
	genCode+=inst+" "+"$t1"+", "+reg2+", "+reg3+"\t#Less than eq instr\n"; 
	string s = "$t1";
	storeReg(oprnd1,s);	
}

void GenMIPS::grEq(const string& oprnd1,const string& oprnd2,const string& oprnd3)
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "sge";
	genCode+=inst+" "+"$t1"+", "+reg2+", "+reg3+"\t#grtr than eq instr\n"; 
	string s = "$t1";
	storeReg(oprnd1,s);	
}

void GenMIPS::eqEq(const string& oprnd1,const string& oprnd2,const string& oprnd3)
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "seq";
	genCode+=inst+" "+"$t1"+", "+reg2+", "+reg3+"\t#eq eq instr\n"; 
	string s = "$t1";
	storeReg(oprnd1,s);	
}

void GenMIPS::nEq(const string& oprnd1,const string& oprnd2,const string& oprnd3)
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "sne";
	genCode+=inst+" "+"$t1"+", "+reg2+", "+reg3+"\t#not eq instr\n"; 
	string s = "$t1";
	storeReg(oprnd1,s);	
}

void GenMIPS::perc(const string& oprnd1,const string& oprnd2,const string& oprnd3)
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "rem";
	genCode+=inst+" "+"$t1"+", "+reg2+", "+reg3+"\t#mod instr\n"; 
	string s = "$t1";
	storeReg(oprnd1,s);	
}

void GenMIPS::leLe(const string& oprnd1,const string& oprnd2,const string& oprnd3)
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "sll";
	genCode+=inst+" "+"$t1"+", "+reg2+", "+reg3+"\t#left shift instr\n"; 
	string s = "$t1";
	storeReg(oprnd1,s);	
}

void GenMIPS::grGr(const string& oprnd1,const string& oprnd2,const string& oprnd3)
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "sra";
	genCode+=inst+" "+"$t1"+", "+reg2+", "+reg3+"\t#right arith shift\n"; 
	string s = "$t1";
	storeReg(oprnd1,s);	
}

void GenMIPS::grGrGr(const string& oprnd1,const string& oprnd2,const string& oprnd3)
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "srl";
	genCode+=inst+" "+"$t1"+", "+reg2+", "+reg3+"\t#right shift instr\n"; 
	string s = "$t1";
	storeReg(oprnd1,s);	
}

void GenMIPS::OpAnd(const string& oprnd1,const string& oprnd2,const string& oprnd3)  //Bitwise and
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "and";
	genCode+=inst+" "+"$t1"+", "+reg2+", "+reg3+"\t#Bitwise and\n"; 
	string s = "$t1";
	storeReg(oprnd1,s);	
}

void GenMIPS::OpOr(const string& oprnd1,const string& oprnd2,const string& oprnd3)  //Bitwise or
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "or";
	genCode+=inst+" "+"$t1"+", "+reg2+", "+reg3+"\t#Bitwise or\n"; 
	string s = "$t1";
	storeReg(oprnd1,s);	
}

void GenMIPS::OpXor(const string& oprnd1,const string& oprnd2,const string& oprnd3)  //Bitwise xor
{
	string reg2 = getReg(oprnd2,2);
	string reg3 = getReg(oprnd3,3);
	string inst = "xor";
	genCode+=inst+" "+"$t1"+", "+reg2+", "+reg3+"\t#Bitwise xor\n"; 
	string s = "$t1";
	storeReg(oprnd1,s);	
}

void GenMIPS::assignExpr(const string& oprnd1,const string& oprnd2)
{
	string reg2 = getReg(oprnd2,2);
	storeReg(oprnd1,reg2);
}

void GenMIPS::OpNot(const string& oprnd1,const string& oprnd2) 
{
	string reg2 = getReg(oprnd2,2);
	string inst = "not";
	genCode+=inst+" "+"$t1"+", "+reg2+"\t#Bitwise not\n"; 
	string s = "$t1";
	storeReg(oprnd1,s);	
}

void GenMIPS::neg(const string& oprnd1,const string& oprnd2)
{
	string reg2 = getReg(oprnd2,2);
	string inst = "neg";
	genCode+=inst+" "+"$t1"+", "+reg2+"\t#neg instr\n"; 
	string s = "$t1";
	storeReg(oprnd1,s);	
}

// TODO CastExpression i.e. emitConv, emitLabel, emitMLabel, emitLCall, emitLCallAssign, emitReturn, emitPushParam
void GenMIPS::ifEqEq(const string& oprnd1,const string& oprnd2,const string& label)  
{
	string reg1 = getReg(oprnd1,1);
	string reg2 = getReg(oprnd2,2);
	string inst = "beq";
	genCode+=inst+" "+reg1+", "+reg2+", "+"_"+label+"\t#branch on eq\n"; 
}

void GenMIPS::ifLe(const string& oprnd1,const string& oprnd2,const string& label)  
{
	string reg1 = getReg(oprnd1,1);
	string reg2 = getReg(oprnd2,2);
	string inst = "blt";
	genCode+=inst+" "+reg1+", "+reg2+", "+"_"+label+"\t#branch on le\n"; 
}

void GenMIPS::ifLeEq(const string& oprnd1,const string& oprnd2,const string& label)  
{
	string reg1 = getReg(oprnd1,1);
	string reg2 = getReg(oprnd2,2);
	string inst = "ble";
	genCode+=inst+" "+reg1+", "+reg2+", "+"_"+label+"\t#branch on le eq\n"; 
}

void GenMIPS::ifGr(const string& oprnd1,const string& oprnd2,const string& label)  
{
	string reg1 = getReg(oprnd1,1);
	string reg2 = getReg(oprnd2,2);
	string inst = "bgt";
	genCode+=inst+" "+reg1+", "+reg2+", "+"_"+label+"\t#branch on gr\n"; 
}

void GenMIPS::ifGrEq(const string& oprnd1,const string& oprnd2,const string& label)  
{
	string reg1 = getReg(oprnd1,1);
	string reg2 = getReg(oprnd2,2);
	string inst = "bge";
	genCode+=inst+" "+reg1+", "+reg2+", "+"_"+label+"\t#branch on gr eq\n"; 
}

void GenMIPS::ifNEq(const string& oprnd1,const string& oprnd2,const string& label)  
{
	string reg1 = getReg(oprnd1,1);
	string reg2 = getReg(oprnd2,2);
	string inst = "bne";
	genCode+=inst+" "+reg1+", "+reg2+", "+"_"+label+"\t#branch on not eq\n"; 
}

void GenMIPS::ifEqEqTrue(const string& oprnd1,const string& label)  
{
	string reg1 = getReg(oprnd1,1);
	string inst = "bne";
	genCode+=inst+" "+reg1+", "+"$0"+", "+"_"+label+"\t#branch on true\n"; 
}

void GenMIPS::OpGoto(const string& label)  
{
	string inst = "b";
	genCode+=inst+" "+"_"+label+"\t#branch on not eq\n"; 
}

void GenMIPS::label(const string& label) 
{
	genCode+="_"+label+":"+"\t#label\n"; 
}

void GenMIPS::beginFunction(const string& funcSize)
{
	genCode+="subu $sp, $sp, 8\t# decrement sp to make space to save ra, fp\n";
	genCode+="sw $fp, 8($sp)\t# save fp\n";
	genCode+="sw $ra, 4($sp)\t# save ra\n";
	genCode+="addiu $fp, $sp, 8\t# set up new fp\n";
	// ST * curTable = quads->qdl->at(curInst).ownTable;
	// STE curSTE = curTable->getSTE(funcName);
	// int frameSz= curSTE.tab->width;
	string frameSz= funcSize;
	if (frameSz != "")
 		genCode+="subu $sp, $sp, "+frameSz+"\t# decrement sp for locals/temps\n";
} 

void GenMIPS::returnFunction(const string& value)
{
	if(value != ""){
		ST * curTable = quads->qdl->at(curInst).ownTable;
		STE curSTE = curTable->getSTE(value);
		string type = curSTE.type;
		if(type!="float"){
			string reg1 = getReg(value,1);
			string inst = "move";
			genCode+=inst+" "+"$v0"+", "+reg1+"\t#assign return value into $v0\n"; 
		}
		else if(type=="float"){
			string reg1 = getReg(value,0); //Storing in $f0
		}
	}
}

void GenMIPS::endFunction()
{
	genCode+="move $sp, $fp\t# pop callee frame off stack\n";
	genCode+="lw $ra, -4($fp)\t# restore saved ra\n";
	genCode+="lw $fp, 0($fp)\t# restore saved fp\n";
	genCode+="jr $ra\t# return from function\n";
} 

void GenMIPS::pushParam(string& param)
{
	string reg1 = getReg(param,1);
	ST * curTable = quads->qdl->at(curInst).ownTable;
	STE curSTE = curTable->getSTE(param);
	int size = curSTE.size;
	string inst2;
	if(size==1){
		inst2 = "sb";
	}
	else if(size==4){
		inst2 = "sw";
	}
	string inst = "subu";
	genCode+=inst+" "+"$sp"+", "+"$sp"+", "+quads->NoToStr(size)+"\t#decrement sp to make space for param\n"; 
	genCode+=inst2+" "+reg1+", "+quads->NoToStr(size)+"($sp)"+"\t#decrement sp to make space for param\n"; 
} 

void GenMIPS::assignFuncCall(string& to,string& func)
{
	string inst = "jal";
	genCode+=inst+" _"+className+"."+func+"\t#jump to function\n";
	string reg1;

	ST * curTable = quads->qdl->at(curInst).ownTable;
	STE curSTE = curTable->getSTE(func);
	string type = curSTE.type;
	if(type!="float"){
		reg1 = getReg(to,1);
		inst = "move";
		genCode+=inst+" "+reg1+", "+"$v0"+"\t#copy function return value from $v0\n";
		storeReg(to,reg1);	
	}
	else if(type=="float"){
		reg1="$f0";
		storeReg(to,reg1);
	}
	int size = 0;
	STE t = quads->qdl->at(curInst).ownTable->getSTE(func);
	for(int i=0; i<t.paramslist->size(); ++i)
	{
		size = size+getTypeSize(t.paramslist->at(i));
	}
	inst = "add";
	genCode+=inst+" "+"$sp"+", "+"$sp"+", "+quads->NoToStr(size)+"\t#pop params off stack \n"; 
}

void GenMIPS::funcCall(string& func)
{
	string inst = "jal";
	genCode+=inst+" _"+className+"."+func+"\t#jump to function\n";
	int size = 0;
	STE t = quads->qdl->at(curInst).ownTable->getSTE(func);
	for(int i=0; i<t.paramslist->size(); ++i)
	{
		size = size+getTypeSize(t.paramslist->at(i));
	}
	inst = "add";
	genCode+=inst+" "+"$sp"+", "+"$sp"+", "+quads->NoToStr(size)+"\t#pop params off stack \n"; 
}	

int GenMIPS::getTypeSize(const string & type)
{
	if(type == "int" || type == "float") return 4;
	else if(type == "char"||type == "boolean") return 1;
	else if(type == "String"){return 4;}
	else {
		//get type size from ST for objects of class TODO
	}
}

void GenMIPS::floatToInt(const string & to, const string & from)
{
	string reg2 = getReg(from,2);
	string inst = "cvt.w.s";
	genCode+=inst+" "+"$f1"+", "+reg2+"\t#single to int\n"; 
	inst = "mfc1";
	genCode+=inst+" "+"$t1"+", "+"$f1"+"\t#floatReg to intReg\n";
 	string s = "$t1";
	storeReg(to,s);	
}

void GenMIPS::intToFloat(const string &to, const string & from)
{
	string reg2 = getReg(from,2);
	string inst = "mtc1";
	genCode+=inst+" "+reg2+", "+"$f1"+"\t#intReg to floatReg\n"; 
	inst = "cvt.s.w";
	genCode+=inst+" "+"$f1"+", "+"$f1"+"\t#Integer to Single\n";
 	string s = "$f1";
	storeReg(to,s);	
}

void GenMIPS::printStr(const string& name){
	string inst = "li";
	genCode+=inst+" "+"$v0"+", "+"4\t#Code for printstr\n";
	inst="la";	
	genCode+=inst+" "+"$a0"+" ,"+name+"\t# load address of string to be printed into $a0\n";
	genCode+="syscall\n";
}

void GenMIPS::printBool(const string& name){
	string reg2 = getReg(name,2);
	string inst = "li";
	genCode+=inst+" "+"$v0"+", "+"1\t#Code for printint\n";
	inst ="move";
	genCode+=inst+" "+"$a0"+" ,"+reg2+"\t## move integer to be printed into $a0\n";
	genCode+="syscall\n";
}

void GenMIPS::printChar(const string& name){
	string reg2 = getReg(name,2);
	string inst = "li";
	genCode+=inst+" "+"$v0"+", "+"11\t#Code for printint\n";
	inst ="move";
	genCode+=inst+" "+"$a0"+" ,"+reg2+"\t## move integer to be printed into $a0\n";
	genCode+="syscall\n";
}

void GenMIPS::printInt(const string& name){
	string reg2 = getReg(name,2);
	string inst = "li";
	genCode+=inst+" "+"$v0"+", "+"1\t#Code for printint\n";
	inst ="move";
	genCode+=inst+" "+"$a0"+" ,"+reg2+"\t## move integer to be printed into $a0\n";
	genCode+="syscall\n";
}

void GenMIPS::printFloat(const string& name){
	string reg2 = getReg(name,2);
	string inst = "li";
	genCode+=inst+" "+"$v0"+", "+"2\t#Code for printfloat\n";
	inst="mov.s"; //TODO check
	genCode+=inst+" "+"$f12"+" ,"+reg2+"\t## move float to be printed into $f12\n";
	genCode+="syscall\n";
}

void GenMIPS::printLn(){
	string inst = "li";
	genCode+=inst+" "+"$v0"+", "+"11\t#Code for printint\n";
	inst ="li";
	genCode+=inst+" "+"$a0"+" ,"+"'\\n'"+"\t## move integer to be printed into $a0\n";
	genCode+="syscall\n";
}

void GenMIPS::arrayAssignLHS(const string& name,const string& size,const string& RHS){
	string reg3 = getReg(RHS,3);
	int loc = getAddr(name);
	string reg4 = getReg(size,4);
	genCode+="neg "+reg4+", "+reg4+"\n";
	string inst = "li";
	genCode+=inst+" "+"$t1"+", "+quads->NoToStr(-loc)+"\t#offset of array\n";
	inst ="add";
	genCode+=inst+" "+"$t2"+", "+"$t1"+", "+"$fp"+"\t#addr of array\n";
	int sizeE = getSize(RHS);
	bool t = isFloat(RHS);
	if(sizeE==1){
		inst = "sb";
	}
	else if(sizeE==4){
		inst = "sw";
		if(t){
			inst="swc1";
		}
	}
	genCode+="add "+reg4+", "+reg4+", "+"$t2"+"\n";
	genCode+=inst+" "+reg3+", "+"0("+reg4+")\t#storing into array\n";
}

void GenMIPS::arrayAssignRHS(const string& LHS,const string& name,const string& size){
	int loc = getAddr(name);
	string reg4 = getReg(size,4);
	genCode+="neg "+reg4+", "+reg4+"\n";
	string inst = "li";
	genCode+=inst+" "+"$t1"+", "+quads->NoToStr(-loc)+"\t#offset of array\n";
	inst ="add";
	genCode+=inst+" "+"$t2"+", "+"$t1"+", "+"$fp"+"\t#addr of array\n";
	int sizeE = getSize(LHS);
	bool t = isFloat(LHS);
	if(sizeE==1){
		inst = "lb";
	}
	else if(sizeE==4){
		inst = "lw";
		if(t){
			inst="lwc1";
		}
	}
	genCode+="add "+reg4+", "+reg4+", "+"$t2"+"\n";
	genCode+=inst+" "+"$t3"+", "+"0("+reg4+")\t#loading from array\n";
	string s = "$t3";
	storeReg(LHS,s);
}

void GenMIPS::emitCode()
{
	for(curInst=0;curInst<quads->qdl->size();++curInst)
	{
		Quad3AC q = quads->qdl->at(curInst);
		label(quads->NoToStr(q.instNo));
		switch(q.type)
		{
			case JUMP:
				OpGoto(q.op1);
				break;
			case JUMPLABEL:			
				label(q.op1);
				break;
			case ARITH:
				switch(q.opt)
				{
					case FLOATADD:{
						addf(q.result,q.op1,q.op2);
						break;
					}
					case INTADD:
					case CHARADD:{
						add(q.result,q.op1,q.op2);
						break;
					}	
					case FLOATSUB:{
						subf(q.result,q.op1,q.op2);
						break;
					}	
					case INTSUB:
					case CHARSUB:{
						sub(q.result,q.op1,q.op2);
						break;
					}
					case FLOATMULT:{
						multf(q.result,q.op1,q.op2);
						break;
					}
					case INTMULT:{
						mult(q.result,q.op1,q.op2);
						break;
					}
					case FLOATDIV:{
						divf(q.result,q.op1,q.op2);
						break;
					}
					case INTDIV:{
						div(q.result,q.op1,q.op2);
						break;
					}
					case INTMOD:{
						perc(q.result,q.op1,q.op2);
						break;
					}
					case SHIFTL:{
						leLe(q.result,q.op1,q.op2);
						break;
					}		
					case SHIFTR:{
						grGr(q.result,q.op1,q.op2);
						break;
					}
					case BITAND:{
						OpAnd(q.result,q.op1,q.op2);
						break;
					}
					case BITOR:{
						OpOr(q.result,q.op1,q.op2);
						break;
					}
					case BITXOR:{
						OpXor(q.result,q.op1,q.op2);
						break;
					}
					default: break;
				}
				break;
			case IFCOMPGOTO:
				switch(q.opt)
				{
					case LESSTHAN:{
						ifLe(q.op1,q.op2,q.result);
						break;
					}
					case GRTHAN:{
						ifGr(q.op1,q.op2,q.result);
						break;
					}
					case LESSEQ:{
						ifLeEq(q.op1,q.op2,q.result);
						break;
					}
					case GREQ:{
						ifGrEq(q.op1,q.op2,q.result);
						break;
					}
					case EQ:{
						ifEqEq(q.op1,q.op2,q.result);
						break;
					}
					default: break;
				}
				break;
			case IFGOTO:{
				ifEqEqTrue(q.op1,q.op2);
				break;
			}
			case ARRAYASSIGN:
			{
				switch(q.opt){
					case LHS:{
						arrayAssignLHS(q.op1,q.op2,q.result);
						break;
					}
					case RHS:{
						arrayAssignRHS(q.op1,q.op2,q.result);
						break;
					}
				}
				break;
			}
			case ASSIGN:
				switch(q.opt)
				{
					case EQASSIGN:{
						assignExpr(q.op1,q.op2);
						break;
					}
					case EQASSIGNCONST:{
						assignConstant(q.op1,q.op2);
						break;
					}
					case EQASSIGNSTR:{
						assignStr(q.op1,q.op2);
						break;
					}
					case MINUSASSIGN:{
						neg(q.op1,q.op2);
						break;
					}
					case UNARYNOT:{
						OpNot(q.op1,q.op2);
						break;
					}
					case UNARYTILDE:{
						OpNot(q.op1,q.op2);
						break;
					}
					case FLOATTOINT:{
						floatToInt(q.op1,q.op2);
						break;
					}
					case INTTOFLOAT:{
						intToFloat(q.op1,q.op2);
						break;
					}
					case CHARTOINT:{
						//TODO
						break;
					}
					case INTTOCHAR:{
						//TODO
						break;
					}
					default: break;
				}
				break;
			//case CONVERT:
			case BOOLEXP:
				switch(q.opt)
				{
					case BOOLAND:{
						OpAnd(q.result,q.op1,q.op2);
						break;
					}
					case BOOLOR:{
						OpOr(q.result,q.op1,q.op2);
						break;
					}
					default: break;
				}
				break;
			case PRINTSTR:{
				printStr(q.op1);
				break;
			}
			case PRINTBOOL:{
				printBool(q.op1);
				break;
			}
			case PRINTCHAR:{
				printChar(q.op1);
				break;
			}
			case PRINTINT:{
				printInt(q.op1);
				break;
			}
			case PRINTFLOAT:{
				printFloat(q.op1);
				break;
			}
			case PRINTLN:{
				printLn();
				break;
			}
			case INITARRAY:{
				//nothingTODO
				break;
			}
			case METHODLBL:
				
				if(q.op1=="BeginFunc"){
					beginFunction(q.op2);  
				}
				else if(q.op1=="EndFunc"){
					endFunction();
				}
				break;
			case LOCAL_FUNCCALL:
				funcCall(q.op1);
				break;
			case LOCAL_FUNCCALL_ASSIGN:
				assignFuncCall(q.op2,q.op1);
				break;
			case RETURN:
				returnFunction(q.op1);
				break;
			case PUSHPARAM:
				pushParam(q.op1);
				break;
			//case PRINT:
				//TODO
			default:
				break;
		}
	}
}
