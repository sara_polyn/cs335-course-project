class test_recursion{
	int factorial(int x)
	{
		if(x==1){
			return 1;
		}
		int y;
		y = factorial(x-1);
		y = x*y;
		return y;
	}
	void main(){
		int a = 5;
		int ans;
		ans = factorial(a);
		System.out.println(ans);
	}
}