class test_ackermann{
	int ackerman(int m, int n){
		if(m==0){
			return n+1;
		} else if(m>0 && n==0){
			return ackerman(m-1, 1);
		} else{
			return ackerman(m-1, ackerman(m, n-1));
		}
		return 0;
	}
	void main(){
		int n = 3;
		int m = 3;
		int ans = ackerman(m,n);
		System.out.println(ans);
	}
}