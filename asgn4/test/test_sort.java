class test_sort{
    void main() {
        int n, c, d, temp;
        n = 10;
        int array[] = new int[10];
        array[0] = 96;
        array[1] = 93;
        array[2] = 67;
        array[3] = 2;
        array[4] = 78;
        array[5] = 40;
        array[6] = 34;
        array[7] = 21;
        array[8] = 74;
        array[9] = 13;
        System.out.print("Number of integers : ");
        System.out.println(n);
        System.out.println("Initial order of numbers : ");
        for (c = 0; c < n; c = c+1){
            System.out.println(array[c]);
        }
        for (c = 0; c < n - 1; c = c+1) {
            for (d = 0; d < n - c - 1; d = d+1) {
                if (array[d] > array[d+1])
                {
                    temp       = array[d];
                    array[d]   = array[d+1];
                    array[d+1] = temp;
                }
            }
        }
        System.out.println("Sorted list of numbers : ");
        for (c = 0; c < n; c = c+1){
            System.out.println(array[c]);
        }
    }
}