class test_array{
	void main(){
		int found = -1;
		int x = 5;
		int i;
		int array[] = new int[5];
		array[0] = 2;
        array[1] = 78;
        array[2] = 5;
        array[3] = 34;
        array[4] = 7;
        for(i=0;i<5;i=i+1){
        	if(array[i] == x){
        		found = i;
        	}
        }
        if(found == -1){
        	System.out.println("Number not found in the array");
        } else {
        	System.out.print("Number found at index ");
        	System.out.println(found);	
        }
	}
}