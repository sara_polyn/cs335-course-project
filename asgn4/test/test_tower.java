class TowersOfHanoi {
  void solve(int n, int start, int auxiliary, int end) {
    int k = n-1;
    if (n == 1) {
      System.out.print(start);
      System.out.print(" -> ");
      System.out.println(end);
    } else {
      solve(k, start, end, auxiliary);
      System.out.print(start);
      System.out.print(" -> ");
      System.out.println(end);
      solve(k, auxiliary, start, end);
    }
  }
  void main(){
    int discs = 6;
    System.out.print("Number of discs = ");
    System.out.println(discs);
    solve(discs, 1, 2, 3);
  }
} 