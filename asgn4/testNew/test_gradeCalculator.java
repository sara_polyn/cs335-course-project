class gradeCalculator{
	char findGrade(int score){
		if(score>90)
			return 'A';
		else if(score>80)
			return 'B';
		else if(score>70)
			return 'C';
		else if(score>60)
			return 'D';
		else if(score>50)
			return 'E';
		return 'F';
	}
	void main()
	{ 
	   int marks = 99;
	   char grade = findGrade(marks);

	   switch(grade)
	   {
	   case 'A' :
	      System.out.println("Excellent!");
	      break;
	   case 'B' :
	   	  System.out.println("Great!");
	   	  break;
	   case 'C' :
	      System.out.println("Well Done!");
	      break;
	   case 'D' :
	      System.out.println("Congrats! You Passed.");
	      break;
	   case 'E' :
	   case 'F'	:
	      System.out.println("Congrats! You got another chance to try.");
	      break;
	   default :
	      System.out.println("Your grade is out of the world.");
	   }
	}
}