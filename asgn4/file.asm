#MIPS code
.globl main
.text


main:
jal _main_func	#call main
li $v0, 10	#halt
syscall

_0:	#label
_testfloat.main:	#label
_1:	#label
_main_func:	#label
_2:	#label
subu $sp, $sp, 8	# decrement sp to make space to save ra, fp
sw $fp, 8($sp)	# save fp
sw $ra, 4($sp)	# save ra
addiu $fp, $sp, 8	# set up new fp
subu $sp, $sp, 28	# decrement sp for locals/temps
_3:	#label
li $t1, 3.5	#constant to reg
swc1 $t1, -8($fp)	#Storing
_4:	#label
lwc1 $f2, -8($fp)	#Loading
swc1 $f2, -12($fp)	#Storing
_5:	#label
li $t1, 4	#constant to reg
sw $t1, -16($fp)	#Storing
_6:	#label
lw $t2, -16($fp)	#Loading
sw $t2, -20($fp)	#Storing
_7:	#label
_8:	#label
lw $t2, -20($fp)	#Loading
lw $t3, -24($fp)	#Loading
mult $t2, $t3	#mul instr
mflo $t1	#LO->reg instr
sw $t1, -28($fp)	#Storing
_9:	#label
lw $t2, -28($fp)	#Loading
sw $t2, -32($fp)	#Storing
_10:	#label
lw $t2, -32($fp)	#Loading
li $v0, 1	#Code for printint
move $a0 ,$t2	## move integer to be printed into $a0
syscall
_11:	#label
move $sp, $fp	# pop callee frame off stack
lw $ra, -4($fp)	# restore saved ra
lw $fp, 0($fp)	# restore saved fp
jr $ra	# return from function

.data:
testfloat: .word 0	#static allocate
