#include <vector>
#include <iostream>
#include <string>
#include <cstring>
#include <cstdio>
#include <iomanip>

#include "lexer.h"

using namespace std;

extern int yylex();
extern int yylineno;
extern char* yytext;

const string token_names[] = {
 "ERROR",
 "LINE_TERMINATOR",
 "WHITESPACE",
 "COMMENT_MULTILINE",
 "COMMENT_SINGLELINE",
 "LITERAL_STRING",
 "LITERAL_CHARACTER",
 "IDENTIFIER",
 "KEYWORD",
 "LITERAL_INTEGER",
 "LITERAL_FLOAT",
 "LITERAL_BOOLEAN",
 "LITERAL_NULL",
 "OPERATOR",
 "SEPARATOR",
 "ERROR",
 "ERROR_LITERAL_STRING_EOF",
 "ERROR_LITERAL_STRING_ESCAPE_CHARACTER",
 "ERROR_LITERAL_STRING_LINE_TERMINATOR",
 "ERROR_COMMENT_MULTILINE_EOF",
 "ERROR_LITERAL_CHARACTER",
 "ERROR_LITERAL_INTEGER_HEX",
 "ERROR_LITERAL_CHARACTER_SINGLEQUOTE"
};

class tokenDescription{
public:
	int nToken;
	string tToken;
	tokenDescription(int n, string t="")
	{
		nToken = n;
		tToken = t;
	}
};
int main(int argc, char *argv[])
{
	bool error_p = false;
	if(argc==2)
	{
		if(freopen(argv[1], "r", stdin)==NULL)
		{
			cout<<"File not found"<<endl;
			return 0;
		}
	}
	else if(argc==3 && strcmp(argv[1], "-v") == 0)
	{
		if(freopen(argv[2], "r", stdin)==NULL)
		{
			cout<<"File not found"<<endl;
			return 0;
		}
		error_p = true;
	}
	else
	{
		cout<<"Incorrect input format.\n";
		cout<<"Please use the format specified in the readme"<<endl;
		return 0;
	}
	
	const string underscore = "_";
	int ntoken, vtoken;
	
	vector<string> tokens;
	vector<tokenDescription> token_desc;
	vector<string>::iterator it;
	vector<tokenDescription>::iterator iit;
	
	vector<string> errors;
	vector<int> errorLineNos;

	ntoken = yylex();
	string codeline;
	const int codewidth = 50;

	while(ntoken)
	{
		if(error_p)
		{
			if(ntoken >= ERROR)
			{
				errorLineNos.push_back(yylineno);
				switch(ntoken)
				{
					case ERROR_LITERAL_STRING_LINE_TERMINATOR:
						errors.push_back("unterminated string");
						break;
					case ERROR_LITERAL_STRING_EOF:
						errors.push_back("EOF befor string terminated");
						break;
					case ERROR_LITERAL_STRING_ESCAPE_CHARACTER:
						errors.push_back("unexpected \\ in line");
						break;
					case ERROR_LITERAL_CHARACTER:
						errors.push_back("incorrect format for character");
						break;
					case ERROR_LITERAL_CHARACTER_SINGLEQUOTE:
						errors.push_back("unexpected singlequote in line");
						break;
					case ERROR_LITERAL_INTEGER_HEX:
						errors.push_back("hex must have atleast one digit following x");
						break;
					case ERROR_COMMENT_MULTILINE_EOF:
						errors.push_back("encountered unexpected EOF while parsing comment");
						break;
					case ERROR:
					default:
						errors.push_back("incorrect syntax");
						break;
						
				}
			}
		}
		switch(ntoken)
		{
			case ERROR_LITERAL_STRING_LINE_TERMINATOR:
				tokens.push_back(yytext);
				token_desc.push_back(tokenDescription(ntoken));
			case LINE_TERMINATOR:
				codeline="";
				for(it = tokens.begin();it!=tokens.end();++it)
				{
					codeline = codeline + *it;
				}
				cout<<left<<setw(codewidth)<<codeline;
				cout<<"//";
				for(iit = token_desc.begin();iit != token_desc.end();++iit)
				{
					if((*iit).nToken != WHITESPACE)
					{
						cout<<token_names[(*iit).nToken]<<(*iit).tToken<<" ";
					}
				}
				tokens.clear(); token_desc.clear();
				cout<<endl;
				break;
			case KEYWORD:
			case SEPARATOR:
			case OPERATOR:
				tokens.push_back(yytext);
				token_desc.push_back(tokenDescription(ntoken, underscore+yytext));
				break;
			default: 
				tokens.push_back(yytext);
				token_desc.push_back(tokenDescription(ntoken));
		}
		//cout<<ntoken<<endl;
		ntoken = yylex();
	}
	if(error_p)
	{
		if(errors.size() > 0)
		{
			cout<<"Errors:"<<errors.size()<<endl;
			for(int i = 0 ; i < errorLineNos.size(); ++i)
			{
				cout<<"In line no:"<<errorLineNos[i]<<" "<<errors[i]<<endl;
			}
		}
		else 
		{
			cout<<"No errors found"<<endl;
		}
		cout<<"Lexer terminated successfully."<<endl;
	}
	return 0;
}
