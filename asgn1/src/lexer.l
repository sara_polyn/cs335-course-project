%{
	#include "lexer.h"
%}
%x IN_COMMENT IN_STRING
LineTerminator	\n|\r|\r\n
EscapeSequence	\\b|\\\\|\\t|\\'|\\n|\\f|\\\"|\\r
Keyword		 "abstract"|"continue"|"for"|"new"|"switch"|"assert"|"default"|"if"|"package"|"synchronized"|"boolean"|"do"|"goto"|"private"|"this"|"break"|"double"|"implements"|"protected"|"throw"|"byte"|"else"|"import"|"public"|"throws"|"case"|"enum"|"instanceof"|"return"|"transient"|"catch"|"extends"|"int"|"short"|"try"|"char"|"final"|"interface"|"static"|"void"|"class"|"finally"|"long"|"strictfp"|"volatile"|"const"|"float"|"native"|"super"|"while"
Separator 	"("|")"|"{"|"}"|"["|"]"|";"|","|"."|"..."|"@"|"::"
Operator	"="|">"|"<"|"!"|"~"|"?"|":"|"->"|"=="|">="|"<="|"!="|"&&"|"||"|"++"|"--"|"+"|"-"|"*"|"/"|"&"|"|"|"^"|"%"|"<<"|">>"|">>>"|"+="|"-="|"*="|"/="|"&="|"|="|"^="|"%="|"<<="|">>="|">>>="
Identifier 	[a-zA-Z][a-zA-Z0-9_$]*


Digit 					[0-9]
Digits					{Digit}*
HexDigit				[0-9a-fA-F]
HexDigits				{HexDigit}+
OctalDigit				[0-7]
BinaryDigit				[01]
NonZeroDigit				[1-9]
IntegerTypeSuffix 			[lL]
DecimalNumeral				0|{NonZeroDigit}{Digits}
DecimalIntegerLiteral			{DecimalNumeral}{IntegerTypeSuffix}?
HexIntegerLiteral			"0"[xX]{HexDigits}
OctalIntegerLiteral			"0"{OctalDigit}+
BinaryIntegerLiteral			"0"[bB]{BinaryDigit}+

HexIntegerError				"0"[xX]

Sign 					[+-]
SignedInteger 				{Sign}?{Digits}
ExponentIndicator			[eE]
BinaryExponentIndicator			[pP]
FloatTypeSuffix 			[fFdD]
HexNumeral				{HexIntegerLiteral}
HexSignificand				({HexNumeral}(".")?)|("0"[xX]{HexDigits}?"."{HexDigits})
BinaryExponent				{BinaryExponentIndicator}{SignedInteger}
HexadecimalFloatingPointLiteral 	{HexSignificand}{BinaryExponent}{FloatTypeSuffix}?	
ExponentPart				{ExponentIndicator}{SignedInteger}
DFPLType1				{Digits}"."{Digits}?{ExponentPart}?{FloatTypeSuffix}?
DFPLType2				"."{Digits}{ExponentPart}?{FloatTypeSuffix}?
DFPLType3				{Digits}{ExponentPart}{FloatTypeSuffix}?
DFPLType4				{Digits}{ExponentPart}?{FloatTypeSuffix}
DecimalFloatingPointLiteral		{DFPLType1}|{DFPLType2}|{DFPLType3}|{DFPLType4}
FloatingPointLiteral 			{DecimalFloatingPointLiteral}|{HexadecimalFloatingPointLiteral}

BooleanLiteral 		"true"|"false"
NullLiteral		"null"

SingleCharacter		[^\\\'\n\r(\r\n)]
CharacterLiteral 	\'({SingleCharacter}|{EscapeSequence})\'

%%
<INITIAL>{
	{LineTerminator}			yylineno++; return LINE_TERMINATOR;	
	\/\/.*					return COMMENT_SINGLELINE;
	{Separator}				return SEPARATOR;
	{Operator}				return OPERATOR;
	[ \t\f]					return WHITESPACE;
	\"					BEGIN(IN_STRING); yymore();
	{Keyword}				return KEYWORD; 
	{BooleanLiteral}			return LITERAL_BOOLEAN;
	{NullLiteral}				return LITERAL_NULL;
	{Identifier}				return IDENTIFIER;

	{CharacterLiteral} 			return LITERAL_CHARACTER;
	\'\'\'|\'                               return ERROR_LITERAL_CHARACTER_SINGLEQUOTE;
	\'[^\'\n\r(\r\n)]*\'			return ERROR_LITERAL_CHARACTER;

	"/*"            	        	BEGIN(IN_COMMENT); yymore();



	{DecimalIntegerLiteral}			|
	{HexIntegerLiteral}			| 
	{OctalIntegerLiteral}			|
	{BinaryIntegerLiteral}			return LITERAL_INTEGER;
	{FloatingPointLiteral}			return LITERAL_FLOAT;
	
	{HexIntegerError}			return ERROR_LITERAL_INTEGER_HEX;

	.					return ERROR;
}
<IN_COMMENT>{
	"*/"    				{
							BEGIN(INITIAL);
							return COMMENT_MULTILINE;
						}
	<<EOF>>					return ERROR_COMMENT_MULTILINE_EOF;
	{LineTerminator}			yylineno++; yymore();
	.   					yymore();
}
<IN_STRING>{
	\"					{
							BEGIN(INITIAL);	
							return LITERAL_STRING;
						}
	{EscapeSequence}				yymore();
	\\					{
							BEGIN(INITIAL); 
							return ERROR_LITERAL_STRING_ESCAPE_CHARACTER;
						}
	<<EOF>>					return ERROR_LITERAL_STRING_EOF;
	{LineTerminator}			{
							yylineno++; 
							yytext[yyleng-1] = '\0'; 
							BEGIN(INITIAL); 
							return ERROR_LITERAL_STRING_LINE_TERMINATOR;
						}
	.         				yymore();
}

%%

int yywrap(void)
{
	return 1;
}
