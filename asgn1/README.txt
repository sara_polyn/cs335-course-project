Compilation instructions:
make

Testing :
bin/lexer [-v] test/test[1-5].java

The test files are labelled test1.java - test5.java
Use -v (optional) for printing detailed error messages

We have strictly followed "The Java Language Specification, Java SE 7 edition" at url : http://docs.oracle.com/javase/specs/jls/se7/jls7.pdf
escept following:
1. We have supported ASCII only and not unicode. 
2. In decimal numeral we don't have support for underscore.

By Group 17:
Pranav Maneriker (12497)
Viveka Kulharia (12831)
Arpit Shrivastava (12161)
