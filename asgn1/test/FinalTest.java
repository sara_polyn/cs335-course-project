class FinalTest{
	public static void main(String args[])
	{
		//if-then-else, identifier, assignment op
		int i;
		if(1 > 2)
		{
			i=2;
		}
		else
		{
			i=2
		}
		//while
		while(true)
		{
			i++;
			if(i>3)break;
		}
		//dowhile
		do{
			i--;
		}
		while(i>0);
		//array
		int arr[] = new int[100];
		//for:
		for(int j=0;j<100;++j)
		{
			arr[j] = j;
		}
		//func call
		i = func(i);
		//constant
		final double INTEREST = 10.0d;
		9alpha = 1.0e4;
		charbeta = 0x25FF;
		char cc = 'c';
		str = "H\"ello \n ok";
		// comment
		/*
		   This is a multiline comment
		   yay!
		   more lines
		   */
		boolean val;
		//switch
		switch(i)
		{
			case 1: {
					val = true;
					break;
				}
			default:{
					val = false;
					break;
			}

		}
		
		//operators:
		//unary:
		i = -1;
		//binary:
		i = 1*2;
		//ternary:
		i = i>2?3:4;
		

		
	}
	static func(int inp)
	{
		inp++;
		return inp;
	}
}
